"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const cors_1 = __importDefault(require("cors"));
const opta_factory_service_1 = require("./lib/opta/service/opta-factory-service");
const opta_constant_1 = require("./lib/opta/opta-constant");
dotenv_1.default.config();
const app = (0, express_1.default)();
const port = process.env.PORT;
const SERVICE_NAME = 'CLOUDANT';
const dbname_teams = 'optadb_teams';
const dbname_players = 'optadb_players';
/* CORS */
app.use((0, cors_1.default)({
    origin: '*'
}));
/* BODY PARSER */
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({
    extended: true
}));
/* ENDPOINTS */
/* OPTA DB RESUME */
app.post('/optadb/resume', (req, res) => {
    opta_factory_service_1.OptaFactoryService.postResume(req.body).then((info) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(info);
    });
});
/* OPTA UI IONIC */
app.post('/optadb/ionic', (req, res) => {
    opta_factory_service_1.OptaFactoryService.postInfoDBName(req.body, opta_constant_1.OptaConstant.dbname_ionic).then((info) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(info);
    });
});
/* OPTA WHOSCORED */
app.post('/optadb/whoscored', (req, res) => {
    opta_factory_service_1.OptaFactoryService.postInfoDBName(req.body, opta_constant_1.OptaConstant.dbname_whoscored).then((info) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(info);
    });
});
/* OPTA Classements */
app.post('/optadb/classements', (req, res) => {
    opta_factory_service_1.OptaFactoryService.postInfoDBName(req.body, opta_constant_1.OptaConstant.dbname_classements).then((info) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(info);
    });
});
app.post('/opta/team', (req, res) => {
    opta_factory_service_1.OptaFactoryService.getInfosByTeam(req.body).then((info) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(info);
    });
});
app.post('/opta/league', (req, res) => {
    opta_factory_service_1.OptaFactoryService.getInfosByLeague(req.body).then((info) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(info);
    });
});
app.get('/opta/info', (req, res) => {
    opta_factory_service_1.OptaFactoryService.getInfos().then((info) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(info);
    });
});
/* SOCCER STATS */
app.post('/soccerstats', (req, res) => {
    opta_factory_service_1.OptaFactoryService.postInfoSoccerStat(req.body).then((info) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(info);
    });
});
/* LISTENERS */
app.listen(port, () => {
    console.log(`[server]: Server is running at http://localhost:${port}`);
});
