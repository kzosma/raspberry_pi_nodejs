"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CloudantDbService = void 0;
const cloudant_1 = require("@ibm-cloud/cloudant");
const luxon_1 = require("luxon");
const opta_constant_1 = require("../../opta/opta-constant");
class CloudantDbService {
    /* ASYNC Function : Get Document by selector */
    static getDocumentBySelector(db_name) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                this.getClient().postAllDocs({
                    db: db_name,
                    includeDocs: true
                }).then(response => {
                    resolve(response.result);
                });
            });
        });
    }
    /* ASYNC Function : Create database */
    static createDatabase(dbName) {
        return __awaiter(this, void 0, void 0, function* () {
            let codeRetour = 200;
            try {
                const dbInfo = yield this.getClient().putDatabase({ db: dbName });
                if (dbInfo.result.ok) {
                    // console.log('Database : ' + dbName + ' is created'); 
                }
            }
            catch (err) {
                if (err.code === 412) {
                    // console.log('Database ' + dbName + ' is already created');
                }
                codeRetour = err.code;
            }
            finally {
                return codeRetour;
            }
        });
    }
    /* Persistance du document dans la base Cloudant */
    static persistDoc(dbName, doc) {
        return __awaiter(this, void 0, void 0, function* () {
            CloudantDbService.getClient().postDocument({
                db: dbName,
                document: doc
            })
                .then((createDocumentResponse) => {
                doc._rev = createDocumentResponse.result.rev;
                // console.log('Persistence du document OK : ', JSON.stringify(doc,null,2));
            });
        });
    }
    /* GETTERS / SETTERS */
    static getClient() {
        if (!this.client) {
            this.client = cloudant_1.CloudantV1.newInstance({ serviceName: opta_constant_1.OptaConstant.SERVICE_NAME_CLOUDANT });
        }
        return this.client;
    }
    /* TEAMDOCUMENT */
    /* Update or create TeamDocument */
    static updateOrCreateTeamDocument(obj, t, fixtures) {
        return __awaiter(this, void 0, void 0, function* () {
            let teamDocument = {};
            const getDocParams = { docId: obj.idTeam, db: obj.dbName };
            CloudantDbService.getClient().getDocument(getDocParams)
                .then((docResult) => {
                teamDocument = docResult.result;
                // console.log('Document found : ', teamDocument);
            })
                .catch((err) => {
                if (err.code === 404) {
                    // console.log('Document ' + obj.idTeam +' not found');
                    teamDocument = {
                        _id: obj.idTeam,
                        nameTeam: obj.nameTeam,
                        date: luxon_1.DateTime.now().toFormat('dd-MM-yyyy HH:mm:ss')
                    };
                }
                else {
                    console.log('Erreur technique : ', err);
                }
            })
                .finally(() => __awaiter(this, void 0, void 0, function* () {
                const retour = this.buildTeamModel(teamDocument, t, fixtures);
                if (retour.up_to_date === true) {
                    CloudantDbService.persistDoc(obj.dbName, retour.teamDocument);
                }
            }));
        });
    }
    // Raspberry
    static buildTeamModel(teamDocument, obj, fixtures) {
        var _a;
        let retour = { teamDocument: teamDocument, up_to_date: false };
        if (teamDocument.fixtures === undefined) {
            teamDocument.fixtures = [];
        }
        const key = obj.history ? obj.history.length - 1 : undefined;
        const model = key !== undefined ? teamDocument.fixtures.find((m) => {
            return m.dayMatch === key;
        }) : undefined;
        if (key !== undefined && (!model || model === undefined)) {
            (_a = obj.history) === null || _a === void 0 ? void 0 : _a.forEach((h, i) => {
                var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q;
                const teamModel = {
                    current_stat: {},
                    against_stat: {}
                };
                teamModel.dayMatch = i;
                const fixture = this.searchFixture(obj, h, fixtures);
                teamModel.date = fixture === null || fixture === void 0 ? void 0 : fixture.datetime;
                teamModel.idMatch = fixture === null || fixture === void 0 ? void 0 : fixture.id;
                teamModel.h_a = h.h_a;
                teamModel.pts = h.pts;
                teamModel.xpts = h.xpts;
                teamModel.wins = h.wins;
                teamModel.draws = h.draws;
                teamModel.loses = h.loses;
                teamModel.current_stat.id_team = teamDocument._id;
                if (((_a = fixture === null || fixture === void 0 ? void 0 : fixture.h) === null || _a === void 0 ? void 0 : _a.id) === teamModel.current_stat.id_team) {
                    teamModel.current_stat.title_team = (_b = fixture === null || fixture === void 0 ? void 0 : fixture.h) === null || _b === void 0 ? void 0 : _b.title;
                    teamModel.current_stat.short_title_team = (_c = fixture === null || fixture === void 0 ? void 0 : fixture.h) === null || _c === void 0 ? void 0 : _c.short_title;
                    teamModel.against_stat.title_team = (_d = fixture === null || fixture === void 0 ? void 0 : fixture.a) === null || _d === void 0 ? void 0 : _d.title;
                    teamModel.against_stat.short_title_team = (_e = fixture === null || fixture === void 0 ? void 0 : fixture.a) === null || _e === void 0 ? void 0 : _e.short_title;
                }
                else if (((_f = fixture === null || fixture === void 0 ? void 0 : fixture.a) === null || _f === void 0 ? void 0 : _f.id) === teamModel.current_stat.id_team) {
                    teamModel.current_stat.title_team = (_g = fixture === null || fixture === void 0 ? void 0 : fixture.a) === null || _g === void 0 ? void 0 : _g.title;
                    teamModel.current_stat.short_title_team = (_h = fixture === null || fixture === void 0 ? void 0 : fixture.a) === null || _h === void 0 ? void 0 : _h.short_title;
                    teamModel.against_stat.title_team = (_j = fixture === null || fixture === void 0 ? void 0 : fixture.h) === null || _j === void 0 ? void 0 : _j.title;
                    teamModel.against_stat.short_title_team = (_k = fixture === null || fixture === void 0 ? void 0 : fixture.h) === null || _k === void 0 ? void 0 : _k.short_title;
                }
                // Current StatModel
                teamModel.current_stat.goals = h.scored;
                teamModel.current_stat.deep = h.deep;
                teamModel.current_stat.npxG = h.npxG;
                teamModel.current_stat.xG = h.xG;
                teamModel.current_stat.ppda_att = (_l = h.ppda) === null || _l === void 0 ? void 0 : _l.att;
                teamModel.current_stat.ppda_def = (_m = h.ppda) === null || _m === void 0 ? void 0 : _m.def;
                // Against StatModel
                teamModel.against_stat.goals = h.missed;
                teamModel.against_stat.deep = h.deep_allowed;
                teamModel.against_stat.npxG = h.npxGA;
                teamModel.against_stat.xG = h.xGA;
                teamModel.against_stat.ppda_att = (_o = h.ppda_allowed) === null || _o === void 0 ? void 0 : _o.att;
                teamModel.against_stat.ppda_def = (_p = h.ppda_allowed) === null || _p === void 0 ? void 0 : _p.def;
                (_q = teamDocument.fixtures) === null || _q === void 0 ? void 0 : _q.push(teamModel);
            });
            teamDocument.date = luxon_1.DateTime.now().toFormat('dd-MM-yyyy HH:mm:ss');
            retour.up_to_date = true;
            retour.teamDocument = teamDocument;
        }
        return retour;
    }
    static searchFixture(t, h, fixtures) {
        if (h.h_a === 'h') {
            return fixtures.find((f) => { var _a; return (f.datetime === h.date && ((_a = f.h) === null || _a === void 0 ? void 0 : _a.id) === t.id); });
        }
        else {
            return fixtures.find((f) => { var _a; return (f.datetime === h.date && ((_a = f.a) === null || _a === void 0 ? void 0 : _a.id) === t.id); });
        }
    }
}
exports.CloudantDbService = CloudantDbService;
