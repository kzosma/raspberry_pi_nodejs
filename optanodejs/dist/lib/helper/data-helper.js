"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataHelper = void 0;
const jackson_js_1 = require("jackson-js");
const fixture_jk_1 = require("../opta/model/fixture-jk");
const teamJK_linkedHashMap_1 = require("../opta/model/teamJK-linkedHashMap");
class DataHelper {
    constructor() { }
    static getTeamList(teamsDataScript) {
        const teamJKList = this.objetMapper.parse(teamsDataScript, { mainCreator: () => [teamJK_linkedHashMap_1.TeamJKLinkedHashMap] });
        return teamJKList;
    }
    static getFeaturesList(datesDataScript) {
        const fixtureJKList = this.objetMapper.parse(datesDataScript, { mainCreator: () => [Array, [fixture_jk_1.FixtureJk]] });
        return fixtureJKList;
    }
    static deserialize(obj) {
        return this.objetMapper.stringify(obj);
    }
}
exports.DataHelper = DataHelper;
DataHelper.objetMapper = new jackson_js_1.ObjectMapper();
