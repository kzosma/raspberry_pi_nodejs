"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OptaHelper = void 0;
const string_helper_1 = require("./string-helper");
class OptaHelper {
    constructor() { }
    static findIndStart(arg) {
        return arg.indexOf("('") + 2;
    }
    static findIndEnd(arg) {
        return arg.indexOf("')");
    }
    static extractDataFromScript(arg) {
        const dataJson = string_helper_1.StringHelper.strip(arg);
        const start = this.findIndStart(dataJson);
        const end = this.findIndEnd(dataJson);
        const data = dataJson.substring(start, end);
        const dataUTF8 = Buffer.from(data, 'utf-8').toString();
        const dataEscapeUTF8 = string_helper_1.StringHelper.unescape(dataUTF8);
        return dataEscapeUTF8;
    }
}
exports.OptaHelper = OptaHelper;
