"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StringHelper = void 0;
class StringHelper {
    constructor() { }
    static strip(expression) {
        return expression.replace(/^[ \t]+|[ \t]+$/g, '');
    }
    static unescape(expression) {
        return expression
            .split('\\x')
            .slice(1)
            .map(v => {
            return String.fromCharCode(parseInt(v.slice(0, 2), 16)) + v.slice(2);
        }).join('');
    }
}
exports.StringHelper = StringHelper;
