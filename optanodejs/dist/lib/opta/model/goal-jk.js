"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GoalJk = void 0;
const jackson_js_1 = require("jackson-js");
class GoalJk {
    constructor(h, a) {
        this.h = h;
        this.a = a;
    }
}
__decorate([
    (0, jackson_js_1.JsonProperty)(),
    (0, jackson_js_1.JsonClassType)({ type: () => [String] })
], GoalJk.prototype, "h", void 0);
__decorate([
    (0, jackson_js_1.JsonProperty)(),
    (0, jackson_js_1.JsonClassType)({ type: () => [String] })
], GoalJk.prototype, "a", void 0);
exports.GoalJk = GoalJk;
