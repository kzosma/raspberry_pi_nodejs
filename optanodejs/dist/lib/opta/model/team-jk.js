"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TeamJk = void 0;
const jackson_js_1 = require("jackson-js");
const history_jk_1 = require("./history-jk");
let TeamJk = class TeamJk {
    constructor(id, title) {
        this.history = [];
        this.id = id;
        this.title = title;
    }
};
__decorate([
    (0, jackson_js_1.JsonProperty)(),
    (0, jackson_js_1.JsonClassType)({ type: () => [String] })
], TeamJk.prototype, "id", void 0);
__decorate([
    (0, jackson_js_1.JsonProperty)(),
    (0, jackson_js_1.JsonClassType)({ type: () => [String] })
], TeamJk.prototype, "title", void 0);
__decorate([
    (0, jackson_js_1.JsonProperty)(),
    (0, jackson_js_1.JsonClassType)({ type: () => [Array, [history_jk_1.HistoryJk]] })
], TeamJk.prototype, "history", void 0);
TeamJk = __decorate([
    (0, jackson_js_1.JsonInclude)({ value: jackson_js_1.JsonIncludeType.NON_EMPTY })
], TeamJk);
exports.TeamJk = TeamJk;
