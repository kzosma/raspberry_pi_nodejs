"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TeamJKLinkedHashMap = void 0;
const jackson_js_1 = require("jackson-js");
const team_jk_1 = require("./team-jk");
class TeamJKLinkedHashMap {
    constructor() {
        this.detail = new Map();
    }
    setDetail(propertyKey, value) {
        this.detail.set(propertyKey, value);
    }
    toList() {
        const arr = [...this.detail].map(([name, value]) => {
            // delete value.history;
            return value;
        });
        return arr;
    }
}
__decorate([
    (0, jackson_js_1.JsonProperty)(),
    (0, jackson_js_1.JsonClassType)({ type: () => [Map, [String, team_jk_1.TeamJk]] })
], TeamJKLinkedHashMap.prototype, "detail", void 0);
__decorate([
    (0, jackson_js_1.JsonAnySetter)()
], TeamJKLinkedHashMap.prototype, "setDetail", null);
exports.TeamJKLinkedHashMap = TeamJKLinkedHashMap;
