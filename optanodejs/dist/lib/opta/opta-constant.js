"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OptaConstant = void 0;
class OptaConstant {
}
exports.OptaConstant = OptaConstant;
/* CLOUDANT */
OptaConstant.SERVICE_NAME_CLOUDANT = 'CLOUDANT';
OptaConstant.dbname_teams = 'optadb_teams';
OptaConstant.dbname_players = 'optadb_players';
OptaConstant.dbname_ionic = 'optadb_ionic';
OptaConstant.dbname_whoscored = 'model_team_whoscored';
OptaConstant.dbname_classements = 'model_team_classements';
OptaConstant.dbname_soccer_stats = 'soccer_stats';
OptaConstant.dbname_resume = 'optadb_resume';
/* URLs*/
OptaConstant.base_url_league = 'https://understat.com/league';
OptaConstant.base_url_team = 'https://understat.com/team';
/* Championnats */
OptaConstant.LIGUE1 = 'Ligue_1';
OptaConstant.LIGA = 'La_liga';
OptaConstant.SERIEA = 'Serie_A';
OptaConstant.BUNDESLIGA = 'Bundesliga';
OptaConstant.EPL = 'EPL';
/*Seasons */
OptaConstant.seasons = ['2018', '2019', '2020', '2021', '2022', '2023'];
