"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OptaFactoryService = void 0;
const opta_constant_1 = require("../../opta/opta-constant");
const data_helper_1 = require("../../helper/data-helper");
const info_jk_1 = require("../model/info-jk");
const info_league_jk_1 = require("../model/info-league-jk");
const cloudant_db_service_1 = require("../../cloudant/service/cloudant-db-service");
class OptaFactoryService {
    constructor() {
    }
    // Service Opta DBNAME
    static postInfoDBName(json, dbname) {
        return __awaiter(this, void 0, void 0, function* () {
            let obj = JSON.parse(JSON.stringify(json));
            const season = obj.season;
            const doc = obj.doc;
            let info = {};
            const url = dbname + '_' + season;
            const p = new Promise((resolve, reject) => {
                cloudant_db_service_1.CloudantDbService.getDocumentBySelector(url)
                    .then((result) => {
                    result.rows.forEach((r) => {
                        const objR = { 'id': r.id, 'data': r.doc.data };
                        if (r.id === doc) {
                            info = objR;
                        }
                    });
                    resolve(data_helper_1.DataHelper.deserialize(info));
                });
            });
            return p;
        });
    }
    // Service Opta Ionic
    static postInfoIonic(json) {
        return __awaiter(this, void 0, void 0, function* () {
            let obj = JSON.parse(JSON.stringify(json));
            const season = obj.season;
            const doc = obj.doc;
            let info = [];
            const url = opta_constant_1.OptaConstant.dbname_ionic + '_' + season;
            const p = new Promise((resolve, reject) => {
                cloudant_db_service_1.CloudantDbService.getDocumentBySelector(url)
                    .then((result) => {
                    result.rows.forEach((r) => {
                        const objR = { 'id': r.id, 'data': r.doc.data };
                        info.push(objR);
                    });
                    resolve(data_helper_1.DataHelper.deserialize(info));
                });
            });
            return p;
        });
    }
    // Service Soccer Stat
    static postInfoSoccerStat(json) {
        return __awaiter(this, void 0, void 0, function* () {
            let obj = JSON.parse(JSON.stringify(json));
            const season = obj.season;
            const doc = obj.doc;
            const dbname = obj.dbname;
            let info = [];
            const url = opta_constant_1.OptaConstant.dbname_soccer_stats + '_' + dbname + '_' + season;
            const p = new Promise((resolve, reject) => {
                cloudant_db_service_1.CloudantDbService.getDocumentBySelector(url)
                    .then((result) => {
                    result.rows.forEach((r) => {
                        const objR = { 'id': r.id, 'data': r.doc.data };
                        if (doc === '' || doc === r.id) {
                            info.push(objR);
                        }
                    });
                    resolve(data_helper_1.DataHelper.deserialize(info));
                });
            });
            return p;
        });
    }
    // Service OptaDB Resume
    static postResume(json) {
        return __awaiter(this, void 0, void 0, function* () {
            let obj = JSON.parse(JSON.stringify(json));
            const season = obj.season;
            const doc = obj.doc;
            const dbname = obj.dbname;
            let info = [];
            const url = opta_constant_1.OptaConstant.dbname_resume + '_' + dbname + '_' + season;
            const p = new Promise((resolve, reject) => {
                cloudant_db_service_1.CloudantDbService.getDocumentBySelector(url)
                    .then((result) => {
                    result.rows.forEach((r) => {
                        const objR = { 'id': r.id, 'data': r.doc.data };
                        if (doc === '' || doc === r.id) {
                            info.push(objR);
                        }
                    });
                    resolve(data_helper_1.DataHelper.deserialize(info));
                });
            });
            return p;
        });
    }
    static getInfos() {
        return __awaiter(this, void 0, void 0, function* () {
            let info = new info_jk_1.InfoJk();
            info.seasons = opta_constant_1.OptaConstant.seasons;
            let ligue1 = new info_league_jk_1.InfoLeagueJk();
            ligue1.id = opta_constant_1.OptaConstant.LIGUE1;
            ligue1.title = opta_constant_1.OptaConstant.LIGUE1.replace('_', ' ');
            info.leagues.push(ligue1);
            let epl = new info_league_jk_1.InfoLeagueJk();
            epl.id = opta_constant_1.OptaConstant.EPL;
            epl.title = opta_constant_1.OptaConstant.EPL.replace('_', ' ');
            info.leagues.push(epl);
            let seriea = new info_league_jk_1.InfoLeagueJk();
            seriea.id = opta_constant_1.OptaConstant.SERIEA;
            seriea.title = opta_constant_1.OptaConstant.SERIEA.replace('_', ' ');
            info.leagues.push(seriea);
            let liga = new info_league_jk_1.InfoLeagueJk();
            liga.id = opta_constant_1.OptaConstant.LIGA;
            liga.title = opta_constant_1.OptaConstant.LIGA.replace('_', ' ');
            info.leagues.push(liga);
            let bundesliga = new info_league_jk_1.InfoLeagueJk();
            bundesliga.id = opta_constant_1.OptaConstant.BUNDESLIGA;
            bundesliga.title = opta_constant_1.OptaConstant.BUNDESLIGA.replace('_', ' ');
            info.leagues.push(bundesliga);
            return data_helper_1.DataHelper.deserialize(info);
        });
    }
    static getInfosByLeague(json) {
        return __awaiter(this, void 0, void 0, function* () {
            let obj = JSON.parse(JSON.stringify(json));
            const league = obj.league.replace(/ /g, '_');
            const season = obj.season;
            let info = [];
            const p = new Promise((resolve, reject) => {
                cloudant_db_service_1.CloudantDbService.getDocumentBySelector(opta_constant_1.OptaConstant.dbname_teams + '_' + season)
                    .then((result) => {
                    result.rows.forEach((r) => {
                        const objR = { 'id': r.id, 'title': r.doc.nameTeam };
                        if (r.doc.nameLeague === league) {
                            info.push(objR);
                        }
                    });
                    resolve(data_helper_1.DataHelper.deserialize(info));
                });
            });
            return p;
        });
    }
    static getInfosByTeam(json) {
        return __awaiter(this, void 0, void 0, function* () {
            let obj = JSON.parse(JSON.stringify(json));
            const id_team = obj.idteam;
            const season = obj.season;
            let team = {};
            const p = new Promise((resolve, reject) => {
                cloudant_db_service_1.CloudantDbService.getDocumentBySelector(opta_constant_1.OptaConstant.dbname_teams + '_' + season)
                    .then((result) => {
                    result.rows.forEach((r) => {
                        const objR = { 'id': r.id, 'title': r.doc.nameTeam, 'league': r.doc.nameLeague, 'fixtures': r.doc.fixtures };
                        if (r.id === id_team) {
                            team = objR;
                        }
                    });
                    resolve(data_helper_1.DataHelper.deserialize(team));
                });
            });
            return p;
        });
    }
}
exports.OptaFactoryService = OptaFactoryService;
