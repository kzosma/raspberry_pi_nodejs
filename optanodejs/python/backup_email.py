from email import message
import os
from dotenv import load_dotenv
from lib.mail import mail_factory

# Variables environment
# load_dotenv('F:/developpement/gitlab/raspberry_pi_nodejs/optanodejs/.env')
load_dotenv('/home/pi/gitlab/raspberry_pi_nodejs/optanodejs/.env')
smtp_server = os.getenv('SMTP_SERVER')
smtp_port = os.getenv('SMTP_PORT')
gmail_user =  os.getenv('GMAIL_USER')
gmail_password = os.getenv('GMAIL_PASSWORD')
print('SMTP SERVER : ' + smtp_server)
print('SMTP PORT : ' + smtp_port)
print('GMAIL USER : ' + gmail_user)
print('GMAIL PASSWORD : ' + gmail_password)
mailHelper = mail_factory.MailHelperService()

# Edit Message Mail
subject =  'OPTA'
msg_content = '\n'
message = mailHelper.createMessage(gmail_user,gmail_user, subject,msg_content)

# Add attachment to Message
attachmentPlayersPath = os.getenv('ATTACHMENT_PLAYERS_PATH')
attachmentTeamsPath = os.getenv('ATTACHMENT_TEAMS_PATH')
message = mailHelper.addAttachment(message, attachmentPlayersPath, 'optadb_players_2022.json', 'json')
message = mailHelper.addAttachment(message, attachmentTeamsPath, 'optadb_teams_2022.json', 'json')
msg_full = message.as_string()

# SSL Context
context = mailHelper.getSSLContext()

# Send Mail 
mailHelper.sendMail(
    smtp_server,
    smtp_port,
    context,
    gmail_user,
    gmail_password,
    msg_full
 )
