# Constants

# Information Cloudant Database
SERVICE_NAME_CLOUDANT = 'CLOUDANT'
CLOUDANT_URL = 'https://3695eefa-cd56-4b84-820a-c7a7f16b57b5-bluemix.cloudantnosqldb.appdomain.cloud'
CLOUDANT_APIKEY = 'hinaW9a3d5-qRVci_jIB36ZalOGvovL9s_Y_t_ahzSCN'
dbname_teams = 'optadb_teams'
dbname_players = 'optadb_players'
dbname_gps = 'optadb_gps'
dbname_ionic = 'optadb_ionic'

# Information Service Understat
base_url_league = 'https://understat.com/league'
base_url_team = 'https://understat.com/team'
base_url_match = 'https://understat.com/match'
base_url_player = 'https://understat.com/player'

# Liste des saisons
seasons = ['2023']
# Liste des leagues
leagues = ['La_liga', 'EPL', 'Bundesliga', 'Serie_A', 'Ligue_1']
LIGUE1 = 'Ligue_1'
LIGA = 'La_liga'
SERIEA = 'Serie_A'
BUNDESLIGA = 'Bundesliga'
EPL = 'EPL'

# Liste de modèles base de données
model_team_whoscored = 'model_team_whoscored'
model_team_classements = 'model_team_classements'

# Soccer Stat
season_soccer_stat = '2024'
dbname_soccer_stats = 'soccer_stats'
url_soccer_stat = "https://www.soccerstats.com"
soccer_stats_leagues = ['AT','AU','AR','BE','BR','CH','CHI','COL','CRO','CZR','DE','D2','ECU','EN','E2','ES','ES2','FI','FR','F2','GR','IRE','ISR','JP','KR','MEX', 'NL','NOI','NOR', 'IT','PAR', 'PL','PT','ROM','SC','SE','SLO','TR', 'USA', 'WAL']

# Football Data CO UK
football_data_co_uk_teams = ['E0', 'F1', 'D1', 'I1','SP1']
football_data_co_uk_year_start = 2022
football_data_co_uk_year_end = 2023
dbname_historical_football_data_co_uk = 'historical_football_data_co_uk'
dbname_identity_football_data_co_uk = 'identity_football_data_co_uk'
football_data_co_uk_map_understat = [
    {'E0':'EPL'},
    {'F1':'Ligue_1'},
    {'D1':'Bundesliga'},
    {'I1':'Serie_A'},
    {'SP1':'La_liga'}
]
football_data_co_uk_map_soccerstat = [
    {'E0':'England - Premier League'},
    {'F1':'France - Ligue 1'},
    {'D1':'Germany - Bundesliga'},
    {'I1':'Italy - Serie A'},
    {'SP1':'Spain - La Liga'}
]

# NBA
# Variables
nba_season = 2023
# DBS
dbname_nba_per_game = 'optadb_nba_per_game'
dbname_nba_totals = 'optadb_nba_totals'
dbname_nba_shooting = 'optadb_nba_shooting'
dbname_nba_advanced = 'optadb_nba_advanced'
dbname_nba_per_poss = 'optadb_nba_per_poss'

# URLs
base_url_nba_per_game = 'https://www.basketball-reference.com/leagues/NBA_{}_per_game.html'
base_url_nba_totals = 'https://www.basketball-reference.com/leagues/NBA_{}_totals.html'
base_url_nba_shooting = 'https://www.basketball-reference.com/leagues/NBA_{}_shooting.html'
base_url_nba_advanced = 'https://www.basketball-reference.com/leagues/NBA_{}_advanced.html'
base_url_nba_per_poss = 'https://www.basketball-reference.com/leagues/NBA_{}_per_poss.html'





