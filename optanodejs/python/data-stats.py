from datetime import datetime
from constants import opta_constants
from lib.understat import understat_factory
from lib.cloudant.service import cloudant_db_service
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from lib.chrome import chrome_service
import os
import pandas as pd
import numpy as np
import json
from optadatalib.Helper import Utils
from fuzzywuzzy import fuzz
from fuzzywuzzy import process


# Functions
# Construction du document
def buildDoc(id, data):
         modelDocument = {}
         modelDocument['_id'] = id
         modelDocument["data"] = data
         modelDocument['date'] =  datetime.now().strftime('%Y-%m-%d %H:%M:%S')
         return modelDocument

# Conversion de données en liste d'équipes
def filter_properties(data):
    filtered_data = []
    for entry in data:
        filtered_entry = {'Team': entry['Team'], 'id': entry['id']}
        filtered_data.append(filtered_entry)
    return filtered_data

# Récupération des données historique des équipes par ligue et saison
def getHistoricalTeams(league,season):
        filter = {'type': ''}
        driver = chrome_service.ChromeService().getDriver()
        data = understatService.getInfosByLeague(league+'/'+season, filter, driver)
        chrome_service.ChromeService().closeDriver(driver)
        teams = filter_properties(data['teams'])
        return teams

# Récupération du nom des ligues à partir du référentiel UnderStat
def get_league_name_understat(code):
    for mapping in opta_constants.football_data_co_uk_map_understat:
        if code in mapping:
            return mapping[code]
    return None

# Récupération du nom des ligues à partir du référentiel SoccerStat
def get_league_name_soccerstat(code):
    for mapping in opta_constants.football_data_co_uk_map_soccerstat:
        if code in mapping:
            return mapping[code]
    return None

# Matching entre les équipes de UnderStat et Football Data CO UK
def find_matching_teams_understat(team_dcu, team_understat):
    matching_teams = []
    for entry_understat in team_understat:
        teams_understat = process.extractOne(entry_understat['Team'], team_dcu, scorer=fuzz.token_sort_ratio)
        if teams_understat[1] >= 10:  
            matching_teams.append({
                'team': entry_understat['Team'],
                'id': entry_understat['id'],
                'team_dcu': teams_understat[0],
            })
    return matching_teams

# Matching entre les équipes de UnderStat et SoccerStat
def find_matching_teams_soccerstat(team_soccerstat, team_understat):
    matching_teams = []
    for entry_understat in team_understat:
        teams_soccerstat = process.extractOne(entry_understat['Team'], team_soccerstat, scorer=fuzz.token_sort_ratio)
        if teams_soccerstat[1] >= 10:  
            matching_teams.append({
                'Team': entry_understat['Team'],
                'id': entry_understat['id'],
                'Team_DCU': teams_soccerstat[0],
            })
    return matching_teams
# Création des documents équipes et championnats
def createIdentityDB():
    cloudantService.deleteDatabase(opta_constants.dbname_identity_football_data_co_uk)
    cloudantService.createDatabase(opta_constants.dbname_identity_football_data_co_uk)
    current_season = opta_constants.seasons[0]
    allteams = cloudantService.getAllDataFromDB(opta_constants.dbname_teams+'_'+current_season)
    leagues = []
    for league_fdc in opta_constants.football_data_co_uk_teams:
        league_soccerstat = get_league_name_soccerstat(league_fdc)
        teams_soccer_stat = getTeamsFromSoccerStat(doc_id=league_soccerstat)
        teams_understat = getHistoricalTeams(league=get_league_name_understat(league_fdc), season=str(2024))
        teams_fuzzy_soccerstat = find_matching_teams_soccerstat(team_soccerstat=teams_soccer_stat, team_understat=teams_understat)
        for t in teams_fuzzy_soccerstat:
            # Création d'une équipe et persistance en base
            data = fillData(allteams,t['id'])
            doc_team = buildDoc(t['id'],data=data)
            cloudantService.persistDoc(dbName=opta_constants.dbname_identity_football_data_co_uk, doc=doc_team)
        # Création d'une ligue et persistance en base
        doc_soccer = buildDoc(league_soccerstat,{'count':len(teams_fuzzy_soccerstat),'teams': teams_fuzzy_soccerstat})
        cloudantService.persistDoc(dbName=opta_constants.dbname_identity_football_data_co_uk, doc=doc_soccer)
        leagues.append(league_soccerstat)
    # Création de la liste des ligues et persistance en base
    doc_leagues = buildDoc('leagues',{'leagues': leagues})
    cloudantService.persistDoc(dbName=opta_constants.dbname_identity_football_data_co_uk, doc=doc_leagues)

# Obtenir les équipes à partir des données de SoccerStat
def getTeamsFromSoccerStat(doc_id):
    teams= []
    db_name = opta_constants.dbname_soccer_stats+'_wide_table_'+opta_constants.season_soccer_stat
    docResult = cloudantService.getClient().get_document(db=db_name,doc_id=doc_id)
    for t in docResult.result['data']:
        teams.append(t['id'])
    return teams

# Fill Data 
def fillData(opta_teams_db_data,id):
    # Modèle de données
    data = {
    'goals_resume':{'GP': 0, 'GPH': 0, 'GPA': 0, 'GFT': 0, 'GAT': 0,
                   'GFHT': 0,'GFAT': 0,'GAHT': 0,'GAAT': 0, 
                   'FOS_H': 0, 'FCG_H': 0,'FG_TIME_H': 0,'FG_TIME_H_FOR': 0, 'FG_TIME_H_AGAINST': 0,
                   'FOS_A': 0, 'FCG_A': 0,'FG_TIME_A': 0, 'FG_TIME_A_FOR': 0, 'FG_TIME_A_AGAINST': 0,
                   'FOS_T': 0, 'FCG_T': 0,'FG_TIME_T': 0, 'FG_TIME_T_FOR': 0, 'FG_TIME_T_AGAINST': 0},
    'goals_histo': []
    }
    # Récupération du document de la base opta_teams
    team_data = Utils.get_team_data_by_id(data=opta_teams_db_data, id=id)
    print('Fill Data for team : ' +team_data['doc']['nameTeam'])
    data['goals_resume']['GP'] = len(team_data['doc']['fixtures'])
    # Récupération des statistiques sur les buts 
    print('Récupération des statistiques sur les buts')
    data = fillGoals(team_data['doc']['fixtures'],data)
    return data

def fillGoals(fixtures, data):
    if fixtures:
        FG_TIME_H = 0
        FG_TIME_A = 0
        FG_TIME_H_FOR = 0
        FG_TIME_A_FOR = 0
        FG_TIME_H_AGAINST = 0
        FG_TIME_A_AGAINST = 0
        for f in fixtures:
            first_goal = getFirstGoal(f)
            FOS = 1 if first_goal['FOS'] == f['h_a'] else 0
            FCG = 1 if (first_goal['FOS'] != f['h_a'] and first_goal['FOS'] != '') else 0
            NG = 1 if first_goal['FOS'] == '' else 0
            dayMatchData = {'dayMatch': f['dayMatch'],
                             'h_a': f['h_a'],
                             'GF': f['g_for'],
                             'GA': f['g_against'],
                             'FOS': FOS,
                             'FCG': FCG,
                             'NG': NG,
                             'FG_TIME': int(first_goal['minute'])
                            }
            data['goals_histo'].append(dayMatchData)
            if f['h_a'] == 'h':
                data['goals_resume']['GFHT'] = data['goals_resume']['GFHT'] + f['g_for']
                data['goals_resume']['GAHT'] = data['goals_resume']['GAHT'] + f['g_against']
                data['goals_resume']['FOS_H'] = data['goals_resume']['FOS_H'] + dayMatchData['FOS']
                data['goals_resume']['FCG_H'] = data['goals_resume']['FCG_H'] + dayMatchData['FCG']
                data['goals_resume']['GPH'] = data['goals_resume']['GPH'] + 1
                FG_TIME_H = FG_TIME_H + int(first_goal['minute'])
                FG_TIME_H_FOR = FG_TIME_H_FOR + (int(first_goal['minute']) if FOS == 1 else 0)  
                FG_TIME_H_AGAINST = FG_TIME_H_AGAINST + (int(first_goal['minute']) if FCG == 1 else 0)

            if f['h_a'] == 'a':
                data['goals_resume']['GFAT'] = data['goals_resume']['GFAT'] + f['g_for']
                data['goals_resume']['GAAT'] = data['goals_resume']['GAAT'] + f['g_against']
                data['goals_resume']['FOS_A'] = data['goals_resume']['FOS_A'] + dayMatchData['FOS']
                data['goals_resume']['FCG_A'] = data['goals_resume']['FCG_A'] + dayMatchData['FCG']
                data['goals_resume']['GPA'] = data['goals_resume']['GPA'] + 1
                FG_TIME_A = FG_TIME_A + int(first_goal['minute'])
                FG_TIME_A_FOR = FG_TIME_A_FOR + (int(first_goal['minute']) if FOS == 1 else 0) 
                FG_TIME_A_AGAINST = FG_TIME_A_AGAINST + (int(first_goal['minute']) if FCG == 1 else 0)

            # TOTAL
            data['goals_resume']['GFT'] = data['goals_resume']['GFT'] + f['g_for']
            data['goals_resume']['GAT'] = data['goals_resume']['GAT'] + f['g_against']
            data['goals_resume']['FOS_T'] = data['goals_resume']['FOS_T'] + FOS
            data['goals_resume']['FCG_T'] = data['goals_resume']['FCG_T'] + FCG
            data['goals_resume']['FG_TIME_H'] = 0 if (data['goals_resume']['FOS_H'] + data['goals_resume']['FCG_H']) == 0 else round(FG_TIME_H / (data['goals_resume']['FOS_H'] + data['goals_resume']['FCG_H']),2)
            data['goals_resume']['FG_TIME_A'] = 0 if (data['goals_resume']['FOS_A'] + data['goals_resume']['FCG_A']) == 0 else  round(FG_TIME_A / (data['goals_resume']['FOS_A'] + data['goals_resume']['FCG_A']),2) 
            data['goals_resume']['FG_TIME_H_FOR'] = 0 if data['goals_resume']['FOS_H'] == 0 else round(FG_TIME_H_FOR / data['goals_resume']['FOS_H'],2)
            data['goals_resume']['FG_TIME_A_FOR'] = 0 if data['goals_resume']['FOS_A'] == 0 else  round(FG_TIME_A_FOR / data['goals_resume']['FOS_A'],2) 
            data['goals_resume']['FG_TIME_H_AGAINST'] = 0 if data['goals_resume']['FCG_H'] == 0 else round(FG_TIME_H_AGAINST / data['goals_resume']['FCG_H'],2)
            data['goals_resume']['FG_TIME_A_AGAINST'] = 0 if data['goals_resume']['FCG_A'] == 0 else  round(FG_TIME_A_AGAINST / data['goals_resume']['FCG_A'],2) 
            if data['goals_resume']['FG_TIME_H'] > 0 and data['goals_resume']['FG_TIME_A'] > 0:
                data['goals_resume']['FG_TIME_T'] = round((data['goals_resume']['FG_TIME_H'] + data['goals_resume']['FG_TIME_A'])/2,2)
            else:
                data['goals_resume']['FG_TIME_T'] = round((data['goals_resume']['FG_TIME_H'] + data['goals_resume']['FG_TIME_A']),2) 
            if data['goals_resume']['FG_TIME_H_FOR'] > 0 and data['goals_resume']['FG_TIME_A_FOR'] > 0:
                data['goals_resume']['FG_TIME_T_FOR'] = round((data['goals_resume']['FG_TIME_H_FOR'] + data['goals_resume']['FG_TIME_A_FOR'])/2,2)
            else:
                data['goals_resume']['FG_TIME_T_FOR'] = round((data['goals_resume']['FG_TIME_H_FOR'] + data['goals_resume']['FG_TIME_A_FOR']),2)    
            if data['goals_resume']['FG_TIME_H_AGAINST'] > 0 and data['goals_resume']['FG_TIME_A_AGAINST'] > 0:
                data['goals_resume']['FG_TIME_T_AGAINST'] = round((data['goals_resume']['FG_TIME_H_AGAINST'] + data['goals_resume']['FG_TIME_A_AGAINST'])/2,2)
            else:
                data['goals_resume']['FG_TIME_T_AGAINST'] = round((data['goals_resume']['FG_TIME_H_AGAINST'] + data['goals_resume']['FG_TIME_A_AGAINST']),2)
    return data

def getFirstGoal(f):
        first_goal = {'FOS': '','minute': 0}
        events_home = f['eventsMatch']['h']
        events_away = f['eventsMatch']['a']
        all_events = events_home + events_away
        all_events_sorted = sorted(all_events, key=lambda x: int(x["minute"]))
        filtered_events = [{"h_a": event["h_a"], "minute": event["minute"], "result": event["result"]} for event in all_events_sorted if event["result"] == "Goal"]
        first_goal_event = next((event for event in filtered_events if event["result"] == "Goal"), None)
        if first_goal_event is not None:
            first_goal['FOS'] = first_goal_event['h_a']
            first_goal['minute'] = first_goal_event['minute']
        return first_goal
# Processing
understatService = understat_factory.UnderStatService()
cloudantService = cloudant_db_service.Cloudant()
createIdentityDB()

# Non utilisé
def get_data_fuzzy(array, team):
     for row in array:
          if row['team_dcu'] == team:
               return row
     return None
# Non utilisé
def convert_df_to_array(df,teams_fuzzy):
    array = []
    for index, row in df.iterrows():
          hometeam = get_data_fuzzy(teams_fuzzy,row['HomeTeam'])
          awayteam = get_data_fuzzy(teams_fuzzy,row['AwayTeam'])
          if hometeam and awayteam:
               row_data = {
               'Date':row['Date'],
               'home': hometeam['team'],
               'idhome': hometeam['id'],
               'away': awayteam['team'],
               'idaway': awayteam['id'],
               'FTHG':row['FTHG'],
               'FTAG':row['FTAG'],
               'HTHG':row['HTHG'],
               'HTAG':row['HTAG'],
               'B365H' : row['B365H'],
               'B365D': row['B365D'],
               'B365A':row['B365A'],
               'B365>2.5': row['B365>2.5'],
               'B365<2.5': row['B365<2.5']
                }
               if 'Time' in df.columns:
                row_data['Time'] = row['Time']
               array.append(row_data)
    return array
# Non utilisé
def createHistoricalDB():
     cloudantService.deleteDatabase(opta_constants.dbname_historical_football_data_co_uk)
     cloudantService.createDatabase(opta_constants.dbname_historical_football_data_co_uk)
     for league_fdc in opta_constants.football_data_co_uk_teams:
       for year_fdc in range(opta_constants.football_data_co_uk_year_start, opta_constants.football_data_co_uk_year_end):
        path_fdc = 'datasets/'+league_fdc+'/'+league_fdc+'_'+str(year_fdc)+'.csv'
        data_fdc = pd.read_csv(os.path.join(os.path.dirname(__file__), path_fdc))
        # Liste des équipes de data co-uk
        teams_dcu = list(set((data_fdc['HomeTeam'].tolist() + data_fdc['AwayTeam'].tolist())))
        teams_understat = getHistoricalTeams(league=get_league_name_understat(league_fdc), season=str(year_fdc))
        teams_fuzzy = find_matching_teams_understat(team_dcu=teams_dcu, team_understat=teams_understat)
        doc_fdc = buildDoc(league_fdc+'_'+str(year_fdc),{'count':len(teams_fuzzy),'teams': teams_fuzzy, 'fixtures': convert_df_to_array(data_fdc,teams_fuzzy)})
        cloudantService.persistDoc(dbName=opta_constants.dbname_historical_football_data_co_uk, doc=doc_fdc)
