import numpy as np
from constants import opta_constants
from lib.understat import understat_factory
from lib.cloudant.service import cloudant_db_service
from haversine import haversine, Unit
understatService = understat_factory.UnderStatService()
cloudantService = cloudant_db_service.Cloudant()

# Get all Data GPS 2023
season = '2023'
allDataGPS = cloudantService.getAllGPS(season)

# Position PSG
positionPSG = cloudantService.findPositionTeam('101', allDataGPS)
positionMetz = cloudantService.findPositionTeam('105', allDataGPS)
print(positionPSG)
print(positionMetz)

psg = (positionPSG['latitude'],positionPSG['longitude'])
metz = (positionMetz['latitude'],positionMetz['longitude'])

dist1 = haversine(psg,metz)
print(dist1)
