from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
class ChromeService:
    def closeDriver(self, driver):
        driver.close()
        driver.quit()
    def getDriver(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--ignore-certificate-errors")
        chrome_options.add_argument("--ignore-ssl-errors")
        chrome_options.add_experimental_option('excludeSwitches', ['enable-logging'])
        #driver = webdriver.Chrome("D:/programmation/install/chromedriver_win32/chromedriver_116/chromedriver.exe", options=chrome_options)
        #driver = webdriver.Chrome("---CHROMEDRIVER-PATH---", options=chrome_options)
        driver = webdriver.Chrome('/usr/bin/chromium-browser/chromedriver', options=chrome_options)
        return driver
         
