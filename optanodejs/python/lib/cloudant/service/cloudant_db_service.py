from datetime import datetime
from pickle import TRUE
from ibmcloudant.cloudant_v1 import CloudantV1
from ibm_cloud_sdk_core.api_exception import ApiException
from constants import opta_constants
from lib.understat import understat_factory
import os
from helper import Helper

class Cloudant:
    def getClient(self):
        os.environ['CLOUDANT_URL'] = opta_constants.CLOUDANT_URL
        os.environ['CLOUDANT_APIKEY'] = opta_constants.CLOUDANT_APIKEY
        client = CloudantV1.new_instance(service_name=opta_constants.SERVICE_NAME_CLOUDANT)
        return client
    def persistDoc(self, dbName, doc):
        self.getClient().post_document(db=dbName, document=doc)
    
    def createDatabase(self, dbName):
        try:
            dbInfo = self.getClient().put_database(db=dbName)
            if dbInfo.status_code == 201:
                print('Database '+ dbName +' created')
        except ApiException as apiexc:
            if apiexc.code == 412:
                print('Database '+ dbName +' is already created')
    
    def deleteDatabase(self, dbName):
        try:
            dbInfo = self.getClient().delete_database(db=dbName)
            if dbInfo.status_code == 200:
                print('Database '+ dbName +' deleted')
        except ApiException as apiexc:
            if apiexc.code == 412:
                print('Database '+ dbName +' is already deleted')
    
    
    def updateOrCreateTeamDocument(self, db_name, doc_id, doc_title, fixtureStat, statsData, datesData, allTeamsFromDB, league, season):
        teamDocument = {}
        already_persist = False
        for team in allTeamsFromDB:
            if team['id'] == doc_id:
                teamDocument = team
        if  bool(teamDocument):
            already_persist = True
        if not teamDocument:
            teamDocument['_id'] = doc_id
            teamDocument['nameTeam'] = doc_title
            teamDocument['nameLeague'] = league
            teamDocument['date'] =  datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        retour = self.buildTeamModel(teamDocument, fixtureStat, statsData, datesData, season, already_persist)
        if retour['up_to_date'] == TRUE:
            self.persistDoc(db_name, retour['doc'])
    def buildTeamModel(self, teamDocument, fixtures, statsData, datesData, season, already_persist):
        understatService = understat_factory.UnderStatService()
        helper = Helper.Utils()
        retour = {'doc': teamDocument,'up_to_date': False}
        key = 0
        if already_persist == True:
            teamDocument = teamDocument['doc']
        if already_persist == False:
            teamDocument['fixtures'] = []
            retour['up_to_date'] = TRUE
        if fixtures.get('history') != None:
            key = len(fixtures.get('history'))
        if len(teamDocument['fixtures']) < key:
            allgps = self.getAllGPS(season)
            i = len(teamDocument['fixtures'])
            for h in fixtures.get('history')[i:]:
                match = self.findMatch(teamDocument['_id'],h['h_a'],h['date'],datesData)
                positionFor = self.findPositionTeam(match['id_for'], allgps)
                positionAgainst = self.findPositionTeam(match['id_against'], allgps)
                distance_harvesine = helper.computeDistanceHarvesine(positionFor, positionAgainst)
                teamModel = {}
                teamModel['dayMatch'] = i + 1
                teamModel['idMatch'] = match['idMatch']
                detailMatch = understatService.getInfosByMatch(match['idMatch'])
                teamModel['rosterMatch'] = detailMatch['rosterData']
                teamModel['eventsMatch'] = detailMatch['eventsData']
                teamModel['id_against'] = match['id_against']
                teamModel['name_against'] = match['name_against']
                teamModel['id_for'] = match['id_for']
                teamModel['name_for'] = match['name_for']
                if h['h_a'] == 'a':
                    teamModel['distanceTeam'] = distance_harvesine
                if h['h_a'] == 'h':
                    teamModel['distanceTeam'] = 0
                teamModel['date'] = h['date']
                teamModel['h_a'] = h['h_a']
                teamModel['xg_for'] = h['xG']
                teamModel['xg_against'] = h['xGA']
                teamModel['g_for'] = h['scored']
                teamModel['g_against'] = h['missed']
                teamModel['xpts'] = h['xpts']
                teamModel['pts'] = h['pts']
                teamModel['att_for'] = h['ppda']['att']
                teamModel['def_for'] = h['ppda']['def']
                teamModel['att_against'] = h['ppda_allowed']['att']
                teamModel['def_against'] = h['ppda_allowed']['def']
                teamModel['formation'] = statsData['formation']
                teamModel['gameState'] = statsData['gameState']
                teamModel['situation'] = statsData['situation']
                teamModel['timing'] = statsData['timing']
                i += 1
                teamDocument['fixtures'].append(teamModel)
            retour['up_to_date'] = TRUE
            teamDocument['date'] =  datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            retour['doc'] = teamDocument
        return retour  
    def persistGPSDocument(self, db_name, doc_id, doc_title,league):
        gpsDocument = {}
        try:
            docResult = self.getClient().get_document(db=db_name,doc_id=doc_id)
            gpsDocument = docResult.result
        except ApiException as apiexc:
            print(apiexc.code)
            if apiexc.code == 404:
                gpsDocument['_id'] = doc_id
                gpsDocument['idTeam'] = doc_id
                gpsDocument['nameTeam'] = doc_title
                gpsDocument['nameLeague'] = league
                gpsDocument['longitudeTeam'] = 0
                gpsDocument['latitudeTeam'] = 0
                gpsDocument['date'] =  datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                self.persistDoc(db_name, gpsDocument) 
    
    def persistModelDocument(self, db_name, doc_id,data):
        for t in data:
            modelDocument = {}
            try:
                docResult = self.getClient().get_document(db=db_name,doc_id=t['idTeam'])
                modelDocument = docResult.result
            except ApiException as apiexc:
                if apiexc.code == 404:
                    modelDocument['_id'] = t['idTeam']
                    modelDocument["data"] = t
                    modelDocument['date'] =  datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    self.persistDoc(db_name, modelDocument) 

    def persistIonicDoc(self, db_name, doc_id,data):
        modelDocument = {}
        try:
            docResult = self.getClient().get_document(db=db_name,doc_id=doc_id)
            modelDocument = docResult.result
        except ApiException as apiexc:
            if apiexc.code == 404:
                modelDocument['_id'] = doc_id
                modelDocument["data"] = data
                modelDocument['date'] =  datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                self.persistDoc(db_name, modelDocument) 
    def persistPlayerDocument(self,
     db_name,
     doc_id,
     doc_title,
     team_name,
     id_team,
     league_name,
     allTeamsFromDB,
     playerData):
        playerData['season'].sort(key=lambda x: int(x['games'])) 
        playerDocument = {}
        dayMatch = 0
        exist = False
        for team in allTeamsFromDB:
                if team['id'] == id_team:
                    dayMatch = len(team['doc']['fixtures'])
        try:
            docResult = self.getClient().get_document(db=db_name,doc_id=doc_id)
            playerDocument = docResult.result
            playerDocument['date'] =  datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            exist = any(x['dayMatch'] == dayMatch for x in playerDocument['fixtures'])
            if exist == False:
                playerDocument['fixtures'].append({'dayMatch' : dayMatch, 'playerData':playerData})
                self.persistDoc(db_name, playerDocument) 
        except ApiException as apiexc:
            print(apiexc.code)
            if apiexc.code == 404:
                playerDocument['_id'] = doc_id
                playerDocument['namePlayer'] = doc_title
                playerDocument['nameTeam'] = team_name
                playerDocument['nameLeague'] = league_name
                playerDocument['fixtures'] = []
                playerDocument['date'] =  datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                playerDocument['fixtures'].append({'dayMatch' : dayMatch, 'playerData':playerData})
                self.persistDoc(db_name, playerDocument) 
    
    def findMatch(self, id_team, h_a, datetime, datesData):
        match = {'idMatch':'', 'id_for':'', 'id_against':'', 'name_for':'', 'name_against':'' }
        for t in datesData:
            if h_a == 'h' and t['h']['id'] == id_team and t['datetime'] == datetime:
                match['idMatch'] = t['id']
                match['id_for'] = t['h']['id']
                match['name_for'] = t['h']['title']
                match['id_against'] = t['a']['id']
                match['name_against'] = t['a']['title']
            if h_a == 'a' and t['a']['id'] == id_team and t['datetime'] == datetime:
                match['idMatch'] = t['id']
                match['id_for'] = t['a']['id']
                match['name_for'] = t['a']['title']
                match['id_against'] = t['h']['id']
                match['name_against'] = t['h']['title']
        return match

    def findPositionTeam(self, id_doc, allgps):
        positionGPS = {'longitude': 0,'latitude' : 0 }
        for gpsDocument in allgps:
            if gpsDocument['idTeam'] == id_doc:
                positionGPS['longitude'] = gpsDocument['longitudeTeam']
                positionGPS['latitude'] = gpsDocument['latitudeTeam']
        return positionGPS

    def getAllDataFromDB(self, db_name):
        allData = self.getClient().post_all_docs(
                db=db_name,
                include_docs=bool(True)
                ).get_result()['rows']
        return allData
    def getAllGPS(self, season):
        alldata = self.getClient().post_all_docs(
                db=opta_constants.dbname_gps+'_'+season,
                include_docs=bool(True)
                ).get_result()['rows']
        alldocs = []
        for data in alldata:
            alldocs.append(data['doc'])
        return alldocs
    # NBA 
    def persistPlayerNBA(self,
     db_name,
     doc_id,
     team_name,
     dayMatch,
     playerData):
        # Delete name attribute
        del playerData['name']
        playerDocument = {}
        exist = False
        try:
            docResult = self.getClient().get_document(db=db_name,doc_id=doc_id)
            playerDocument = docResult.result
            playerDocument['date'] =  datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            exist = any(x['dayMatch'] == dayMatch for x in playerDocument['fixtures'])
            if exist == False:
                playerDocument['fixtures'].append({'dayMatch' : dayMatch, 'playerData':playerData})
                self.persistDoc(db_name, playerDocument) 
        except ApiException as apiexc:
            print(apiexc.code)
            if apiexc.code == 404:
                playerDocument['_id'] = doc_id
                playerDocument['namePlayer'] = doc_id
                playerDocument['nameTeam'] = team_name
                playerDocument['fixtures'] = []
                playerDocument['date'] =  datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                playerDocument['fixtures'].append({'dayMatch' : dayMatch, 'playerData':playerData})
                self.persistDoc(db_name, playerDocument) 
    
    