import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.message import MIMEMessage
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

class MailHelperService:

    # SSL Context
    def getSSLContext(self):
        context = ssl.create_default_context()
        return context

    # Create Message
    def createMessage(self, user, to, subject, content):
        message = MIMEMultipart('mixed')
        message['From'] = 'Contact <{sender}>'.format(sender = user)
        message['To'] = to
        message['Subject'] = subject
        msg_content = content
        body = MIMEText(msg_content, 'html')
        message.attach(body)
        return message
    
    # Add Attachment to Message
    def addAttachment(self, message, attachmentPath, fileName, subtype):
        try:
            with open(attachmentPath, "rb") as attachment:
                p = MIMEApplication(attachment.read(),_subtype=subtype)
                p.add_header('Content-Disposition', "attachment; filename= %s" % fileName)
                message.attach(p)
        except Exception as e:
            print(str(e))
        return message

    def sendMail(self, smtp_server, smtp_port, context, user, password, message):
        with smtplib.SMTP(smtp_server, smtp_port) as server:
            server.ehlo()
            server.starttls(context=context)
            server.ehlo()
            server.login(user, password)
            server.sendmail(user,user.split(";"),message)
            server.quit()
        print("email sent out successfully")