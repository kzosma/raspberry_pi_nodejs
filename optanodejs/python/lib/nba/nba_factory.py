import pandas as pd
from urllib.request import urlopen
from bs4 import BeautifulSoup
class NbaService:
    
    def getStats(self, url):
        html = urlopen(url)
        soup = BeautifulSoup(html, 'html.parser')
        soup.findAll('tr', limit=2)
        headers = [th.getText() for th in soup.findAll('tr', limit=2)[0].findAll('th')]
        headers = headers[1:]
        rows = soup.findAll('tr')[1:]
        player_stats = [[td.getText() for td in rows[i].findAll('td')]
                        for i in range(len(rows))]
        stats = pd.DataFrame(player_stats, columns = headers)
        # print(len(player_stats))
        return player_stats
    
    def getStatsShooting(self, url):
        html = urlopen(url)
        soup = BeautifulSoup(html, 'html.parser')
        soup.findAll('tr', limit=2)
        headers = [th.getText() for th in soup.findAll('tr', limit=2)[1].findAll('th')]
        headers = headers[1:]
        headers = [x for x in headers if x != '\xa0']
        rows = soup.findAll('tr')[2:]
        player_stats = [[td.getText() for td in rows[i].findAll('td')]
                        for i in range(len(rows))]
        return player_stats
    
    def getTeamsName(self, player_stats):
        teamsName = []
        for i in range(len(player_stats)):
            if len(player_stats[i]) > 0:
                playerjson = {'name': player_stats[i][0]}
                teamsName.append(player_stats[i][3])
                teamsName = list(dict.fromkeys(teamsName))
        return teamsName
    
    def getAllStatsPerPoss(self, teamsName, player_stats):
        allStats = []
        for team in teamsName:
            teamJson = {'team': team, 'players': []}
            for i in range(len(player_stats)):
                if len(player_stats[i]) > 0 and player_stats[i][3] == team:
                    playerjson = {'name': player_stats[i][0],
                                  'age': player_stats[i][2],
                                  'games': player_stats[i][4],
                                  'minutes_played_per_game': player_stats[i][6],
                                  'field_goal_per_game': player_stats[i][7],
                                  'field_goal_attempt_per_game': player_stats[i][8],
                                  'three_points_field_goal_per_game': player_stats[i][10],
                                  'three_points_field_goal_attempt_per_game': player_stats[i][11],
                                  'two_points_field_goal_per_game': player_stats[i][13],
                                  'two_points_field_goal_attempt_per_game': player_stats[i][14],
                                  'offensive_rating_per_game': player_stats[i][29],
                                  'defensive_rating_per_game': player_stats[i][30]
                                 }
                    teamJson['players'].append(playerjson)
            allStats.append(teamJson)        
        return allStats
    
    def getAllStats(self, teamsName, player_stats):
        allStats = []
        for team in teamsName:
            teamJson = {'team': team, 'players': []}
            for i in range(len(player_stats)):
                if len(player_stats[i]) > 0 and player_stats[i][3] == team:
                    playerjson = {'name': player_stats[i][0],
                                  'position': player_stats[i][1],
                                  'age': player_stats[i][2],
                                  'games': player_stats[i][4],
                                  'games_started': player_stats[i][5],
                                  'minutes_played_per_game': player_stats[i][6],
                                  'field_goal_per_game': player_stats[i][7],
                                  'field_goal_attempt_per_game': player_stats[i][8],
                                  'field_goal_percent_per_game': player_stats[i][9],
                                  'three_points_field_goal_per_game': player_stats[i][10],
                                  'three_points_field_goal_attempt_per_game': player_stats[i][11],
                                  'three_points_field_goal_percent_per_game': player_stats[i][12],
                                  'two_points_field_goal_per_game': player_stats[i][13],
                                  'two_points_field_goal_attempt_per_game': player_stats[i][14],
                                  'two_points_field_goal_percent_per_game': player_stats[i][15],
                                  'effective_field_goal_percent_per_game': player_stats[i][16],
                                  'free_throws_per_game': player_stats[i][17],
                                  'free_throws_attempt_per_game': player_stats[i][18],
                                  'free_throws_percent_per_game': player_stats[i][19],
                                  'offensive_rebound_per_game': player_stats[i][20],
                                  'defensive_rebound_per_game': player_stats[i][21],
                                  'total_rebound_per_game': player_stats[i][22],
                                  'assists_per_game': player_stats[i][23],
                                  'steal_per_game': player_stats[i][24],
                                  'block_per_game': player_stats[i][25],
                                  'turnover_per_game': player_stats[i][26],
                                  'personal_fouls_per_game': player_stats[i][27],
                                  'points_per_game': player_stats[i][28]
                                 }
                    teamJson['players'].append(playerjson)
            allStats.append(teamJson)        
        return allStats
    
    def getAllStatsShooting(self, teamsName, player_stats):
        allStats = []
        for team in teamsName:
            teamJson = {'team': team, 'players': []}
            for i in range(len(player_stats)):
                if len(player_stats[i]) > 0 and player_stats[i][3] == team:
                    playerjson = {'name': player_stats[i][0],
                                  'position': player_stats[i][1],
                                  'age': player_stats[i][2],
                                  'games': player_stats[i][4],
                                  'minutes_played': player_stats[i][5],
                                  'field_goal_percent': player_stats[i][6],
                                  'field_goal_average_distance': player_stats[i][7],
                                  'field_goal_attempt_2P_percent': player_stats[i][9],
                                  'field_goal_attempt_0_3_percent': player_stats[i][10],
                                  'field_goal_attempt_3_10_percent': player_stats[i][11],
                                  'field_goal_attempt_10_16_percent': player_stats[i][12],
                                  'field_goal_attempt_16_3P_percent': player_stats[i][13],
                                  'field_goal_attempt_3P_percent': player_stats[i][14],
                                  
                                  'field_goal_2P_percent': player_stats[i][16],
                                  'field_goal_0_3_percent': player_stats[i][17],
                                  'field_goal_3_10_percent': player_stats[i][18],
                                  'field_goal_10_16_percent': player_stats[i][19],
                                  'field_goal_16_3P_percent': player_stats[i][20],
                                  'field_goal_3P_percent': player_stats[i][21],
                                  
                                  'field_goal_assist_2P_percent': player_stats[i][23],
                                  'field_goal_assist_3P_percent': player_stats[i][24],
                                  
                                  'field_goal_attempt_dunk_percent': player_stats[i][26],
                                  'field_goal_dunk_number': player_stats[i][27],
                                  'field_goal_attempt_corner3P_percent': player_stats[i][29],
                                  'field_goal_corner3P_percent': player_stats[i][30]
                                  
                                 }
                    teamJson['players'].append(playerjson)
            allStats.append(teamJson)        
        return allStats