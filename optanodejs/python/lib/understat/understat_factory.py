import requests
from requests.adapters import HTTPAdapter
from urllib3.util import Retry
import json
import ast
import time
from bs4 import BeautifulSoup
from helper import Helper
from constants import opta_constants
from os import path
from lib.cloudant.service import cloudant_db_service
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from lib.chrome import chrome_service

class UnderStatService:


    # Get Opta Players
    def getOptaPlayersData(self, league, season):
        filter = {'type': ''}
        driver = chrome_service.ChromeService().getDriver()
        data = self.getInfosByLeague(league+'/'+season, filter, driver)
        chrome_service.ChromeService().closeDriver(driver)
        teams = data['teams']
        teamsName = self.getTeamsName(teams)
        teamsData = json.loads(data['allteams'])
        print(len(teamsData))
        cloudantService = cloudant_db_service.Cloudant()
        cloudantService.createDatabase(opta_constants.dbname_players+'_'+season)
        allTeamsFromDB = cloudantService.getAllDataFromDB(opta_constants.dbname_teams+'_'+season)
        for team in teamsName:
            print('Traitement équipe : ' + team['Team'])
            driver = chrome_service.ChromeService().getDriver()
            infosTeam = self.getInfosByTeam(team['Team'],season, driver)
            chrome_service.ChromeService().closeDriver(driver)
            # Liste des joueurs de l'équipe
            playersData = self.getPlayersList(json.loads(infosTeam['playersData']))
            for player in playersData:
                driver = chrome_service.ChromeService().getDriver()
                statsPlayer = self.getInfosByPlayer(player['id_player'], season, driver)
                chrome_service.ChromeService().closeDriver(driver)
                cloudantService.persistPlayerDocument(
                    opta_constants.dbname_players+'_'+season,
                    player['id_player'],
                    player['player_name'],
                    team['Team'],
                    team['id'],
                    league,
                    allTeamsFromDB,
                    statsPlayer
                    )
    # Get Opta Data GPS
    def getOptaDataGPS(self, league, season):
        filter = {'type': ''}
        driver = chrome_service.ChromeService().getDriver()
        data = self.getInfosByLeague(league+'/'+season, filter, driver)
        chrome_service.ChromeService().closeDriver(driver)
        teams = data['teams']
        teamsListName = self.getTeamsName(teams)
        print("--------------------------------")
        print(season + " - " + league);
        cloudantService = cloudant_db_service.Cloudant()
        cloudantService.createDatabase(opta_constants.dbname_gps+'_'+season)
        for team in teamsListName:
            time.sleep(0.1)
            cloudantService.persistGPSDocument(opta_constants.dbname_gps+'_'+season, team['id'], team['Team'],league)
            print(team['Team'])
        driver.close()
        driver.quit()
    # Get Opta Data Ionic
    def getOptaNextMatchesIonic(self, league, season):
        nextMatches = []
        filter = {'type': ''}
        driver = chrome_service.ChromeService().getDriver()
        data = self.getInfosByLeague(league+'/'+season, filter, driver)
        chrome_service.ChromeService().closeDriver(driver)
        allDates = json.loads(data['dates'])
        allDates_filtered = [d for d in allDates if d['isResult'] == False]
        for dateF in allDates_filtered:
            nextMatch = {'id': dateF['id'], 'home': dateF['h'], 'away': dateF['a'], 'datetime': dateF['datetime']}
            nextMatches.append(nextMatch)
        return nextMatches
    
    # Get Opta Data
    def getOptaData(self, league, season):
        cloudantService = cloudant_db_service.Cloudant();
        filter = {'type': ''}
        driver = chrome_service.ChromeService().getDriver()
        data = self.getInfosByLeague(league+'/'+season, filter, driver)
        chrome_service.ChromeService().closeDriver(driver)
        datesData = json.loads(data['dates'])
        # Chargement des équipes de la league en cours
        teams = data['teams']
        teamsName = self.getTeamsName(teams)
        print(season)
        teamsData = json.loads(data['allteams'])
        print(len(teamsData))
        cloudantService.createDatabase(opta_constants.dbname_teams+'_'+season)
        allTeamsFromDB = cloudantService.getAllDataFromDB(opta_constants.dbname_teams+'_'+season)
        for team in teamsName:
            print('Traitement équipe : ' + team['Team'])
            fixtureStat = self.getFixtureDataByTeam(teamsData,team['id'])
            driver = chrome_service.ChromeService().getDriver()
            infosTeam = self.getInfosByTeam(team['Team'],season, driver)
            chrome_service.ChromeService().closeDriver(driver)
            statsData = json.loads(infosTeam['statisticsData'])
            cloudantService.updateOrCreateTeamDocument(opta_constants.dbname_teams+'_'+season, team['id'], team['Team'], fixtureStat, statsData, datesData, allTeamsFromDB, league, season)
            # Liste des joueurs de l'équipe
            # playersData = json.loads(infosTeam['playersData'])
    # League : Classement des équipes, statistiques des joueurs de la ligue, calendrier et résultats des équipes
    def getInfosByLeague(self, league, filter, driver):
        helper = Helper.Utils()
        infos = {'teams': {}, 'allteams': {}, 'players': {}, 'dates': {}}
        url = opta_constants.base_url_league+'/'+league
        driver.get(url=url)
        soup = BeautifulSoup(driver.page_source, 'lxml')
        scripts = soup.find_all('script')
        for script in scripts:
            if 'teamsData' in script.text:
                teams_data = helper.extractDataFromScript(script.text)
                infos['teams'] = helper.getStatsDataForLeague(ast.literal_eval(teams_data), filter)
                infos['allteams'] = teams_data
            if 'playersData' in script.text:
                infos['players'] = helper.extractDataFromScript(script.text)
            if 'datesData' in script.text:
                infos['dates'] = helper.extractDataFromScript(script.text)
        return infos
    
    # Team : Calendrier et résultats de l'équipe, statistiques de l'équipe, statistiques des joueurs de l'équipe 
    def getInfosByTeam(self, team, season, driver):
        helper = Helper.Utils()
        infos = {'datesData': {}, 'statisticsData': {}, 'playersData': {}, 'teamsData': {}}
        url = opta_constants.base_url_team+'/'+team+'/'+season
        driver.get(url=url)
        soup = BeautifulSoup(driver.page_source, "lxml")
        scripts = soup.find_all('script')
        for script in scripts:
            if 'datesData' in script.text:
                datesData = helper.extractDataFromScript(script.text)
                infos['datesData'] = datesData
            if 'statisticsData' in script.text:
                statisticsData = helper.extractDataFromScript(script.text)
                infos['statisticsData'] = statisticsData
            if 'playersData' in script.text:
                playersData = helper.extractDataFromScript(script.text)
                infos['playersData'] = playersData
        return infos

    def getInfosByMatch(self, idMatch):
        detailMatch = {'rosterData':{}, 'eventsData': {}}
        helper = Helper.Utils()
        url = opta_constants.base_url_match+'/'+idMatch
        driver = chrome_service.ChromeService().getDriver()
        driver.get(url=url)
        soup = BeautifulSoup(driver.page_source, "lxml")
        chrome_service.ChromeService().closeDriver(driver)
        scripts = soup.find_all('script')
        for script in scripts:
            if 'rostersData' in script.text:
                roster_data = helper.extractDataFromScript(script.text)
                detailMatch['rosterData'] = json.loads(roster_data)
            if 'shotsData' in script.text:
                events_data = helper.extractDataFromScript(script.text)
                detailMatch['eventsData'] = json.loads(events_data)
        return detailMatch
    
    # Récupération des statistiques du joueur
    def getInfosByPlayer(self, idPlayer, season,driver):
        statPlayer = {
         'season': [],
         'position': {},
         'situation': {},
         'shotZones': {},
         'shotTypes': {}
          }
        helper = Helper.Utils()
        url = opta_constants.base_url_player+'/'+idPlayer
        driver.get(url=url)
        soup = BeautifulSoup(driver.page_source, "lxml")
        scripts = soup.find_all('script')
        for script in scripts:
            if 'groupsData' in script.text:
                groupsData = json.loads(helper.extractDataFromScript(script.text))
                # Season
                for seasonData in groupsData['season']:
                    if seasonData['season'] == season:
                        statPlayer['season'].append(seasonData)
                # Position
                if season in groupsData['position']:
                    positionData = groupsData['position'][season]
                    statPlayer['position'] = positionData
                # Situation
                if season in groupsData['situation']:
                    situationData = groupsData['situation'][season]
                    statPlayer['situation'] = situationData
                # ShotZones
                if season in groupsData['shotZones']:
                    shotZonesData = groupsData['shotZones'][season]
                    statPlayer['shotZones'] = shotZonesData
                # ShotTypes
                if season in groupsData['shotTypes']:
                    shotTypesData = groupsData['shotTypes'][season]
                    statPlayer['shotTypes'] = shotTypesData
        return statPlayer
    
    # API : Récupération des statistiques des joueurs d'une équipe
    def getPlayersStats(self, filter):
        url = opta_constants.base_url_players_stats
        res = requests.post(url, data = filter)
        print(res.text)
    
    # API : Récupération des statistiques d'un joueur
    def getPlayerStats(self, id):
        helper = Helper.Utils()
        infos = {'groupsData': {}, 'minMaxPlayerStats': {}, 'shotsData': {}, 'matchesData': {}}
        url = opta_constants.base_url_player_stats + id
        driver = chrome_service.ChromeService().getDriver()
        driver.get(url=url)
        soup = BeautifulSoup(driver.page_source, "lxml")
        chrome_service.ChromeService().closeDriver(driver)
        scripts = soup.find_all('script')
        for script in scripts:
            if 'groupsData' in script.text:
                groupsData = helper.extractDataFromScript(script.text)
                infos['groupsData'] = json.loads(groupsData)
            if 'minMaxPlayerStats' in script.text:
                minMaxPlayerStats = helper.extractDataFromScript(script.text)
                infos['minMaxPlayerStats'] = json.loads(minMaxPlayerStats)
            if 'shotsData' in script.text:
                shotsData = helper.extractDataFromScript(script.text)
                infos['shotsData'] = json.loads(shotsData)
            if 'matchesData' in script.text:
                matchesData = helper.extractDataFromScript(script.text)
                infos['matchesData'] = json.loads(matchesData)
        return infos

    def getFixtureDataByTeam(self, teamsData, infosTeamID):
        response = {}
        for teamdata in teamsData:
            if teamsData[teamdata]['id'] == infosTeamID:
                response = teamsData[teamdata]
        return response
    # Utilitaire : retourne la liste des noms des équipes de la league
    def getTeamsName(self, arg):
        teamsName = []
        for team in arg:
            team['Team'] = team['Team'].replace(' ','_')
            teamsName.append(team)
        return teamsName
    def findByKey(self, arr , key):
        valueKey = key
        for x in arr:
            if x["key"] == key:
                valueKey = x['value']
        return valueKey
    # Utilitaire : retourne la liste des joueurs des équipes de la league
    def getPlayersList(self, playersData):
        playersList = []
        for player in playersData:
            playersList.append({'id_player':player['id'],'player_name':player['player_name']})
        return playersList
    # Get N Last Matchs
    def getNLastMatchs(self, list_teams,poste):
        for team in list_teams:
            for k_nearest_national_team in team['k_nearest_national_teams'][poste]:
                last_matchs =  [t['last_matchs'] for t in list_teams if t['idTeam'] == k_nearest_national_team['id']][0]
                n = 5
                if len(last_matchs) < 5:
                    n = len(last_matchs)
                n_last_matchs = last_matchs[-n:]
                k_nearest_national_team['rank'] = n_last_matchs[0]["own_rank"] 
                for n_team in n_last_matchs:
                    if n_team['h_a'] == 'h':
                        k_nearest_national_team['BP'] = k_nearest_national_team['BP'] + n_team['home_gfor_ft'] 
                        k_nearest_national_team['BC'] = k_nearest_national_team['BC'] + n_team['away_gfor_ft'] 
                        if n_team['home_gfor_ft'] == n_team['away_gfor_ft'] :
                            k_nearest_national_team['Pts'] = k_nearest_national_team['Pts'] + 1
                        if n_team['home_gfor_ft'] > n_team['away_gfor_ft'] :
                            k_nearest_national_team['Pts'] = k_nearest_national_team['Pts'] + 3

                    if n_team['h_a'] == 'a':
                        k_nearest_national_team['BP'] = k_nearest_national_team['BP'] + n_team['away_gfor_ft'] 
                        k_nearest_national_team['BC'] = k_nearest_national_team['BC'] + n_team['home_gfor_ft'] 
                        if n_team['home_gfor_ft'] == n_team['away_gfor_ft'] :
                            k_nearest_national_team['Pts'] = k_nearest_national_team['Pts'] + 1
                        if n_team['home_gfor_ft'] < n_team['away_gfor_ft'] :
                            k_nearest_national_team['Pts'] = k_nearest_national_team['Pts'] + 3
        return list_teams
    
