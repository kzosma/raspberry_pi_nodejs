from lib.cloudant.service import cloudant_db_service
from lib.understat import understat_factory
from constants import opta_constants
from ibm_cloud_sdk_core.api_exception import ApiException
from optadatalib.opta_ai_service import OptaAi
from optadatalib.Helper import Utils
import pandas as pd
import numpy as np
import statsmodels.formula.api as smf
from optadatalib.Helper import Utils
from collections import namedtuple 


# Functions
def getPythagoreanStat(team):
        df_cloudant = pd.json_normalize(team)
        df_cloudant_data = []
        for fixtures in df_cloudant['doc.fixtures']:
            for fixture in fixtures:
                fixture_data = {}
                fixture_data['idMatch'] = fixture['idMatch']
                fixture_data['date'] = fixture['date']
                fixture_data['HomeTeam'] = np.array2string(np.where(fixture['h_a']=='h', fixture['name_for'],fixture['name_against']))
                fixture_data['AwayTeam'] = np.array2string(np.where(fixture['h_a']=='a', fixture['name_for'],fixture['name_against']))
                fixture_data['HomeGoal'] = np.where(fixture['h_a']=='h', fixture['g_for'],fixture['g_against']).sum()
                fixture_data['AwayGoal'] = np.where(fixture['h_a']=='a', fixture['g_for'],fixture['g_against']).sum()
                df_cloudant_data.append(fixture_data)
        df_cloudant_data_8 = pd.json_normalize(df_cloudant_data)
        df_cloudant_data_8['hwin'] = np.where(df_cloudant_data_8['HomeGoal'] > df_cloudant_data_8['AwayGoal'],1,0)
        df_cloudant_data_8['awin'] = np.where(df_cloudant_data_8['HomeGoal'] < df_cloudant_data_8['AwayGoal'],1,0)
        df_cloudant_data_8['count'] = 1
        df_cloudant_data_8_home = df_cloudant_data_8.groupby('HomeTeam')[['hwin','HomeGoal','AwayGoal','count']].sum().reset_index()
        df_cloudant_data_8_team_home = df_cloudant_data_8_home[df_cloudant_data_8_home['count'] > 1] 
        df_cloudant_data_8_team_home = df_cloudant_data_8_team_home.rename(columns={'HomeTeam':'team','HomeGoal':'HomeGH', 'AwayGoal':'HomeGA', 'count':'MH'})
        df_cloudant_data_8_away = df_cloudant_data_8.groupby('AwayTeam')[['awin','HomeGoal','AwayGoal','count']].sum().reset_index()
        df_cloudant_data_8_team_away = df_cloudant_data_8_away[df_cloudant_data_8_away['count'] > 1] 
        df_cloudant_data_8_team_away = df_cloudant_data_8_team_away.rename(columns={'AwayTeam':'team','HomeGoal':'AwayGH', 'AwayGoal':'AwayGA', 'count':'MA'})
        df_cloudant_data_8 = pd.merge(df_cloudant_data_8_team_home, df_cloudant_data_8_team_away,on='team')
        df_cloudant_data_8['M'] = df_cloudant_data_8['MH'] + df_cloudant_data_8['MA']
        df_cloudant_data_8['W'] = df_cloudant_data_8['hwin'] + df_cloudant_data_8['awin']
        df_cloudant_data_8['GF'] = df_cloudant_data_8['HomeGH'] + df_cloudant_data_8['AwayGA']
        df_cloudant_data_8['GA'] = df_cloudant_data_8['HomeGA'] + df_cloudant_data_8['AwayGH']
        df_cloudant_data_8['wpc'] = df_cloudant_data_8['W'] / df_cloudant_data_8['M']
        df_cloudant_data_8['pyth'] = df_cloudant_data_8['GF']**2 / (df_cloudant_data_8['GF']**2 + df_cloudant_data_8['GA']**2)
        M=df_cloudant_data_8['M']   
        W=df_cloudant_data_8['W']
        GF=df_cloudant_data_8['GF']
        GA=df_cloudant_data_8['GA']
        wpc=df_cloudant_data_8['wpc']
        pyth=df_cloudant_data_8['pyth']
        PythExpectation = namedtuple('PythExpectation', 'M W GF GA wpc pyth')
        pe = PythExpectation(M=M, W=W, GF=GF, GA=GA, wpc=wpc, pyth=pyth)
        return pe

def getPythagoreanExpectation(data):
        df_cloudant = pd.json_normalize(data)
        df_cloudant_data = []
        for fixtures in df_cloudant['doc.fixtures']:
            for fixture in fixtures:
                fixture_data = {}
                fixture_data['idMatch'] = fixture['idMatch']
                fixture_data['date'] = fixture['date']
                fixture_data['HomeTeam'] = np.array2string(np.where(fixture['h_a']=='h', fixture['name_for'],fixture['name_against']))
                fixture_data['AwayTeam'] = np.array2string(np.where(fixture['h_a']=='a', fixture['name_for'],fixture['name_against']))
                fixture_data['HomeGoal'] = np.where(fixture['h_a']=='h', fixture['g_for'],fixture['g_against']).sum()
                fixture_data['AwayGoal'] = np.where(fixture['h_a']=='a', fixture['g_for'],fixture['g_against']).sum()
                df_cloudant_data.append(fixture_data)
        df_cloudant_data_8 = pd.json_normalize(Utils.remove_duplicates_by_property(df_cloudant_data, 'idMatch'))
        df_cloudant_data_8['hwin'] = np.where(df_cloudant_data_8['HomeGoal'] > df_cloudant_data_8['AwayGoal'],1,0)
        df_cloudant_data_8['awin'] = np.where(df_cloudant_data_8['HomeGoal'] < df_cloudant_data_8['AwayGoal'],1,0)
        df_cloudant_data_8['count'] = 1
        df_cloudant_data_8_home = df_cloudant_data_8.groupby('HomeTeam')[['hwin','HomeGoal','AwayGoal','count']].sum().reset_index()
        df_cloudant_data_8_home = df_cloudant_data_8_home.rename(columns={'HomeTeam':'team','HomeGoal':'HomeGH', 'AwayGoal':'HomeGA', 'count':'MH'})
        df_cloudant_data_8_away = df_cloudant_data_8.groupby('AwayTeam')[['awin','HomeGoal','AwayGoal','count']].sum().reset_index()
        df_cloudant_data_8_away = df_cloudant_data_8_away.rename(columns={'AwayTeam':'team','HomeGoal':'AwayGH', 'AwayGoal':'AwayGA', 'count':'MA'})
        df_cloudant_data_8 = pd.merge(df_cloudant_data_8_home, df_cloudant_data_8_away,on='team')
        df_cloudant_data_8['M'] = df_cloudant_data_8['MH'] + df_cloudant_data_8['MA']
        df_cloudant_data_8['W'] = df_cloudant_data_8['hwin'] + df_cloudant_data_8['awin']
        df_cloudant_data_8['GF'] = df_cloudant_data_8['HomeGH'] + df_cloudant_data_8['AwayGA']
        df_cloudant_data_8['GA'] = df_cloudant_data_8['HomeGA'] + df_cloudant_data_8['AwayGH']
        df_cloudant_data_8['wpc'] = df_cloudant_data_8['W'] / df_cloudant_data_8['M']
        df_cloudant_data_8['pyth'] = df_cloudant_data_8['GF']**2 / (df_cloudant_data_8['GF']**2 + df_cloudant_data_8['GA']**2)
        if df_cloudant_data_8.empty:
            return None
        pyth_lm = smf.ols(formula='wpc ~ pyth', data=df_cloudant_data_8).fit()
        df = pd.read_html(pyth_lm.summary().tables[1].as_html(),header=0,index_col=0)[0]
        pyth=df['coef'].values[1]
        intercept = df['coef'][0]
        confidence = df['P>|t|'][1]
        PythExpectation = namedtuple('PythExpectation', 'pyth intercept confidence')
        pe = PythExpectation(pyth=pyth, intercept=intercept, confidence=confidence)
        return pe
current_season = opta_constants.seasons[0]
cloudantService = cloudant_db_service.Cloudant()
understatService = understat_factory.UnderStatService()
allteams = cloudantService.getAllDataFromDB(opta_constants.dbname_teams+'_'+current_season)
cloudantService.deleteDatabase(opta_constants.model_team_classements+'_'+current_season)
cloudantService.createDatabase(opta_constants.model_team_classements+'_'+current_season)
model_teams_classement = []
model_teams_ranks = []
model_teams_classement_handicaps = []
model_teams_classement_domicile = []
model_teams_classement_exterieur = []


for team in allteams:
    docWS = {}
    # idDoc
    idDoc = team['doc']['_id']
    # fixture
    fixtures = team['doc']['fixtures']
    if not fixtures:
        continue
    team_pyth = Utils.get_team_data_by_id(data=allteams, id=idDoc)
    pe_team = getPythagoreanStat(team_pyth)
    teams_league = Utils.get_team_data_by_league(data=allteams, league=team['doc']['nameLeague'])
    pe_league = getPythagoreanExpectation(teams_league)
    if pe_league is not None:
        try:
            position_team = Utils.get_position_pyth_exp_point(pyth=pe_team.pyth, wpc=pe_team.wpc, a=pe_league.pyth, b=pe_league.intercept)
        except Exception as exc:
            PythExpectPoint = namedtuple('PythExpectPoint', 'pyth_origin wpc_origin wpc_calc')
            position_team = PythExpectPoint(pyth_origin=0, wpc_origin=0, wpc_calc=0)
    else:
        PythExpectPoint = namedtuple('PythExpectPoint', 'pyth_origin wpc_origin wpc_calc')
        position_team = PythExpectPoint(pyth_origin=0, wpc_origin=0, wpc_calc=0)
    try:
        docWS = cloudantService.getClient().get_document(db=opta_constants.model_team_whoscored+'_'+current_season,doc_id=idDoc).get_result()
    except ApiException as apiexc:
            if apiexc.code == 404:
                print('Document ' + str(idDoc) +' not found')
    # modèle classement
    matchs = team['doc']['fixtures']
    lf_model_team = {
            'idTeam':idDoc,
            'team':team['doc']['nameTeam'],
            'league':team['doc']['nameLeague'],
            'Pts': 0,
            'J': 0,
            'G': 0,
            'N': 0,
            'P': 0,
            'Bp': 0,
            'Bc': 0,
            'Diff': 0,
            'XgChain' : 0,
            'XgBuildup' : 0,
            'Sorare_Pts' : 0,
            'Pts_Dom': 0,
            'J_Dom': 0,
            'G_Dom': 0,
            'N_Dom': 0,
            'P_Dom': 0,
            'Bp_Dom': 0,
            'Bc_Dom': 0,
            'Diff_Dom': 0,
            'Pts_Ext': 0,
            'J_Ext': 0,
            'G_Ext' : 0,
            'N_Ext': 0,
            'P_Ext' : 0,
            'Bp_Ext': 0,
            'Bc_Ext': 0,
            'Diff_Ext': 0,
            'pyth_exp': position_team.pyth_origin,
            'wpc_exp': position_team.wpc_origin,
            'wpc_calc': position_team.wpc_calc,
            'last_matchs': [],
            'k_nearest_national_teams': {'def': [], 'mil': [], 'att_mil': [], 'att': [], 'sub': [], 'total': []},
            '3_best_scorer': [],
            '3_best_assists': [],
            'favorites': [],
            'synthese_1_5': {'G': 0, 'N': 0, 'P': 0, 'BP': 0, 'BC': 0},
            'synthese_5_10': {'G': 0, 'N': 0, 'P': 0, 'BP': 0, 'BC': 0},
            'synthese_10_end': {'G': 0, 'N': 0, 'P': 0, 'BP': 0, 'BC': 0}
            }
    # modèle classement handicap
    lf_model_handicap_team = {
            'idTeam': idDoc,
            'team':team['doc']['nameTeam'],
            'league':team['doc']['nameLeague'],
            'V1': 0,
            'V2': 0,
            'V>2' : 0,
            'D1': 0,
            'D2' : 0,
            'D>2': 0,
            'rank': 0
    }
    matchs_dom = 0
    matchs_ext = 0
    players_stats_list = []
    lineups_win = []
    lineups_deuce = []
    lineups_lose = []
    for match in matchs:
        lineups = []
        last_match = {
        'dayMatch': match['dayMatch'],     
        'idMatch': match['idMatch'],
        'own_name':match['name_for'],
        'against_name':match['name_against'],
        'own_id':match['id_for'],
        'against_id':match['id_against'],
        'h_a': match['h_a'],
        'home_gfor_mt': 0,
        'away_gfor_mt': 0,
        'home_gfor_ft': 0,
        'away_gfor_ft': 0,
        'own_rank': 0,
        'against_rank': 0,
        'home_xg': 0,
        'away_xg': 0,
        'home_shots': 0,
        'away_shots': 0,
        'home_bigchance': 0,
        'away_bigchance': 0,
        'home_bigchance_missed': 0,
        'away_bigchance_missed': 0
        }
        # EventsMatch Home
        for evt_h in match['eventsMatch']['h']:
            if evt_h['result'] == 'Goal' and int(evt_h['minute']) < 45:
                last_match['home_gfor_mt'] = last_match['home_gfor_mt'] + 1
            if evt_h['result'] == 'Goal':
                last_match['home_gfor_ft'] = last_match['home_gfor_ft'] + 1
            
            if float(evt_h['xG']) >= 0.5 and evt_h['result'] == 'MissedShots':
                last_match['home_bigchance_missed'] = last_match['home_bigchance_missed'] + 1
            if float(evt_h['xG']) >= 0.5 and (evt_h['result'] == 'ShotOnPost' or evt_h['result'] == 'SavedShot'):
                last_match['home_bigchance'] = last_match['home_bigchance'] + 1
        
        # EventsMatch Away
        for evt_a in match['eventsMatch']['a']:
            if evt_a['result'] == 'Goal' and int(evt_a['minute']) < 45:
                last_match['away_gfor_mt'] = last_match['away_gfor_mt'] + 1
            if evt_a['result'] == 'Goal':
                last_match['away_gfor_ft'] = last_match['away_gfor_ft'] + 1

            if float(evt_a['xG']) >= 0.5 and evt_a['result'] == 'MissedShots':
                last_match['away_bigchance_missed'] = last_match['away_bigchance_missed'] + 1
            if float(evt_a['xG']) >= 0.5 and (evt_a['result'] == 'ShotOnPost' or evt_a['result'] == 'SavedShot'):
                last_match['away_bigchance'] = last_match['away_bigchance'] + 1
        
        # RosterMatch Home
        for roster_h in match['rosterMatch']['h']:
            last_match['home_xg'] = last_match['home_xg'] + float(match['rosterMatch']['h'][roster_h]['xG'])
            last_match['home_shots'] = last_match['home_shots'] + float(match['rosterMatch']['h'][roster_h]['shots'])
            if match['h_a'] == 'h':
                if int(match['rosterMatch']['h'][roster_h]['time']) > 15:
                    lineups.append({'id':match['rosterMatch']['h'][roster_h]['player_id'],'name':match['rosterMatch']['h'][roster_h]['player']})
                player_list_h = list(filter(lambda t: t["id"] == match['rosterMatch']['h'][roster_h]['player_id'], players_stats_list))
                player_current_h = {
                     'id': match['rosterMatch']['h'][roster_h]['player_id'],
                     'name': match['rosterMatch']['h'][roster_h]['player'],
                     'xgchain_home': float(match['rosterMatch']['h'][roster_h]['xGChain']),
                     'xgchain_away': 0,
                     'xgchain_total': float(match['rosterMatch']['h'][roster_h]['xGChain']),
                     'xgbuildup_home': float(match['rosterMatch']['h'][roster_h]['xGBuildup']),
                     'xgbuildup_away': 0,
                     'xgbuildup_total': float(match['rosterMatch']['h'][roster_h]['xGBuildup']),
                     'goal_home': int(match['rosterMatch']['h'][roster_h]['goals']),
                     'goal_away': 0,
                     'goal_total': int(match['rosterMatch']['h'][roster_h]['goals']),
                     'assist_home': int(match['rosterMatch']['h'][roster_h]['assists']),
                     'assist_away': 0,
                     'assist_total': int(match['rosterMatch']['h'][roster_h]['assists']),
                     'count': 1
                     }
                if len(player_list_h) == 0:
                    players_stats_list.append(player_current_h)
                else:
                    player_current_h = player_list_h[0]
                    player_current_h['goal_home'] +=  int(match['rosterMatch']['h'][roster_h]['goals'])
                    player_current_h['goal_total'] = player_current_h['goal_home'] + player_current_h['goal_away']  
                    player_current_h['assist_home'] +=  int(match['rosterMatch']['h'][roster_h]['assists'])
                    player_current_h['assist_total'] = player_current_h['assist_home'] + player_current_h['assist_away']
                    player_current_h['xgchain_home'] +=  float(match['rosterMatch']['h'][roster_h]['xGChain'])
                    player_current_h['xgchain_total'] = player_current_h['xgchain_home'] + player_current_h['xgchain_away']
                    player_current_h['xgbuildup_home'] +=  float(match['rosterMatch']['h'][roster_h]['xGBuildup'])
                    player_current_h['xgbuildup_total'] = player_current_h['xgbuildup_home'] + player_current_h['xgbuildup_away']
                    player_current_h['count'] = player_current_h['count'] + 1
        # RosterMatch Away
        for roster_a in match['rosterMatch']['a']:
            last_match['away_xg'] = last_match['away_xg'] + float(match['rosterMatch']['a'][roster_a]['xG'])
            last_match['away_shots'] = last_match['away_shots'] + float(match['rosterMatch']['a'][roster_a]['shots'])
            if match['h_a'] == 'a':
                if int(match['rosterMatch']['a'][roster_a]['time']) > 15:
                    lineups.append({'id':match['rosterMatch']['a'][roster_a]['player_id'],'name':match['rosterMatch']['a'][roster_a]['player']})
                player_list_a = list(filter(lambda t: t["id"] == match['rosterMatch']['a'][roster_a]['player_id'], players_stats_list))
                player_current_a = {
                     'id': match['rosterMatch']['a'][roster_a]['player_id'],
                     'name': match['rosterMatch']['a'][roster_a]['player'],
                     'xgchain_home': 0,
                     'xgchain_away': float(match['rosterMatch']['a'][roster_a]['xGChain']),
                     'xgchain_total': float(match['rosterMatch']['a'][roster_a]['xGChain']),
                     'xgbuildup_home': 0,
                     'xgbuildup_away': float(match['rosterMatch']['a'][roster_a]['xGBuildup']),
                     'xgbuildup_total': float(match['rosterMatch']['a'][roster_a]['xGBuildup']),
                     'goal_home': 0,
                     'goal_away': int(match['rosterMatch']['a'][roster_a]['goals']),
                     'goal_total': int(match['rosterMatch']['a'][roster_a]['goals']),
                     'assist_home': 0,
                     'assist_away': int(match['rosterMatch']['a'][roster_a]['assists']),
                     'assist_total': int(match['rosterMatch']['a'][roster_a]['assists']),
                     'count': 1
                     }
                if len(player_list_a) == 0:
                    players_stats_list.append(player_current_a)
                else:
                    player_current_a = player_list_a[0]
                    player_current_a['goal_away'] +=  int(match['rosterMatch']['a'][roster_a]['goals'])
                    player_current_a['goal_total'] = player_current_a['goal_home'] + player_current_a['goal_away']  
                    player_current_a['assist_away'] +=  int(match['rosterMatch']['a'][roster_a]['assists'])
                    player_current_a['assist_total'] = player_current_a['assist_home'] + player_current_a['assist_away']
                    player_current_a['xgchain_away'] +=  float(match['rosterMatch']['a'][roster_a]['xGChain'])
                    player_current_a['xgchain_total'] = player_current_a['xgchain_home'] + player_current_a['xgchain_away']
                    player_current_a['xgbuildup_away'] +=  float(match['rosterMatch']['a'][roster_a]['xGBuildup'])
                    player_current_a['xgbuildup_total'] = player_current_a['xgbuildup_home'] + player_current_a['xgbuildup_away']
                    player_current_a['count'] = player_current_a['count'] + 1
 
        lf_model_team['Pts'] = lf_model_team['Pts'] + int(match['pts'])
        lf_model_team['Bp'] = lf_model_team['Bp'] + int(match['g_for'])
        lf_model_team['Bc'] = lf_model_team['Bc'] + int(match['g_against'])
        if int(match['pts']) == 3:
            lf_model_team['G'] = lf_model_team['G'] + int('1')
        elif int(match['pts']) == 1:
            lf_model_team['N'] = lf_model_team['N'] + int('1')
        elif int(match['pts']) == 0:
            lf_model_team['P'] = lf_model_team['P'] + int('1')
        
        # Favorites Win and Deuce
        if int(match['pts']) > 0:
            for p in lineups:
                player_count = list(filter(lambda t: t["id"] == p['id'], players_stats_list))
                player_win_list = list(filter(lambda t: t["id"] == p['id'], lineups_win))
                if len(player_win_list) == 0:
                    lineups_win.append({'id': p['id'], 'name': p['name'], 'win': 1, 'count': player_count[0]['count']})
                else:
                    player_current_win = player_win_list[0]
                    player_current_win['win'] += 1
                    player_current_win['count'] = player_count[0]['count']
        # Calcul handicap
        # V1
        if int(match['g_for']) - int(match['g_against']) == 1:
            lf_model_handicap_team['V1'] = lf_model_handicap_team['V1'] + 1
        # V2
        if int(match['g_for']) - int(match['g_against']) == 2:
            lf_model_handicap_team['V2'] = lf_model_handicap_team['V2'] + 1
        # V>2
        if int(match['g_for']) - int(match['g_against']) > 2:
            lf_model_handicap_team['V>2'] = lf_model_handicap_team['V>2'] + 1
        # D1
        if int(match['g_against']) - int(match['g_for']) == 1:
            lf_model_handicap_team['D1'] = lf_model_handicap_team['D1'] + 1
        # D2
        if int(match['g_against']) - int(match['g_for']) == 2:
            lf_model_handicap_team['D2'] = lf_model_handicap_team['D2'] + 1
        # D>2
        if int(match['g_against']) - int(match['g_for']) > 2:
            lf_model_handicap_team['D>2'] = lf_model_handicap_team['D>2'] + 1
        
        # Calcul Domicile
        if match['h_a'] == 'h':
            matchs_dom = matchs_dom + 1
            lf_model_team['Pts_Dom'] = lf_model_team['Pts_Dom'] + int(match['pts'])
            lf_model_team['Bp_Dom'] = lf_model_team['Bp_Dom'] + int(match['g_for'])
            lf_model_team['Bc_Dom'] = lf_model_team['Bc_Dom'] + int(match['g_against'])
            if  int(match['pts']) == 3:
                lf_model_team['G_Dom'] = lf_model_team['G_Dom'] + int('1')
            elif int(match['pts']) == 1:
                lf_model_team['N_Dom'] = lf_model_team['N_Dom'] + int('1')
            elif int(match['pts']) == 0:
                lf_model_team['P_Dom'] = lf_model_team['P_Dom'] + int('1')

        # Calcul Exterieure
        if match['h_a'] == 'a':
            matchs_ext = matchs_ext + 1
            lf_model_team['Pts_Ext'] = lf_model_team['Pts_Ext'] + int(match['pts'])
            lf_model_team['Bp_Ext'] = lf_model_team['Bp_Ext'] + int(match['g_for'])
            lf_model_team['Bc_Ext'] = lf_model_team['Bc_Ext'] + int(match['g_against'])
            if  int(match['pts']) == 3:
                lf_model_team['G_Ext'] = lf_model_team['G_Ext'] + int('1')
            elif int(match['pts']) == 1:
                lf_model_team['N_Ext'] = lf_model_team['N_Ext'] + int('1')
            elif int(match['pts']) == 0:
                lf_model_team['P_Ext'] = lf_model_team['P_Ext'] + int('1')
        
        # Last Match
        lf_model_team['last_matchs'].append(last_match)
        
    
    # Classement Général
    lf_model_team['Diff'] = lf_model_team['Bp']  - lf_model_team['Bc'] 
    lf_model_team['J'] = len(matchs)
    lf_model_team['XgChain'] = docWS['data']['global']['xgChain']
    lf_model_team['XgBuildup'] = docWS['data']['global']['xgBuildup']
    lf_model_team['Sorare_Pts'] = docWS['data']['global']['points']
    # Classement Domicile
    lf_model_team['Diff_Dom'] = lf_model_team['Bp_Dom']  - lf_model_team['Bc_Dom'] 
    lf_model_team['J_Dom'] = matchs_dom

    # Classement Exterieure
    lf_model_team['Diff_Ext'] = lf_model_team['Bp_Ext']  - lf_model_team['Bc_Ext'] 
    lf_model_team['J_Ext'] = matchs_ext

    model_teams_classement.append(lf_model_team)
    # Classement Handicap
    lf_model_handicap_team['rank'] = 3 * lf_model_handicap_team['V1'] + 6 * lf_model_handicap_team['V2'] + 9 * lf_model_handicap_team['V>2'] 
    - 3 * lf_model_handicap_team['D1'] - 6 * lf_model_handicap_team['D2'] - 9 * lf_model_handicap_team['D>2']
    model_teams_classement_handicaps.append(lf_model_handicap_team)

    # k_nearest_national_teams
    
    # Defense
    for def_team in docWS['data']['global']['rank_def_league']:
        team_def = {'id': def_team['idTeam'], 'name': def_team['nameTeam'], 'rank': 0, 'Pts': 0, 'BP': 0, 'BC': 0}
        lf_model_team['k_nearest_national_teams']['def'].append(team_def)
    
    # Milieu
    for mil_team in docWS['data']['global']['rank_mil_league']:
        team_mil = {'id': mil_team['idTeam'], 'name': mil_team['nameTeam'], 'rank': 0, 'Pts': 0, 'BP': 0, 'BC': 0}
        lf_model_team['k_nearest_national_teams']['mil'].append(team_mil)
    
    # Milieu Attaque
    for att_mil_team in docWS['data']['global']['rank_att_mil_league']:
        team_att_mil = {'id': att_mil_team['idTeam'], 'name': att_mil_team['nameTeam'], 'rank': 0, 'Pts': 0, 'BP': 0, 'BC': 0}
        lf_model_team['k_nearest_national_teams']['att_mil'].append(team_att_mil)
    
    # Attaque
    for att_team in docWS['data']['global']['rank_att_league']:
        team_att = {'id': att_team['idTeam'], 'name': att_team['nameTeam'], 'rank': 0, 'Pts': 0, 'BP': 0, 'BC': 0}
        lf_model_team['k_nearest_national_teams']['att'].append(team_att)
    
    # Substitute
    for sub_team in docWS['data']['global']['rank_sub_league']:
        team_sub = {'id': sub_team['idTeam'], 'name': sub_team['nameTeam'], 'rank': 0, 'Pts': 0, 'BP': 0, 'BC': 0}
        lf_model_team['k_nearest_national_teams']['sub'].append(team_sub)
    # Total
    for total_team in docWS['data']['global']['rank_total_league']:
        team_total = {'id': total_team['idTeam'], 'name': total_team['nameTeam'], 'rank': 0, 'Pts': 0, 'BP': 0, 'BC': 0}
        lf_model_team['k_nearest_national_teams']['total'].append(team_total)
    
    # 3_best_scorer
    players_stats_list.sort(key=lambda x: float(x['goal_total']), reverse=True) 
    players_stats_list_3_best_scorer = players_stats_list[0:3]
    for p in players_stats_list_3_best_scorer:
        lf_model_team['3_best_scorer'].append(p)
    # 3_best_assists
    players_stats_list.sort(key=lambda x: float(x['assist_total']), reverse=True)
    players_stats_list_3_best_assist = players_stats_list[0:3]
    for a in players_stats_list_3_best_assist:
        lf_model_team['3_best_assists'].append(a) 
    # Favorites players
    for f_win in lineups_win:
        f_win['rating'] = (f_win['win']/f_win['count'])*100
    lineups_win_filtered = list(filter(lambda t: t["rating"] > 60, lineups_win))
    lineups_win_filtered.sort(key=lambda x: float(x['win']), reverse=True) 
    for w in lineups_win_filtered:
        lf_model_team['favorites'].append(w) 
# Classement championnats
ranks_leagues = []

# Classement Sorare
ranks_sorare = []

# Classement Handicap
ranks_handicap = []


for league in opta_constants.leagues:
     # Championnat
     model_teams_classement_filtered = list(filter(lambda t: t["league"] == league, model_teams_classement))
     model_teams_classement_filtered.sort(key=lambda x: float(x['Pts']), reverse=True) 
     rank_league = {'idTeam': 'Rank_League_'+league, 'list': model_teams_classement_filtered }
     ranks_leagues.append(rank_league)
     # Sorare
     model_teams_sorare_filtered = list(filter(lambda t: t["league"] == league, model_teams_classement))
     model_teams_sorare_filtered.sort(key=lambda x: float(x['Sorare_Pts']), reverse=True) 
     rank_sorare = {'idTeam': 'Rank_Sorare_'+league, 'list': model_teams_sorare_filtered }
     ranks_sorare.append(rank_sorare)
     # Handicaps
     model_teams_handicap_filtered = list(filter(lambda t: t["league"] == league, model_teams_classement_handicaps))
     model_teams_handicap_filtered.sort(key=lambda x: float(x['rank']), reverse=True) 
     rank_handicap = {'idTeam': 'Rank_Handicap_'+league, 'list': model_teams_handicap_filtered }
     ranks_handicap.append(rank_handicap)

# Last matchs : ranking
x = 0
for league in ranks_leagues:
    for team in ranks_leagues[x]['list']:
        for last_match in team['last_matchs']:
            for index, element in enumerate(ranks_leagues[x]['list']):
                if element["idTeam"] == last_match["own_id"]:
                    last_match['own_rank'] = index + 1
                if element["idTeam"] == last_match["against_id"]:
                    last_match['against_rank'] = index + 1
                
            if last_match['against_rank'] > 0 and last_match['against_rank'] < 6:
                
                if last_match['home_gfor_ft'] == last_match['away_gfor_ft']:
                    team["synthese_1_5"]['N'] = team["synthese_1_5"]['N'] + 1
                
                if last_match['h_a'] == 'h' and last_match['home_gfor_ft'] > last_match['away_gfor_ft']:
                    team["synthese_1_5"]['G'] = team["synthese_1_5"]['G'] + 1
                
                if last_match['h_a'] == 'a' and last_match['home_gfor_ft'] > last_match['away_gfor_ft']:
                    team["synthese_1_5"]['P'] = team["synthese_1_5"]['P'] + 1
                
                if last_match['h_a'] == 'h' and last_match['home_gfor_ft'] < last_match['away_gfor_ft']:
                    team["synthese_1_5"]['P'] = team["synthese_1_5"]['P'] + 1

                if last_match['h_a'] == 'a' and last_match['home_gfor_ft'] < last_match['away_gfor_ft']:
                    team["synthese_1_5"]['G'] = team["synthese_1_5"]['G'] + 1
                
                if last_match['h_a'] == 'h':
                    team["synthese_1_5"]['BP'] = team["synthese_1_5"]['BP'] + last_match['home_gfor_ft']
                    team["synthese_1_5"]['BC'] = team["synthese_1_5"]['BC'] + last_match['away_gfor_ft']
                
                if last_match['h_a'] == 'a':
                    team["synthese_1_5"]['BP'] = team["synthese_1_5"]['BP'] + last_match['away_gfor_ft']
                    team["synthese_1_5"]['BC'] = team["synthese_1_5"]['BC'] + last_match['home_gfor_ft']
                
            if last_match['against_rank'] > 5 and last_match['against_rank'] < 11:
                if last_match['home_gfor_ft'] == last_match['away_gfor_ft']:
                    team["synthese_5_10"]['N'] = team["synthese_5_10"]['N'] + 1
                
                if last_match['h_a'] == 'h' and last_match['home_gfor_ft'] > last_match['away_gfor_ft']:
                    team["synthese_5_10"]['G'] = team["synthese_5_10"]['G'] + 1
                
                if last_match['h_a'] == 'a' and last_match['home_gfor_ft'] > last_match['away_gfor_ft']:
                    team["synthese_5_10"]['P'] = team["synthese_5_10"]['P'] + 1
                
                if last_match['h_a'] == 'h' and last_match['home_gfor_ft'] < last_match['away_gfor_ft']:
                    team["synthese_5_10"]['P'] = team["synthese_5_10"]['P'] + 1
                
                if last_match['h_a'] == 'a' and last_match['home_gfor_ft'] < last_match['away_gfor_ft']:
                    team["synthese_5_10"]['G'] = team["synthese_5_10"]['G'] + 1
                
                if last_match['h_a'] == 'h':
                    team["synthese_5_10"]['BP'] = team["synthese_5_10"]['BP'] + last_match['home_gfor_ft']
                    team["synthese_5_10"]['BC'] = team["synthese_5_10"]['BC'] + last_match['away_gfor_ft']
                
                if last_match['h_a'] == 'a':
                    team["synthese_5_10"]['BP'] = team["synthese_5_10"]['BP'] + last_match['away_gfor_ft']
                    team["synthese_5_10"]['BC'] = team["synthese_5_10"]['BC'] + last_match['home_gfor_ft']
                
            if last_match['against_rank'] > 11:
                if last_match['home_gfor_ft'] == last_match['away_gfor_ft']:
                    team["synthese_10_end"]['N'] = team["synthese_10_end"]['N'] + 1
                
                if last_match['h_a'] == 'h' and last_match['home_gfor_ft'] > last_match['away_gfor_ft']:
                    team["synthese_10_end"]['G'] = team["synthese_10_end"]['G'] + 1
                
                if last_match['h_a'] == 'a' and last_match['home_gfor_ft'] > last_match['away_gfor_ft']:
                    team["synthese_10_end"]['P'] = team["synthese_10_end"]['P'] + 1
                
                if last_match['h_a'] == 'h' and last_match['home_gfor_ft'] < last_match['away_gfor_ft']:
                    team["synthese_10_end"]['P'] = team["synthese_10_end"]['P'] + 1

                if last_match['h_a'] == 'a' and last_match['home_gfor_ft'] < last_match['away_gfor_ft']:
                    team["synthese_10_end"]['G'] = team["synthese_10_end"]['G'] + 1
                
                if last_match['h_a'] == 'h':
                    team["synthese_10_end"]['BP'] = team["synthese_10_end"]['BP'] + last_match['home_gfor_ft']
                    team["synthese_10_end"]['BC'] = team["synthese_10_end"]['BC'] + last_match['away_gfor_ft']
                
                if last_match['h_a'] == 'a':
                    team["synthese_10_end"]['BP'] = team["synthese_10_end"]['BP'] + last_match['away_gfor_ft']
                    team["synthese_10_end"]['BC'] = team["synthese_10_end"]['BC'] + last_match['home_gfor_ft']
    x +=1
# k_nearest_national_teams
x = 0
for league in ranks_leagues:
    try:
        list_teams = ranks_leagues[x]['list']
        list_teams = understatService.getNLastMatchs(list_teams, 'def') 
        list_teams = understatService.getNLastMatchs(list_teams, 'mil')
        list_teams = understatService.getNLastMatchs(list_teams, 'att_mil')
        list_teams = understatService.getNLastMatchs(list_teams, 'att')
        list_teams = understatService.getNLastMatchs(list_teams, 'sub')
        list_teams = understatService.getNLastMatchs(list_teams, 'total')     
        x +=1
    except Exception as e:
        continue

# Persistence en base
cloudantService.persistModelDocument(opta_constants.model_team_classements+'_'+current_season, "Rank_Leagues", ranks_leagues)
cloudantService.persistModelDocument(opta_constants.model_team_classements+'_'+current_season, "Rank_Sorare", ranks_sorare)
cloudantService.persistModelDocument(opta_constants.model_team_classements+'_'+current_season, "Rank_Handicap", ranks_handicap)
  