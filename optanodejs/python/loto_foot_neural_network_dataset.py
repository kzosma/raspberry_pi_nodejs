from lib.cloudant.service import cloudant_db_service
from lib.understat import understat_factory
from constants import opta_constants
from optadatalib.Helper import Utils
import pandas as pd
import numpy as np
from itertools import combinations
cloudantService = cloudant_db_service.Cloudant()
understatService = understat_factory.UnderStatService()

def updateRowH(row, GFFT=0, GAFT=0, count=0):
        row['countH'] = count + 1
        row['GFH'] = row['GFH'] + int(GFFT)
        row['GAH'] = row['GAH'] + int(GAFT)
        if int(GFFT) > int(GAFT):
             row['WH'] = row['WH'] + 1
             row['PTSH'] = row['PTSH'] + 3
        elif int(GFFT) == int(GAFT):
             row['DH'] = row['DH'] + 1
             row['PTSH'] = row['PTSH'] + 1
        elif int(GFFT) < int(GAFT):
             row['LH'] = row['LH'] + 1
        return row

def updateRowA(row, GFFT=0, GAFT=0, count=0):
        row['countA'] = count + 1
        row['GFA'] = row['GFA'] + int(GFFT)
        row['GAA'] = row['GAA'] + int(GAFT)
        if int(GFFT) > int(GAFT):
             row['WA'] = row['WA'] + 1
             row['PTSA'] = row['PTSA'] + 3
        elif int(GFFT) == int(GAFT):
             row['DA'] = row['DA'] + 1
             row['PTSA'] = row['PTSA'] + 1
        elif int(GFFT) < int(GAFT):
             row['LA'] = row['LA'] + 1
        return row


def computeHomeStat(row, empty: bool,update=None):
    if empty == True:
        row['countH'] = 0
        row['WH'] = 0
        row['DH'] = 0
        row['LH'] = 0
        row['PTSH'] = 0
        row['GFH'] = 0
        row['GAH'] = 0
    else:
         if len(update) == 1 and row['h_a'] == 'h':
                    GF_FT_H = update.loc[update['teamH'] == row['teamH'], 'GF_FT_H'].iloc[0]
                    GA_FT_H = update.loc[update['teamH'] == row['teamH'], 'GA_FT_H'].iloc[0]
                    countH = update.loc[update['teamH'] == row['teamH'], 'countH'].iloc[0]
                    row = updateRowH(row=row, GFFT=GF_FT_H, GAFT=GA_FT_H,count=countH)
         if len(update) > 1 and row['h_a'] == 'h':
                    for index, stat in update.iterrows():
                            GF_FT_H = stat['GF_FT_H']
                            GA_FT_H = stat['GA_FT_H']
                            countH = row['countH']
                            row = updateRowH(row=row, GFFT=GF_FT_H, GAFT=GA_FT_H,count=countH)
    return row

def computeAwayStat(row, empty: bool, update=None):
    if empty == True:
        row['countA'] = 0
        row['WA'] = 0
        row['DA'] = 0
        row['LA'] = 0
        row['PTSA'] = 0
        row['GFA'] = 0
        row['GAA'] = 0
    else:
         if len(update) == 1 and row['h_a'] == 'a':
                    GF_FT_A = update.loc[update['teamA'] == row['teamA'], 'GF_FT_A'].iloc[0]
                    GA_FT_A = update.loc[update['teamA'] == row['teamA'], 'GA_FT_A'].iloc[0]
                    countA = update.loc[update['teamA'] == row['teamA'], 'countA'].iloc[0]
                    row = updateRowA(row=row, GFFT=GF_FT_A, GAFT=GA_FT_A,count=countA)
         if len(update) > 1 and row['h_a'] == 'a':
                    for index, stat in update.iterrows():
                            GF_FT_A = stat['GF_FT_A']
                            GA_FT_A = stat['GA_FT_A']
                            countA = row['countA']
                            row = updateRowA(row=row, GFFT=GF_FT_A, GAFT=GA_FT_A,count=countA)
    return row

def computeTotalStat(row, empty: bool, team=None):
    if empty == True:
        row['WT'] = 0
        row['DT'] = 0
        row['LT'] = 0
        row['PTST'] = 0
        row['GFT'] = 0
        row['GAT'] = 0
    elif empty == False:
                WT = 0
                DT = 0
                LT = 0
                if len(df_home) > 0 :
                      update = df_home.loc[df_home['teamH'] == team]
                      if not update.empty and len(update) > 1:
                        for index, stat in update.iterrows():
                            GF_FT_H = stat['GF_FT_H']
                            GA_FT_H = stat['GA_FT_H']
                            if GF_FT_H > GA_FT_H:
                                  WT = WT + 1
                            if GF_FT_H < GA_FT_H:
                                  LT = LT + 1
                            if GF_FT_H == GA_FT_H:
                                  DT = DT + 1 
                      elif not update.empty and len(update) == 1:
                            GF_FT_H = update.loc[update['teamH'] == team, 'GF_FT_H'].iloc[0]
                            GA_FT_H = update.loc[update['teamH'] == team, 'GA_FT_H'].iloc[0]
                            if GF_FT_H > GA_FT_H:
                                  WT = WT + 1
                            if GF_FT_H < GA_FT_H:
                                  LT = LT + 1
                            if GF_FT_H == GA_FT_H:
                                  DT = DT + 1
                if len(df_away) > 0 :
                      updateA = df_away.loc[df_away['teamA'] == team]
                      if not updateA.empty and len(updateA) > 1:
                        for index, statA in updateA.iterrows():
                            GF_FT_A = statA['GF_FT_A']
                            GA_FT_A = statA['GA_FT_A']
                            if GF_FT_A > GA_FT_A:
                                  WT = WT + 1
                            if GF_FT_A < GA_FT_A:
                                  LT = LT + 1
                            if GF_FT_A == GA_FT_A:
                                  DT = DT + 1 
                      elif not updateA.empty and len(updateA) == 1:
                            GF_FT_A = updateA.loc[updateA['teamA'] == team, 'GF_FT_A'].iloc[0]
                            GA_FT_A = updateA.loc[updateA['teamA'] == team, 'GA_FT_A'].iloc[0]
                            if GF_FT_A > GA_FT_A:
                                  WT = WT + 1
                            if GF_FT_A < GA_FT_A:
                                  LT = LT + 1
                            if GF_FT_A == GA_FT_A:
                                  DT = DT + 1 
                row['WT'] = WT
                row['DT'] = DT
                row['LT'] = LT  
    return row

loto_foot_datasets = cloudantService.getAllDataFromDB(opta_constants.model_team_classements+'_2022')
df_home = pd.DataFrame()
df_away = pd.DataFrame()
df_total = pd.DataFrame()
rank_league = Utils.get_team_data_by_id(data=loto_foot_datasets, id='Rank_League_Ligue_1')
loto_foot_datasets_teams = rank_league['doc']['data']['list']
for team_LF in loto_foot_datasets_teams:
    for fixture in team_LF['last_matchs']:
        loto_foot_dataset_row = {'idMatch': fixture['idMatch']}
        h_a = fixture['h_a']
        loto_foot_dataset_row['h_a'] = h_a
        dayMatch = fixture['dayMatch']
        loto_foot_dataset_row['dayMatch'] = dayMatch
        if h_a == 'h':
            loto_foot_dataset_row['idTeamH'] = fixture['own_id']
            loto_foot_dataset_row['teamH'] = fixture['own_name']
            loto_foot_dataset_row['xgH'] = fixture['home_xg']
            loto_foot_dataset_row['GF_MT_H'] = fixture['home_gfor_mt']
            loto_foot_dataset_row['GF_FT_H'] = fixture['home_gfor_ft']
            loto_foot_dataset_row['GA_MT_H'] = fixture['away_gfor_mt']
            loto_foot_dataset_row['GA_FT_H'] = fixture['away_gfor_ft']

            loto_foot_dataset_row['GF_MT_RESULT'] = fixture['home_gfor_mt']
            loto_foot_dataset_row['GF_FT_RESULT'] = fixture['home_gfor_ft']
            loto_foot_dataset_row['GA_MT_RESULT'] = fixture['away_gfor_mt']
            loto_foot_dataset_row['GA_FT_RESULT'] = fixture['away_gfor_ft']

            loto_foot_dataset_row = computeHomeStat(row=loto_foot_dataset_row, empty=True)
        if h_a == 'a':
            loto_foot_dataset_row['idTeamA'] = fixture['own_id']
            loto_foot_dataset_row['teamA'] = fixture['own_name']
            loto_foot_dataset_row['xgA'] = fixture['away_xg']
            loto_foot_dataset_row['GF_MT_A'] = fixture['away_gfor_mt']
            loto_foot_dataset_row['GF_FT_A'] = fixture['away_gfor_ft']
            loto_foot_dataset_row['GA_MT_A'] = fixture['home_gfor_mt']
            loto_foot_dataset_row['GA_FT_A'] = fixture['home_gfor_ft']

            loto_foot_dataset_row['GF_MT_RESULT'] = fixture['away_gfor_mt']
            loto_foot_dataset_row['GF_FT_RESULT'] = fixture['away_gfor_ft']
            loto_foot_dataset_row['GA_MT_RESULT'] = fixture['home_gfor_mt']
            loto_foot_dataset_row['GA_FT_RESULT'] = fixture['home_gfor_ft']
            loto_foot_dataset_row = computeAwayStat(row=loto_foot_dataset_row, empty=True)
        
        if len(df_home) > 0 :
            team_stats = df_home.loc[df_home['teamH'] == fixture['own_name']]
            if not team_stats.empty:
                loto_foot_dataset_row = computeHomeStat(row=loto_foot_dataset_row, empty=False, update=team_stats)
        if len(df_away) > 0 :
            team_stats = df_away.loc[df_away['teamA'] == fixture['own_name']]
            if not team_stats.empty:
                loto_foot_dataset_row = computeAwayStat(row=loto_foot_dataset_row, empty=False, update=team_stats)
        
        loto_foot_dataset_total = {'idMatch': fixture['idMatch'], 'idTeam': fixture['own_id'],'team': fixture['own_name'],'dayMatch' : fixture['dayMatch']}
        loto_foot_dataset_total = computeTotalStat(row=loto_foot_dataset_total,empty=False,team=fixture['own_name'])
        df_total = pd.concat([df_total, pd.json_normalize(loto_foot_dataset_total)], ignore_index=True)
        
        if h_a == 'h':
            df_home = pd.concat([df_home, pd.json_normalize(loto_foot_dataset_row)], ignore_index=True)
        if h_a == 'a':
            df_away = pd.concat([df_away, pd.json_normalize(loto_foot_dataset_row)], ignore_index=True)
        
        


# Print
pd.set_option('display.max_rows', None)

# HOME TOTAL
df_home = df_home.rename(columns={'teamH':'team', 'idTeamH': 'idTeam'})
df_home = df_home[['dayMatch', 'idTeam','team', 'h_a', 'countH', 'WH', 'DH', 'LH','GF_FT_H', 'GA_FT_H', 'GF_MT_H', 'GA_MT_H', 'GFH', 'GAH', 'PTSH', 'GF_FT_RESULT','GA_FT_RESULT','GF_MT_RESULT','GA_MT_RESULT']] 
#print(df_home)

# Pythagorean Expectation Home
hwinvalue = df_home['WH'] + df_home['DH'] / 2 
df_home['wpc_H'] = np.where(df_home['countH']==0,0,hwinvalue / df_home['countH'])
df_home['pyth_H'] = np.where(df_home['GFH']**2 + df_home['GAH']**2==0,0,df_home['GFH']**2/(df_home['GFH']**2 + df_home['GAH']**2))
df_home_total = pd.merge(df_home, df_total, on=['dayMatch', 'team', 'idTeam'])
df_home_total = df_home_total.rename(columns={'dayMatch':'GP', 'countH': 'GPH'})
df_home_total = df_home_total.reindex(columns=['idMatch','idTeam','team','h_a','GP', 'WT', 'DT', 'LT', 'GPH', 'WH', 'DH', 'LH','GF_FT_H', 'GA_FT_H', 'GF_MT_H', 'GA_MT_H','GFH', 'GAH', 'PTSH','GF_FT_RESULT','GA_FT_RESULT','GF_MT_RESULT','GA_MT_RESULT','wpc_H','pyth_H'])

# AWAY TOTAL
df_away = df_away.rename(columns={'teamA':'team', 'idTeamA': 'idTeam'})
df_away = df_away[['dayMatch','idTeam','team','h_a', 'countA', 'WA', 'DA', 'LA', 'GF_FT_A', 'GA_FT_A', 'GF_MT_A', 'GA_MT_A', 'GFA', 'GAA', 'PTSA','GF_FT_RESULT', 'GA_FT_RESULT','GF_MT_RESULT','GA_MT_RESULT']] 
# Pythagorean Expectation Away
awinvalue = df_away['WA'] + df_away['DA'] / 2 
df_away['wpc_A'] = np.where(df_away['countA']==0,0,awinvalue / df_away['countA'])
df_away['pyth_A'] = np.where(df_away['GFA']**2 + df_away['GAA']**2==0,0,df_away['GFA']**2/(df_away['GFA']**2 + df_away['GAA']**2))

df_away_total = pd.merge(df_away, df_total, on=['dayMatch', 'team', 'idTeam'])
df_away_total = df_away_total.rename(columns={'dayMatch':'GP', 'countA': 'GPA'})
df_away_total = df_away_total.reindex(columns=['idMatch', 'idTeam', 'team','h_a', 'GP', 'WT', 'DT', 'LT', 'GPA', 'WA', 'DA', 'LA', 'GF_FT_A', 'GA_FT_A', 'GF_MT_A','GA_MT_A', 'GFA', 'GAA', 'PTSA', 'GF_FT_RESULT', 'GA_FT_RESULT','GF_MT_RESULT','GA_MT_RESULT','wpc_A','pyth_A'])


merged_df_total = pd.merge(df_home_total, df_away_total, on=['idMatch', 'idTeam', 'team','h_a', 'GP', 'WT', 'DT', 'LT', 'GF_FT_RESULT', 'GA_FT_RESULT','GF_MT_RESULT','GA_MT_RESULT'], how='outer')
merged_df_total = merged_df_total[['idMatch', 'idTeam', 'team','h_a', 'GP', 'WT', 'DT', 'LT', 'GPH', 'GPA', 'WH', 'WA', 'DH', 'DA', 'LH', 'LA', 'GF_FT_H', 'GA_FT_H', 'GF_MT_H', 'GA_MT_H', 'GF_FT_A', 'GA_FT_A', 'GF_MT_A','GA_MT_A', 'GFH', 'GAH','GFA', 'GAA', 'PTSH', 'PTSA', 'GF_FT_RESULT', 'GA_FT_RESULT','GF_MT_RESULT','GA_MT_RESULT','wpc_H', 'pyth_H','wpc_A', 'pyth_A']]

# trier les lignes par "team"
sorted_df_total = merged_df_total.sort_values(by=['team', 'GP'])
sorted_df_total = sorted_df_total.reset_index(drop=True)

# Fill Missing data Nan
sorted_df_total.fillna(0, inplace=True)
for i in range(1, len(sorted_df_total)):
    if sorted_df_total.loc[i, 'GP'] > 1:
        if (sorted_df_total.loc[i-1, 'WT'] + 1 == sorted_df_total.loc[i, 'WT']) and (sorted_df_total.loc[i-1, 'h_a'] == 'h'):
            sorted_df_total.loc[i, 'WH'] = sorted_df_total.loc[i-1, 'WH'] + 1
            sorted_df_total.loc[i, 'DH'] = sorted_df_total.loc[i-1, 'DH']
            sorted_df_total.loc[i, 'LH'] = sorted_df_total.loc[i-1, 'LH']
        elif (sorted_df_total.loc[i-1, 'WT'] + 1 == sorted_df_total.loc[i, 'WT']) and (sorted_df_total.loc[i-1, 'h_a'] == 'a'):
            sorted_df_total.loc[i, 'WA'] = sorted_df_total.loc[i-1, 'WA'] + 1
            sorted_df_total.loc[i, 'DA'] = sorted_df_total.loc[i-1, 'DA']
            sorted_df_total.loc[i, 'LA'] = sorted_df_total.loc[i-1, 'LA']
        if (sorted_df_total.loc[i-1, 'DT'] + 1 == sorted_df_total.loc[i, 'DT']) and (sorted_df_total.loc[i-1, 'h_a'] == 'h'):
            sorted_df_total.loc[i, 'DH'] = sorted_df_total.loc[i-1, 'DH'] + 1
            sorted_df_total.loc[i, 'WH'] = sorted_df_total.loc[i-1, 'WH']
            sorted_df_total.loc[i, 'LH'] = sorted_df_total.loc[i-1, 'LH']
        elif (sorted_df_total.loc[i-1, 'DT'] + 1 == sorted_df_total.loc[i, 'DT']) and (sorted_df_total.loc[i-1, 'h_a'] == 'a'):
            sorted_df_total.loc[i, 'DA'] = sorted_df_total.loc[i-1, 'DA'] + 1
            sorted_df_total.loc[i, 'WA'] = sorted_df_total.loc[i-1, 'WA']
            sorted_df_total.loc[i, 'LA'] = sorted_df_total.loc[i-1, 'LA']
        if (sorted_df_total.loc[i-1, 'LT'] + 1 == sorted_df_total.loc[i, 'LT']) and (sorted_df_total.loc[i-1, 'h_a'] == 'h'):
            sorted_df_total.loc[i, 'LH'] = sorted_df_total.loc[i-1, 'LH'] + 1
            sorted_df_total.loc[i, 'WH'] = sorted_df_total.loc[i-1, 'WH']
            sorted_df_total.loc[i, 'DH'] = sorted_df_total.loc[i-1, 'DH']
        elif (sorted_df_total.loc[i-1, 'LT'] + 1 == sorted_df_total.loc[i, 'LT']) and (sorted_df_total.loc[i-1, 'h_a'] == 'a'):
            sorted_df_total.loc[i, 'LA'] = sorted_df_total.loc[i-1, 'LA'] + 1
            sorted_df_total.loc[i, 'WA'] = sorted_df_total.loc[i-1, 'WA']
            sorted_df_total.loc[i, 'DA'] = sorted_df_total.loc[i-1, 'DA']
        ####
        if (sorted_df_total.loc[i, 'h_a'] == 'h') and (sorted_df_total.loc[i-1, 'h_a'] == 'a'):
            sorted_df_total.loc[i, 'GPA'] = sorted_df_total.loc[i-1, 'GPA'] + 1
        
        if (sorted_df_total.loc[i, 'h_a'] == 'h') and (sorted_df_total.loc[i-1, 'h_a'] == 'h'):
            sorted_df_total.loc[i, 'GPA'] = sorted_df_total.loc[i-1, 'GPA']

        if (sorted_df_total.loc[i, 'h_a'] == 'a') and (sorted_df_total.loc[i-1, 'h_a'] == 'h'):
            sorted_df_total.loc[i, 'GPH'] = sorted_df_total.loc[i-1, 'GPH'] + 1
        
        if (sorted_df_total.loc[i, 'h_a'] == 'a') and (sorted_df_total.loc[i-1, 'h_a'] == 'a'):
            sorted_df_total.loc[i, 'GPH'] = sorted_df_total.loc[i-1, 'GPH']
        
    elif sorted_df_total.loc[i, 'GP'] == 1:
        sorted_df_total.loc[i, ['GPH', 'GPA', 'WH', 'WA', 'DH', 'DA', 'LH', 'LA']] = 0


# Distance Euclidienne Home
unique_teams_total = sorted_df_total['idTeam'].unique()
for id_team_total in unique_teams_total:
     docWS = cloudantService.getClient().get_document(db=opta_constants.model_team_whoscored+'_2022',doc_id=id_team_total).get_result()
     matchs = docWS['data']['fixtures']
     total_rows = sorted_df_total[(sorted_df_total['idTeam'] == id_team_total)]
     # xg
     xg_mean_total = 0
     xg_mean_def = 0
     xg_mean_att = 0
     xg_mean_mil = 0
     xg_mean_att_mil = 0
     # xgchain
     xgchain_mean_total = 0
     xgchain_mean_def = 0
     xgchain_mean_att = 0
     xgchain_mean_mil = 0
     xgchain_mean_att_mil = 0

     # xgbuildup
     xgbuildup_mean_total = 0
     xgbuildup_mean_def = 0
     xgbuildup_mean_att = 0
     xgbuildup_mean_mil = 0
     xgbuildup_mean_att_mil = 0
     
     for index, row_total in total_rows.iterrows():
          
          # Total --total
          sorted_df_total.at[index, 'xG_total'] = 0
          sorted_df_total.at[index, 'xA_total'] = 0
          sorted_df_total.at[index, 'assists_total'] = 0
          sorted_df_total.at[index, 'shots_total'] = 0
          sorted_df_total.at[index, 'xGChain_total'] = 0
          sorted_df_total.at[index, 'xGBuildup_total'] = 0
          sorted_df_total.at[index, 'goals_total'] = 0
          sorted_df_total.at[index, 'xg_mean_total'] = 0
          sorted_df_total.at[index, 'xgchain_mean_total'] = 0
          sorted_df_total.at[index, 'xgbuildup_mean_total'] = 0
                   

          # Defense -- def
          sorted_df_total.at[index, 'xG_def'] = 0
          sorted_df_total.at[index, 'xA_def'] = 0
          sorted_df_total.at[index, 'assists_def'] = 0
          sorted_df_total.at[index, 'shots_def'] = 0
          sorted_df_total.at[index, 'xGChain_def'] = 0
          sorted_df_total.at[index, 'xGBuildup_def'] = 0
          sorted_df_total.at[index, 'goals_def'] = 0
          sorted_df_total.at[index, 'xg_mean_def'] = 0
          sorted_df_total.at[index, 'xgchain_mean_def'] = 0
          sorted_df_total.at[index, 'xgbuildup_mean_def'] = 0
          
          # Milieu -- mil
          sorted_df_total.at[index, 'xG_mil'] = 0
          sorted_df_total.at[index, 'xA_mil'] = 0
          sorted_df_total.at[index, 'assists_mil'] = 0
          sorted_df_total.at[index, 'shots_mil'] = 0
          sorted_df_total.at[index, 'xGChain_mil'] = 0
          sorted_df_total.at[index, 'xGBuildup_mil'] = 0
          sorted_df_total.at[index, 'goals_mil'] = 0
          sorted_df_total.at[index, 'xg_mean_mil'] = 0
          sorted_df_total.at[index, 'xgchain_mean_mil'] = 0
          sorted_df_total.at[index, 'xgbuildup_mean_mil'] = 0
          
          # Attaque --att
          sorted_df_total.at[index, 'xG_att'] = 0
          sorted_df_total.at[index, 'xA_att'] = 0
          sorted_df_total.at[index, 'assists_att'] = 0
          sorted_df_total.at[index, 'shots_att'] = 0
          sorted_df_total.at[index, 'xGChain_att'] = 0
          sorted_df_total.at[index, 'xGBuildup_att'] = 0
          sorted_df_total.at[index, 'goals_att'] = 0
          sorted_df_total.at[index, 'xg_mean_att'] = 0
          sorted_df_total.at[index, 'xgchain_mean_att'] = 0
          sorted_df_total.at[index, 'xgbuildup_mean_att'] = 0
          
          # Attaque Milieu -- att_mil
          sorted_df_total.at[index, 'xG_att_mil'] = 0
          sorted_df_total.at[index, 'xA_att_mil'] = 0
          sorted_df_total.at[index, 'assists_att_mil'] = 0
          sorted_df_total.at[index, 'shots_att_mil'] = 0
          sorted_df_total.at[index, 'xGChain_att_mil'] = 0
          sorted_df_total.at[index, 'xGBuildup_att_mil'] = 0
          sorted_df_total.at[index, 'goals_att_mil'] = 0
          sorted_df_total.at[index, 'xg_mean_att_mil'] = 0
          sorted_df_total.at[index, 'xgchain_mean_att_mil'] = 0
          sorted_df_total.at[index, 'xgbuildup_mean_att_mil'] = 0
          

          GP = +row_total['GP']
          if GP > 1:
               gp_index = GP - 1
               objet_match = list(filter(lambda x: x['dayMatch'] == gp_index, matchs))[0]
               # Total
               sorted_df_total.at[index, 'xG_total'] = objet_match['featuresEuclidien']['total']['xG']
               sorted_df_total.at[index, 'xA_total'] = objet_match['featuresEuclidien']['total']['xA']
               sorted_df_total.at[index, 'assists_total'] = objet_match['featuresEuclidien']['total']['assists']
               sorted_df_total.at[index, 'shots_total'] = objet_match['featuresEuclidien']['total']['shots']
               sorted_df_total.at[index, 'xGChain_total'] = objet_match['featuresEuclidien']['total']['xGChain']
               sorted_df_total.at[index, 'xGBuildup_total'] = objet_match['featuresEuclidien']['total']['xGBuildup']
               sorted_df_total.at[index, 'goals_total'] = objet_match['featuresEuclidien']['total']['goals']
               # xg mean
               xg_mean_total = xg_mean_total + (sorted_df_total.at[index, 'xG_total'] - xg_mean_total) / (gp_index + 1)
               sorted_df_total.at[index, 'xg_mean_total'] = xg_mean_total 
               
               # xg chain mean total
               xgchain_mean_total = xgchain_mean_total + (sorted_df_total.at[index, 'xGChain_total'] - xgchain_mean_total) / (gp_index + 1)
               sorted_df_total.at[index, 'xgchain_mean_total'] = xgchain_mean_total 

               # xg buildup mean total
               xgbuildup_mean_total = xgbuildup_mean_total + (sorted_df_total.at[index, 'xGBuildup_total'] - xgbuildup_mean_total) / (gp_index + 1)
               sorted_df_total.at[index, 'xgbuildup_mean_total'] = xgbuildup_mean_total 

               
               # Defense
               sorted_df_total.at[index, 'xG_def'] = objet_match['featuresEuclidien']['def']['xG']
               sorted_df_total.at[index, 'xA_def'] = objet_match['featuresEuclidien']['def']['xA']
               sorted_df_total.at[index, 'assists_def'] = objet_match['featuresEuclidien']['def']['assists']
               sorted_df_total.at[index, 'shots_def'] = objet_match['featuresEuclidien']['def']['shots']
               sorted_df_total.at[index, 'xGChain_def'] = objet_match['featuresEuclidien']['def']['xGChain']
               sorted_df_total.at[index, 'xGBuildup_def'] = objet_match['featuresEuclidien']['def']['xGBuildup']
               sorted_df_total.at[index, 'goals_def'] = objet_match['featuresEuclidien']['def']['goals']
               xg_mean_def = xg_mean_def + (sorted_df_total.at[index, 'xG_def'] - xg_mean_def) / (gp_index + 1)
               sorted_df_total.at[index, 'xg_mean_def'] = xg_mean_def 

               # xg chain mean defense
               xgchain_mean_def = xgchain_mean_def + (sorted_df_total.at[index, 'xGChain_def'] - xgchain_mean_def) / (gp_index + 1)
               sorted_df_total.at[index, 'xgchain_mean_def'] = xgchain_mean_def 
               # xg buildup mean defense
               xgbuildup_mean_def = xgbuildup_mean_def + (sorted_df_total.at[index, 'xGBuildup_def'] - xgbuildup_mean_def) / (gp_index + 1)
               sorted_df_total.at[index, 'xgbuildup_mean_def'] = xgbuildup_mean_def

               # Milieu
               sorted_df_total.at[index, 'xG_mil'] = objet_match['featuresEuclidien']['mil']['xG']
               sorted_df_total.at[index, 'xA_mil'] = objet_match['featuresEuclidien']['mil']['xA']
               sorted_df_total.at[index, 'assists_mil'] = objet_match['featuresEuclidien']['mil']['assists']
               sorted_df_total.at[index, 'shots_mil'] = objet_match['featuresEuclidien']['mil']['shots']
               sorted_df_total.at[index, 'xGChain_mil'] = objet_match['featuresEuclidien']['mil']['xGChain']
               sorted_df_total.at[index, 'xGBuildup_mil'] = objet_match['featuresEuclidien']['mil']['xGBuildup']
               sorted_df_total.at[index, 'goals_mil'] = objet_match['featuresEuclidien']['mil']['goals']
               xg_mean_mil = xg_mean_mil + (sorted_df_total.at[index, 'xG_mil'] - xg_mean_mil) / (gp_index + 1)
               sorted_df_total.at[index, 'xg_mean_mil'] = xg_mean_mil 
               
               # xg chain mean milieu
               xgchain_mean_mil = xgchain_mean_mil + (sorted_df_total.at[index, 'xGChain_mil'] - xgchain_mean_mil) / (gp_index + 1)
               sorted_df_total.at[index, 'xgchain_mean_mil'] = xgchain_mean_mil 
               # xg buildup mean milieu
               xgbuildup_mean_mil = xgbuildup_mean_mil + (sorted_df_total.at[index, 'xGBuildup_mil'] - xgbuildup_mean_mil) / (gp_index + 1)
               sorted_df_total.at[index, 'xgbuildup_mean_mil'] = xgbuildup_mean_mil
               
               
               # Attaque
               sorted_df_total.at[index, 'xG_att'] = objet_match['featuresEuclidien']['att']['xG']
               sorted_df_total.at[index, 'xA_att'] = objet_match['featuresEuclidien']['att']['xA']
               sorted_df_total.at[index, 'assists_att'] = objet_match['featuresEuclidien']['att']['assists']
               sorted_df_total.at[index, 'shots_att'] = objet_match['featuresEuclidien']['att']['shots']
               sorted_df_total.at[index, 'xGChain_att'] = objet_match['featuresEuclidien']['att']['xGChain']
               sorted_df_total.at[index, 'xGBuildup_att'] = objet_match['featuresEuclidien']['att']['xGBuildup']
               sorted_df_total.at[index, 'goals_att'] = objet_match['featuresEuclidien']['att']['goals']
               xg_mean_att = xg_mean_att + (sorted_df_total.at[index, 'xG_att'] - xg_mean_att) / (gp_index + 1)
               sorted_df_total.at[index, 'xg_mean_att'] = xg_mean_att 

               # xg chain mean attaque
               xgchain_mean_att = xgchain_mean_att + (sorted_df_total.at[index, 'xGChain_att'] - xgchain_mean_att) / (gp_index + 1)
               sorted_df_total.at[index, 'xgchain_mean_att'] = xgchain_mean_att 
               # xg buildup mean attaque
               xgbuildup_mean_att = xgbuildup_mean_att + (sorted_df_total.at[index, 'xGBuildup_att'] - xgbuildup_mean_att) / (gp_index + 1)
               sorted_df_total.at[index, 'xgbuildup_mean_att'] = xgbuildup_mean_att

               # Attaque Milieu
               sorted_df_total.at[index, 'xG_att_mil'] = objet_match['featuresEuclidien']['att_mil']['xG']
               sorted_df_total.at[index, 'xA_att_mil'] = objet_match['featuresEuclidien']['att_mil']['xA']
               sorted_df_total.at[index, 'assists_att_mil'] = objet_match['featuresEuclidien']['att_mil']['assists']
               sorted_df_total.at[index, 'shots_att_mil'] = objet_match['featuresEuclidien']['att_mil']['shots']
               sorted_df_total.at[index, 'xGChain_att_mil'] = objet_match['featuresEuclidien']['att_mil']['xGChain']
               sorted_df_total.at[index, 'xGBuildup_att_mil'] = objet_match['featuresEuclidien']['att_mil']['xGBuildup']
               sorted_df_total.at[index, 'goals_att_mil'] = objet_match['featuresEuclidien']['att_mil']['goals']
               xg_mean_att_mil = xg_mean_att_mil + (sorted_df_total.at[index, 'xG_att_mil'] - xg_mean_att_mil) / (gp_index + 1)
               sorted_df_total.at[index, 'xg_mean_att_mil_mil'] = xg_mean_att_mil 

               # xg chain mean attaque milieu
               xgchain_mean_att_mil = xgchain_mean_att_mil + (sorted_df_total.at[index, 'xGChain_att_mil'] - xgchain_mean_att_mil) / (gp_index + 1)
               sorted_df_total.at[index, 'xgchain_mean_att_mil'] = xgchain_mean_att_mil 
               # xg buildup mean attaque milieu
               xgbuildup_mean_att_mil = xgbuildup_mean_att_mil + (sorted_df_total.at[index, 'xGBuildup_att_mil'] - xgbuildup_mean_att_mil) / (gp_index + 1)
               sorted_df_total.at[index, 'xgbuildup_mean_att_mil'] = xgbuildup_mean_att_mil



# Analyse
sorted_df_total_test = sorted_df_total[['idTeam','team','xgbuildup_mean_total', 'xg_mean_total']] 
last_rows = sorted_df_total_test.groupby('idTeam').tail(1)

last_rows = last_rows.copy() 

last_rows.loc[:, 'closest_idteam'] = -1

# boucle sur chaque équipe
for i, row_i in last_rows.iterrows():
    min_dist = np.inf
    closest_idteam = -1
    i_array = np.array((row_i['xgbuildup_mean_total'],row_i['xg_mean_total']))
    # comparer avec toutes les autres équipes
    for j, row_j in last_rows.iterrows():
        if i != j: # ignorer l'équipe elle-même
            j_array = np.array((row_j['xgbuildup_mean_total'],row_j['xg_mean_total']))
            dist = np.linalg.norm(i_array - j_array)
            if dist < min_dist:
                min_dist = dist
                closest_idteam = row_j['idTeam']
    last_rows.at[i, 'closest_idteam'] = closest_idteam

# supprimer la colonne 'idTeam' de l'index et la placer en tant que colonne normale
last_rows = last_rows.reset_index(drop=True)
print(last_rows)



# total_n_array = np.array((total_n['xG'],total_n['goals'],total_n['xA'],total_n['assists'],total_n['shots'],total_n['xGChain'],total_n['xGBuildup']))
# total_f_array = np.array((total_f['xG'],total_f['goals'],total_f['xA'],total_f['assists'],total_f['shots'],total_f['xGChain'],total_f['xGBuildup']))
# distance euclidienne
# dist_def = np.linalg.norm(def_n_array-def_f_array)
# print(last_rows.sort_values('xgbuildup_mean_total'))
# distances = np.linalg.norm(last_rows[['xgchain_mean_def', 'xgchain_mean_att']] - last_rows[['xgchain_mean_def', 'xgchain_mean_att']].values[-1], axis=1)
# last_rows = last_rows.assign(dist=distances)
# sorted_rows = last_rows.sort_values(by='dist')
# print(sorted_rows[['team', 'dist']])
# home_rows = sorted_df_total_test[(sorted_df_total_test['team'] == 'Lyon')]
# print(home_rows.tail(1).to_string(index=False))
# fin de test
# sorted_df_total = sorted_df_total[['idMatch','team', 'GP','h_a','wpc_H', 'pyth_H']] 
# print(sorted_df_total)
# print(sorted_df_total.columns.to_list())





