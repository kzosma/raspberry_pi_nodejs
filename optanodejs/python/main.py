from datetime import datetime
from constants import opta_constants
from lib.understat import understat_factory
understatService = understat_factory.UnderStatService()
for league in opta_constants.leagues:
    for season in opta_constants.seasons:
        understatService.getOptaData(league, season)
