from lib.cloudant.service import cloudant_db_service
from constants import opta_constants
from lib.understat import understat_factory
understatService = understat_factory.UnderStatService()
cloudantService = cloudant_db_service.Cloudant()
season_ref = opta_constants.seasons[0]
cloudantService.deleteDatabase(opta_constants.dbname_ionic+'_'+season_ref)
cloudantService.createDatabase(opta_constants.dbname_ionic+'_'+season_ref)
nextMatchesArray= []
for league in opta_constants.leagues:
    for season in opta_constants.seasons:
        nextMatches = {'league': league, 'season': season, 'nextMatches': []}
        nextMatches['nextMatches'] = understatService.getOptaNextMatchesIonic(league, season)
        nextMatchesArray.append(nextMatches)
cloudantService.persistIonicDoc(opta_constants.dbname_ionic+'_'+season_ref,"ui_ionic_nextmatches",nextMatchesArray)       

