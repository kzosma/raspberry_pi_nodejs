from lib.cloudant.service import cloudant_db_service
from constants import opta_constants
from ibm_cloud_sdk_core.api_exception import ApiException
import numpy as np
cloudantService = cloudant_db_service.Cloudant()
season_current = opta_constants.seasons[0]
allteams = cloudantService.getAllDataFromDB(opta_constants.dbname_teams+'_'+season_current)
cloudantService.deleteDatabase(opta_constants.model_team_whoscored+'_'+season_current)
cloudantService.createDatabase(opta_constants.model_team_whoscored+'_'+season_current)
model_teams_whoscored = []
for team in allteams:
    idDoc = team['doc']['_id']
    try:
        docModele = cloudantService.getClient().get_document(db=opta_constants.model_team_whoscored+'_'+season_current,doc_id=idDoc)
    except ApiException as apiexc:
            if apiexc.code == 404:
                print('Document ' + str(idDoc) +' not found')
    model_team = {'idTeam': idDoc ,
     'nameTeam':team['doc']['nameTeam'],
     'nameLeague':team['doc']['nameLeague'],
     'rank': 0,
     'fixtures': [],
     'global': {
        'goals': 0,
        'shots': 0,
        'yellow_card': 0,
        'red_card': 0,
        'xgChain' : 0,
        'xgBuildup': 0,
        'points' : 0,
        "rank_def_league": [],
        "rank_def_europe": [],
        "rank_mil_league": [],
        "rank_mil_europe": [],
        "rank_att_mil_league": [],
        "rank_att_mil_europe": [],
        "rank_att_league": [],
        "rank_att_europe": [],
        "rank_sub_league": [],
        "rank_sub_europe": [],
        "rank_total_league": [],
        "rank_total_europe": []
     }
    }
    for match in team['doc']['fixtures']:
        h_a = match['h_a']
        dayMatch = match['dayMatch']
        model_fixture = {
         'dayMatch': dayMatch,
         'featuresEuclidien': {
            'def': {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 },
            'mil': {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 },
            'att_mil': {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 },
            'att': {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 },
            'sub': {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 },
            'total': {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
         },
         'sorareData': {
            'def': 0,
            'mil': 0,
            'att_mil': 0,
            'att': 0,
            'sub': 0,
            'total': 0
          }
        }
        rosterMatch = match['rosterMatch'][h_a]
        def_points = 0
        mil_points = 0
        att_mil_points = 0
        sub_points = 0
        att_points = 0
        
        def_roster = {
                'goals' : 0,
                'own_goals' : 0,
                'yellow_card' : 0,
                'red_card' : 0,
                'key_passes': 0,
                'assists' : 0,
                'xg_chain' : 0,
                'xg_buildup': 0,
                'shots': 0 
        }
        mil_roster = {
                'goals' : 0,
                'own_goals' : 0,
                'yellow_card' : 0,
                'red_card' : 0,
                'key_passes': 0,
                'assists' : 0,
                'xg_chain' : 0,
                'xg_buildup': 0,
                'shots': 0 
        }
        att_mil_roster = {
                'goals' : 0,
                'own_goals' : 0,
                'yellow_card' : 0,
                'red_card' : 0,
                'key_passes': 0,
                'assists' : 0,
                'xg_chain' : 0,
                'xg_buildup': 0,
                'shots': 0 
        }
        att_roster = {
                'goals' : 0,
                'own_goals' : 0,
                'yellow_card' : 0,
                'red_card' : 0,
                'key_passes': 0,
                'assists' : 0,
                'xg_chain' : 0,
                'xg_buildup': 0,
                'shots': 0 
        }
        sub_roster = {
                'goals' : 0,
                'own_goals' : 0,
                'yellow_card' : 0,
                'red_card' : 0,
                'key_passes': 0,
                'assists' : 0,
                'xg_chain' : 0,
                'xg_buildup': 0,
                'shots': 0 
        }
        for rosterItem in rosterMatch:
            roster = rosterMatch[rosterItem] 
            time = roster['time']
            position = roster['position']
            model_team['global']['goals'] += float(roster['goals'])
            model_team['global']['yellow_card'] += float(roster['yellow_card'])
            model_team['global']['red_card'] += float(roster['red_card'])
            model_team['global']['shots'] += float(roster['shots'])
            model_team['global']['xgChain'] += float(roster['xGChain'])
            model_team['global']['xgBuildup'] += float(roster['xGBuildup'])
            if int(time) > 15 and position.startswith('D'):
                def_roster['goals'] += (20) * float(roster['goals'])
                def_roster['own_goals'] += (-15) * float(roster['own_goals'])
                def_roster['yellow_card'] +=  float(roster['yellow_card']) *(-3) 
                def_roster['red_card'] += (-5) * float(roster['red_card'])
                def_roster['key_passes'] += (2) * float(roster['key_passes'])
                def_roster['assists'] += (5) * float(roster['assists'])
                def_roster['xg_chain'] += (10) * float(roster['xGChain'])
                def_roster['xg_buildup'] += (10) * float(roster['xGBuildup'])
                def_roster['shots'] += (3) * float(roster['shots'])
                model_fixture['featuresEuclidien']['def']['xG'] += float(roster['xG']) 
                model_fixture['featuresEuclidien']['def']['goals'] += float(roster['goals']) 
                model_fixture['featuresEuclidien']['def']['xA'] += float(roster['xA']) 
                model_fixture['featuresEuclidien']['def']['assists'] += float(roster['assists']) 
                model_fixture['featuresEuclidien']['def']['shots'] += float(roster['shots']) 
                model_fixture['featuresEuclidien']['def']['xGChain'] += float(roster['xGChain']) 
                model_fixture['featuresEuclidien']['def']['xGBuildup'] += float(roster['xGBuildup']) 
                
            if int(time) > 15 and position.startswith('M'):
                mil_roster['goals'] += (15) * float(roster['goals'])
                mil_roster['own_goals'] += (-10) * float(roster['own_goals'])
                mil_roster['yellow_card'] +=  float(roster['yellow_card']) *(-3) 
                mil_roster['red_card'] += (-5) * float(roster['red_card'])
                mil_roster['key_passes'] += (2) * float(roster['key_passes'])
                mil_roster['assists'] += (5) * float(roster['assists'])
                mil_roster['xg_chain'] += (10) * float(roster['xGChain'])
                mil_roster['xg_buildup'] += (10) * float(roster['xGBuildup'])
                mil_roster['shots'] += (3) * float(roster['shots'])
                model_fixture['featuresEuclidien']['mil']['xG'] += float(roster['xG']) 
                model_fixture['featuresEuclidien']['mil']['goals'] += float(roster['goals']) 
                model_fixture['featuresEuclidien']['mil']['xA'] += float(roster['xA']) 
                model_fixture['featuresEuclidien']['mil']['assists'] += float(roster['assists']) 
                model_fixture['featuresEuclidien']['mil']['shots'] += float(roster['shots']) 
                model_fixture['featuresEuclidien']['mil']['xGChain'] += float(roster['xGChain']) 
                model_fixture['featuresEuclidien']['mil']['xGBuildup'] += float(roster['xGBuildup'])
            if int(time) > 15 and position.startswith('AM'):
                att_mil_roster['goals'] += (15) * float(roster['goals'])
                att_mil_roster['own_goals'] += (-10) * float(roster['own_goals'])
                att_mil_roster['yellow_card'] +=  float(roster['yellow_card']) *(-3) 
                att_mil_roster['red_card'] += (-5) * float(roster['red_card'])
                att_mil_roster['key_passes'] += (2) * float(roster['key_passes'])
                att_mil_roster['assists'] += (5) * float(roster['assists'])
                att_mil_roster['xg_chain'] += (10) * float(roster['xGChain'])
                att_mil_roster['xg_buildup'] += (10) * float(roster['xGBuildup'])
                att_mil_roster['shots'] += (3) * float(roster['shots'])
                model_fixture['featuresEuclidien']['att_mil']['xG'] += float(roster['xG']) 
                model_fixture['featuresEuclidien']['att_mil']['goals'] += float(roster['goals']) 
                model_fixture['featuresEuclidien']['att_mil']['xA'] += float(roster['xA']) 
                model_fixture['featuresEuclidien']['att_mil']['assists'] += float(roster['assists']) 
                model_fixture['featuresEuclidien']['att_mil']['shots'] += float(roster['shots']) 
                model_fixture['featuresEuclidien']['att_mil']['xGChain'] += float(roster['xGChain']) 
                model_fixture['featuresEuclidien']['att_mil']['xGBuildup'] += float(roster['xGBuildup'])
            if int(time) > 15 and position.startswith('F'):
                att_roster['goals'] += (10) * float(roster['goals'])
                att_roster['own_goals'] += (-5) * float(roster['own_goals'])
                att_roster['yellow_card'] +=  float(roster['yellow_card']) *(-3) 
                att_roster['red_card'] += (-5) * float(roster['red_card'])
                att_roster['key_passes'] += (2) * float(roster['key_passes'])
                att_roster['assists'] += (5) * float(roster['assists'])
                att_roster['xg_chain'] += (10) * float(roster['xGChain'])
                att_roster['xg_buildup'] += (10) * float(roster['xGBuildup'])
                att_roster['shots'] += (3) * float(roster['shots'])
                model_fixture['featuresEuclidien']['att']['xG'] += float(roster['xG']) 
                model_fixture['featuresEuclidien']['att']['goals'] += float(roster['goals']) 
                model_fixture['featuresEuclidien']['att']['xA'] += float(roster['xA']) 
                model_fixture['featuresEuclidien']['att']['assists'] += float(roster['assists']) 
                model_fixture['featuresEuclidien']['att']['shots'] += float(roster['shots']) 
                model_fixture['featuresEuclidien']['att']['xGChain'] += float(roster['xGChain']) 
                model_fixture['featuresEuclidien']['att']['xGBuildup'] += float(roster['xGBuildup'])
            if int(time) > 15 and position.startswith('Sub'):
                sub_roster['goals'] += (15) * float(roster['goals'])
                sub_roster['own_goals'] += (-10) * float(roster['own_goals'])
                sub_roster['yellow_card'] +=  float(roster['yellow_card']) *(-3) 
                sub_roster['red_card'] += (-5) * float(roster['red_card'])
                sub_roster['key_passes'] += (2) * float(roster['key_passes'])
                sub_roster['assists'] += (5) * float(roster['assists'])
                sub_roster['xg_chain'] += (10) * float(roster['xGChain'])
                sub_roster['xg_buildup'] += (10) * float(roster['xGBuildup'])
                sub_roster['shots'] += (3) * float(roster['shots'])
                model_fixture['featuresEuclidien']['sub']['xG'] += float(roster['xG']) 
                model_fixture['featuresEuclidien']['sub']['goals'] += float(roster['goals']) 
                model_fixture['featuresEuclidien']['sub']['xA'] += float(roster['xA']) 
                model_fixture['featuresEuclidien']['sub']['assists'] += float(roster['assists']) 
                model_fixture['featuresEuclidien']['sub']['shots'] += float(roster['shots']) 
                model_fixture['featuresEuclidien']['sub']['xGChain'] += float(roster['xGChain']) 
                model_fixture['featuresEuclidien']['sub']['xGBuildup'] += float(roster['xGBuildup'])
        model_fixture['featuresEuclidien']['total']['xG'] = model_fixture['featuresEuclidien']['def']['xG'] + model_fixture['featuresEuclidien']['mil']['xG'] + model_fixture['featuresEuclidien']['att_mil']['xG'] + model_fixture['featuresEuclidien']['att']['xG'] + model_fixture['featuresEuclidien']['sub']['xG']
        model_fixture['featuresEuclidien']['total']['goals'] = model_fixture['featuresEuclidien']['def']['goals'] + model_fixture['featuresEuclidien']['mil']['goals'] + model_fixture['featuresEuclidien']['att_mil']['goals'] + model_fixture['featuresEuclidien']['att']['goals'] + model_fixture['featuresEuclidien']['sub']['goals']
        model_fixture['featuresEuclidien']['total']['xA'] = model_fixture['featuresEuclidien']['def']['xA'] + model_fixture['featuresEuclidien']['mil']['xA'] + model_fixture['featuresEuclidien']['att_mil']['xA'] + model_fixture['featuresEuclidien']['att']['xA'] + model_fixture['featuresEuclidien']['sub']['xA']
        model_fixture['featuresEuclidien']['total']['assists'] = model_fixture['featuresEuclidien']['def']['assists'] + model_fixture['featuresEuclidien']['mil']['assists'] + model_fixture['featuresEuclidien']['att_mil']['assists'] + model_fixture['featuresEuclidien']['att']['assists'] + model_fixture['featuresEuclidien']['sub']['assists']
        model_fixture['featuresEuclidien']['total']['shots'] = model_fixture['featuresEuclidien']['def']['shots'] + model_fixture['featuresEuclidien']['mil']['shots'] + model_fixture['featuresEuclidien']['att_mil']['shots'] + model_fixture['featuresEuclidien']['att']['shots'] + model_fixture['featuresEuclidien']['sub']['shots']
        model_fixture['featuresEuclidien']['total']['xGChain'] = model_fixture['featuresEuclidien']['def']['xGChain'] + model_fixture['featuresEuclidien']['mil']['xGChain'] + model_fixture['featuresEuclidien']['att_mil']['xGChain'] + model_fixture['featuresEuclidien']['att']['xGChain'] + model_fixture['featuresEuclidien']['sub']['xGChain']
        model_fixture['featuresEuclidien']['total']['xGBuildup'] = model_fixture['featuresEuclidien']['def']['xGBuildup'] + model_fixture['featuresEuclidien']['mil']['xGBuildup'] + model_fixture['featuresEuclidien']['att_mil']['xGBuildup'] + model_fixture['featuresEuclidien']['att']['xGBuildup'] + model_fixture['featuresEuclidien']['sub']['xGBuildup']
        
        def_points += def_roster['goals']
        def_points += def_roster['assists']
        def_points += def_roster['key_passes']
        def_points += def_roster['own_goals']
        def_points += def_roster['yellow_card']
        def_points += def_roster['red_card']
        def_points += def_roster['shots']
        def_points += def_roster['xg_chain']
        def_points += def_roster['xg_buildup']
        mil_points += mil_roster['goals']
        mil_points += mil_roster['assists']
        mil_points += mil_roster['key_passes']
        mil_points += mil_roster['own_goals']
        mil_points += mil_roster['yellow_card']
        mil_points += mil_roster['red_card']
        mil_points += mil_roster['shots']
        mil_points += mil_roster['xg_chain']
        mil_points += mil_roster['xg_buildup']
        att_mil_points += att_mil_roster['goals']
        att_mil_points += att_mil_roster['assists']
        att_mil_points += att_mil_roster['key_passes']
        att_mil_points += att_mil_roster['own_goals']
        att_mil_points += att_mil_roster['yellow_card']
        att_mil_points += att_mil_roster['red_card']
        att_mil_points += att_mil_roster['shots']
        att_mil_points += att_mil_roster['xg_chain']
        att_mil_points += att_mil_roster['xg_buildup']
        att_points += att_roster['goals']
        att_points += att_roster['assists']
        att_points += att_roster['key_passes']
        att_points += att_roster['own_goals']
        att_points += att_roster['yellow_card']
        att_points += att_roster['red_card']
        att_points += att_roster['shots']
        att_points += att_roster['xg_chain']
        att_points += att_roster['xg_buildup']
        sub_points += sub_roster['assists']
        sub_points += sub_roster['key_passes']
        sub_points += sub_roster['own_goals']
        sub_points += sub_roster['yellow_card']
        sub_points += sub_roster['red_card']
        sub_points += sub_roster['shots']
        sub_points += sub_roster['xg_chain']
        sub_points += sub_roster['xg_buildup']
        total_points = 0
        total_points += def_points
        total_points += mil_points
        total_points += att_mil_points
        total_points += att_points
        total_points += sub_points
        model_fixture['sorareData']['def'] = str(def_points)
        model_fixture['sorareData']['mil'] = str(mil_points)
        model_fixture['sorareData']['att_mil'] = str(att_mil_points)
        model_fixture['sorareData']['att'] = str(att_points)
        model_fixture['sorareData']['sub'] = str(sub_points)
        model_fixture['sorareData']['total'] = str(total_points)
        model_team['fixtures'].append(model_fixture)
    totalPoints = 0
    for model_t in model_team['fixtures']:
        totalPoints += float(model_t['sorareData']['total'])
    if len(model_team['fixtures']) > 0:
        model_team['global']['points'] = str(totalPoints / len(model_team['fixtures']))
    model_teams_whoscored.append(model_team)

model_teams_whoscored.sort(key=lambda x: float(x['global']['points']), reverse=True) 
index = 0
for tn in model_teams_whoscored:
    index += 1
    tn['rank'] = index
    # Classement championnat
    model_teams_whoscored_clone_filtered = filter(lambda t: t["idTeam"] != tn["idTeam"] and t["nameLeague"] == tn["nameLeague"], model_teams_whoscored)
    fixtures_n =  tn["fixtures"]
    def_n = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
    mil_n = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
    att_mil_n = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
    att_n = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
    sub_n = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
    total_n = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
    rank_dist_tn = []
    for fn in fixtures_n:
        # def
        def_n['xG'] += float(fn["featuresEuclidien"]["def"]["xG"])
        def_n['goals'] += float(fn["featuresEuclidien"]["def"]["goals"])
        def_n['xA'] += float(fn["featuresEuclidien"]["def"]["xA"])
        def_n['assists'] += float(fn["featuresEuclidien"]["def"]["assists"])
        def_n['shots'] += float(fn["featuresEuclidien"]["def"]["shots"])
        def_n['xGChain'] += float(fn["featuresEuclidien"]["def"]["xGChain"])
        def_n['xGBuildup'] += float(fn["featuresEuclidien"]["def"]["xGBuildup"])
        # mil
        mil_n['xG'] += float(fn["featuresEuclidien"]["mil"]["xG"])
        mil_n['goals'] += float(fn["featuresEuclidien"]["mil"]["goals"])
        mil_n['xA'] += float(fn["featuresEuclidien"]["mil"]["xA"])
        mil_n['assists'] += float(fn["featuresEuclidien"]["mil"]["assists"])
        mil_n['shots'] += float(fn["featuresEuclidien"]["mil"]["shots"])
        mil_n['xGChain'] += float(fn["featuresEuclidien"]["mil"]["xGChain"])
        mil_n['xGBuildup'] += float(fn["featuresEuclidien"]["mil"]["xGBuildup"])
        # att_mil
        att_mil_n['xG'] += float(fn["featuresEuclidien"]["att_mil"]["xG"])
        att_mil_n['goals'] += float(fn["featuresEuclidien"]["att_mil"]["goals"])
        att_mil_n['xA'] += float(fn["featuresEuclidien"]["att_mil"]["xA"])
        att_mil_n['assists'] += float(fn["featuresEuclidien"]["att_mil"]["assists"])
        att_mil_n['shots'] += float(fn["featuresEuclidien"]["att_mil"]["shots"])
        att_mil_n['xGChain'] += float(fn["featuresEuclidien"]["att_mil"]["xGChain"])
        att_mil_n['xGBuildup'] += float(fn["featuresEuclidien"]["att_mil"]["xGBuildup"])
        # att
        att_n['xG'] += float(fn["featuresEuclidien"]["att"]["xG"])
        att_n['goals'] += float(fn["featuresEuclidien"]["att"]["goals"])
        att_n['xA'] += float(fn["featuresEuclidien"]["att"]["xA"])
        att_n['assists'] += float(fn["featuresEuclidien"]["att"]["assists"])
        att_n['shots'] += float(fn["featuresEuclidien"]["att"]["shots"])
        att_n['xGChain'] += float(fn["featuresEuclidien"]["att"]["xGChain"])
        att_n['xGBuildup'] += float(fn["featuresEuclidien"]["att"]["xGBuildup"])
        # sub
        sub_n['xG'] += float(fn["featuresEuclidien"]["sub"]["xG"])
        sub_n['goals'] += float(fn["featuresEuclidien"]["sub"]["goals"])
        sub_n['xA'] += float(fn["featuresEuclidien"]["sub"]["xA"])
        sub_n['assists'] += float(fn["featuresEuclidien"]["sub"]["assists"])
        sub_n['shots'] += float(fn["featuresEuclidien"]["sub"]["shots"])
        sub_n['xGChain'] += float(fn["featuresEuclidien"]["sub"]["xGChain"])
        sub_n['xGBuildup'] += float(fn["featuresEuclidien"]["sub"]["xGBuildup"])
        # total
        total_n['xG'] += float(fn["featuresEuclidien"]["total"]["xG"])
        total_n['goals'] += float(fn["featuresEuclidien"]["total"]["goals"])
        total_n['xA'] += float(fn["featuresEuclidien"]["total"]["xA"])
        total_n['assists'] += float(fn["featuresEuclidien"]["total"]["assists"])
        total_n['shots'] += float(fn["featuresEuclidien"]["total"]["shots"])
        total_n['xGChain'] += float(fn["featuresEuclidien"]["total"]["xGChain"])
        total_n['xGBuildup'] += float(fn["featuresEuclidien"]["total"]["xGBuildup"])
    for tf in model_teams_whoscored_clone_filtered:
        fixtures_f =  tf["fixtures"]
        def_f = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
        mil_f = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
        att_mil_f = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
        att_f = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
        sub_f = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
        total_f = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
        for ff in fixtures_f:
            # def
            def_f['xG'] += float(ff["featuresEuclidien"]["def"]["xG"])
            def_f['goals'] += float(ff["featuresEuclidien"]["def"]["goals"])
            def_f['xA'] += float(ff["featuresEuclidien"]["def"]["xA"])
            def_f['assists'] += float(ff["featuresEuclidien"]["def"]["assists"])
            def_f['shots'] += float(ff["featuresEuclidien"]["def"]["shots"])
            def_f['xGChain'] += float(ff["featuresEuclidien"]["def"]["xGChain"])
            def_f['xGBuildup'] += float(ff["featuresEuclidien"]["def"]["xGBuildup"])
            # mil
            mil_f['xG'] += float(ff["featuresEuclidien"]["mil"]["xG"])
            mil_f['goals'] += float(ff["featuresEuclidien"]["mil"]["goals"])
            mil_f['xA'] += float(ff["featuresEuclidien"]["mil"]["xA"])
            mil_f['assists'] += float(ff["featuresEuclidien"]["mil"]["assists"])
            mil_f['shots'] += float(ff["featuresEuclidien"]["mil"]["shots"])
            mil_f['xGChain'] += float(ff["featuresEuclidien"]["mil"]["xGChain"])
            mil_f['xGBuildup'] += float(ff["featuresEuclidien"]["mil"]["xGBuildup"])
            # att_mil
            att_mil_f['xG'] += float(ff["featuresEuclidien"]["att_mil"]["xG"])
            att_mil_f['goals'] += float(ff["featuresEuclidien"]["att_mil"]["goals"])
            att_mil_f['xA'] += float(ff["featuresEuclidien"]["att_mil"]["xA"])
            att_mil_f['assists'] += float(ff["featuresEuclidien"]["att_mil"]["assists"])
            att_mil_f['shots'] += float(ff["featuresEuclidien"]["att_mil"]["shots"])
            att_mil_f['xGChain'] += float(ff["featuresEuclidien"]["att_mil"]["xGChain"])
            att_mil_f['xGBuildup'] += float(ff["featuresEuclidien"]["att_mil"]["xGBuildup"])
            # att
            att_f['xG'] += float(ff["featuresEuclidien"]["att"]["xG"])
            att_f['goals'] += float(ff["featuresEuclidien"]["att"]["goals"])
            att_f['xA'] += float(ff["featuresEuclidien"]["att"]["xA"])
            att_f['assists'] += float(ff["featuresEuclidien"]["att"]["assists"])
            att_f['shots'] += float(ff["featuresEuclidien"]["att"]["shots"])
            att_f['xGChain'] += float(ff["featuresEuclidien"]["att"]["xGChain"])
            att_f['xGBuildup'] += float(ff["featuresEuclidien"]["att"]["xGBuildup"])
            # sub
            sub_f['xG'] += float(ff["featuresEuclidien"]["sub"]["xG"])
            sub_f['goals'] += float(ff["featuresEuclidien"]["sub"]["goals"])
            sub_f['xA'] += float(ff["featuresEuclidien"]["sub"]["xA"])
            sub_f['assists'] += float(ff["featuresEuclidien"]["sub"]["assists"])
            sub_f['shots'] += float(ff["featuresEuclidien"]["sub"]["shots"])
            sub_f['xGChain'] += float(ff["featuresEuclidien"]["sub"]["xGChain"])
            sub_f['xGBuildup'] += float(ff["featuresEuclidien"]["sub"]["xGBuildup"])
            # total
            total_f['xG'] += float(ff["featuresEuclidien"]["total"]["xG"])
            total_f['goals'] += float(ff["featuresEuclidien"]["total"]["goals"])
            total_f['xA'] += float(ff["featuresEuclidien"]["total"]["xA"])
            total_f['assists'] += float(ff["featuresEuclidien"]["total"]["assists"])
            total_f['shots'] += float(ff["featuresEuclidien"]["total"]["shots"])
            total_f['xGChain'] += float(ff["featuresEuclidien"]["total"]["xGChain"])
            total_f['xGBuildup'] += float(ff["featuresEuclidien"]["total"]["xGBuildup"])
        
        # defense
        def_n_array = np.array((def_n['xG'],def_n['goals'],def_n['xA'],def_n['assists'],def_n['shots'],def_n['xGChain'],def_n['xGBuildup']))
        def_f_array = np.array((def_f['xG'],def_f['goals'],def_f['xA'],def_f['assists'],def_f['shots'],def_f['xGChain'],def_f['xGBuildup']))
        
        # milieu
        mil_n_array = np.array((mil_n['xG'],mil_n['goals'],mil_n['xA'],mil_n['assists'],mil_n['shots'],mil_n['xGChain'],mil_n['xGBuildup']))
        mil_f_array = np.array((mil_f['xG'],mil_f['goals'],mil_f['xA'],mil_f['assists'],mil_f['shots'],mil_f['xGChain'],mil_f['xGBuildup']))
        
        # milieu attaquant
        att_mil_n_array = np.array((att_mil_n['xG'],att_mil_n['goals'],att_mil_n['xA'],att_mil_n['assists'],att_mil_n['shots'],att_mil_n['xGChain'],att_mil_n['xGBuildup']))
        att_mil_f_array = np.array((att_mil_f['xG'],att_mil_f['goals'],att_mil_f['xA'],att_mil_f['assists'],att_mil_f['shots'],att_mil_f['xGChain'],att_mil_f['xGBuildup']))
        
        # attaquant
        att_n_array = np.array((att_n['xG'],att_n['goals'],att_n['xA'],att_n['assists'],att_n['shots'],att_n['xGChain'],att_n['xGBuildup']))
        att_f_array = np.array((att_f['xG'],att_f['goals'],att_f['xA'],att_f['assists'],att_f['shots'],att_f['xGChain'],att_f['xGBuildup']))
        
        # remplaçant
        sub_n_array = np.array((sub_n['xG'],sub_n['goals'],sub_n['xA'],sub_n['assists'],sub_n['shots'],sub_n['xGChain'],sub_n['xGBuildup']))
        sub_f_array = np.array((sub_f['xG'],sub_f['goals'],sub_f['xA'],sub_f['assists'],sub_f['shots'],sub_f['xGChain'],sub_f['xGBuildup']))
        
        # total
        total_n_array = np.array((total_n['xG'],total_n['goals'],total_n['xA'],total_n['assists'],total_n['shots'],total_n['xGChain'],total_n['xGBuildup']))
        total_f_array = np.array((total_f['xG'],total_f['goals'],total_f['xA'],total_f['assists'],total_f['shots'],total_f['xGChain'],total_f['xGBuildup']))

        # distance euclidienne
        dist_def = np.linalg.norm(def_n_array-def_f_array)
        dist_mil = np.linalg.norm(mil_n_array-mil_f_array)
        dist_att_mil = np.linalg.norm(att_mil_n_array-att_mil_f_array)
        dist_att = np.linalg.norm(att_n_array-att_f_array)
        dist_sub = np.linalg.norm(sub_n_array-sub_f_array)
        dist_total = np.linalg.norm(total_n_array-total_f_array)
        distance = {"nameTeam": tf["nameTeam"], "idTeam": tf["idTeam"],
          "dist_def": dist_def, "dist_mil": dist_mil,
          "dist_att_mil": dist_att_mil, "dist_att": dist_att,
          "dist_sub": dist_sub, "dist_total": dist_total}
        rank_dist_tn.append(distance)
    
    rank_dist_tn.sort(key=lambda x: float(x['dist_def'])) 
    for i in range(0, 3):
        tn["global"]["rank_def_league"].append(rank_dist_tn[i])
   
    rank_dist_tn.sort(key=lambda x: float(x['dist_mil'])) 
    for i in range(0, 3):
        tn["global"]["rank_mil_league"].append(rank_dist_tn[i])
   
    rank_dist_tn.sort(key=lambda x: float(x['dist_att_mil'])) 
    for i in range(0, 3):
        tn["global"]["rank_att_mil_league"].append(rank_dist_tn[i])
   
    rank_dist_tn.sort(key=lambda x: float(x['dist_att'])) 
    for i in range(0, 3):
        tn["global"]["rank_att_league"].append(rank_dist_tn[i])
   
    rank_dist_tn.sort(key=lambda x: float(x['dist_sub'])) 
    for i in range(0, 3):
        tn["global"]["rank_sub_league"].append(rank_dist_tn[i])
    
    rank_dist_tn.sort(key=lambda x: float(x['dist_total'])) 
    for i in range(0, 3):
        tn["global"]["rank_total_league"].append(rank_dist_tn[i])
    
    # Classement Europe
    model_teams_whoscored_clone_filtered = filter(lambda t: t["idTeam"] != tn["idTeam"], model_teams_whoscored)
    fixtures_n =  tn["fixtures"]
    def_n = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
    mil_n = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
    att_mil_n = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
    att_n = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
    sub_n = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
    total_n = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
    rank_dist_tn = []
    for fn in fixtures_n:
        # def
        def_n['xG'] += float(fn["featuresEuclidien"]["def"]["xG"])
        def_n['goals'] += float(fn["featuresEuclidien"]["def"]["goals"])
        def_n['xA'] += float(fn["featuresEuclidien"]["def"]["xA"])
        def_n['assists'] += float(fn["featuresEuclidien"]["def"]["assists"])
        def_n['shots'] += float(fn["featuresEuclidien"]["def"]["shots"])
        def_n['xGChain'] += float(fn["featuresEuclidien"]["def"]["xGChain"])
        def_n['xGBuildup'] += float(fn["featuresEuclidien"]["def"]["xGBuildup"])
        # mil
        mil_n['xG'] += float(fn["featuresEuclidien"]["mil"]["xG"])
        mil_n['goals'] += float(fn["featuresEuclidien"]["mil"]["goals"])
        mil_n['xA'] += float(fn["featuresEuclidien"]["mil"]["xA"])
        mil_n['assists'] += float(fn["featuresEuclidien"]["mil"]["assists"])
        mil_n['shots'] += float(fn["featuresEuclidien"]["mil"]["shots"])
        mil_n['xGChain'] += float(fn["featuresEuclidien"]["mil"]["xGChain"])
        mil_n['xGBuildup'] += float(fn["featuresEuclidien"]["mil"]["xGBuildup"])
        # att_mil
        att_mil_n['xG'] += float(fn["featuresEuclidien"]["att_mil"]["xG"])
        att_mil_n['goals'] += float(fn["featuresEuclidien"]["att_mil"]["goals"])
        att_mil_n['xA'] += float(fn["featuresEuclidien"]["att_mil"]["xA"])
        att_mil_n['assists'] += float(fn["featuresEuclidien"]["att_mil"]["assists"])
        att_mil_n['shots'] += float(fn["featuresEuclidien"]["att_mil"]["shots"])
        att_mil_n['xGChain'] += float(fn["featuresEuclidien"]["att_mil"]["xGChain"])
        att_mil_n['xGBuildup'] += float(fn["featuresEuclidien"]["att_mil"]["xGBuildup"])
        # att
        att_n['xG'] += float(fn["featuresEuclidien"]["att"]["xG"])
        att_n['goals'] += float(fn["featuresEuclidien"]["att"]["goals"])
        att_n['xA'] += float(fn["featuresEuclidien"]["att"]["xA"])
        att_n['assists'] += float(fn["featuresEuclidien"]["att"]["assists"])
        att_n['shots'] += float(fn["featuresEuclidien"]["att"]["shots"])
        att_n['xGChain'] += float(fn["featuresEuclidien"]["att"]["xGChain"])
        att_n['xGBuildup'] += float(fn["featuresEuclidien"]["att"]["xGBuildup"])
        # sub
        sub_n['xG'] += float(fn["featuresEuclidien"]["sub"]["xG"])
        sub_n['goals'] += float(fn["featuresEuclidien"]["sub"]["goals"])
        sub_n['xA'] += float(fn["featuresEuclidien"]["sub"]["xA"])
        sub_n['assists'] += float(fn["featuresEuclidien"]["sub"]["assists"])
        sub_n['shots'] += float(fn["featuresEuclidien"]["sub"]["shots"])
        sub_n['xGChain'] += float(fn["featuresEuclidien"]["sub"]["xGChain"])
        sub_n['xGBuildup'] += float(fn["featuresEuclidien"]["sub"]["xGBuildup"])
        # total
        total_n['xG'] += float(fn["featuresEuclidien"]["total"]["xG"])
        total_n['goals'] += float(fn["featuresEuclidien"]["total"]["goals"])
        total_n['xA'] += float(fn["featuresEuclidien"]["total"]["xA"])
        total_n['assists'] += float(fn["featuresEuclidien"]["total"]["assists"])
        total_n['shots'] += float(fn["featuresEuclidien"]["total"]["shots"])
        total_n['xGChain'] += float(fn["featuresEuclidien"]["total"]["xGChain"])
        total_n['xGBuildup'] += float(fn["featuresEuclidien"]["total"]["xGBuildup"])
    for tf in model_teams_whoscored_clone_filtered:
        fixtures_f =  tf["fixtures"]
        def_f = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
        mil_f = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
        att_mil_f = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
        att_f = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
        sub_f = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
        total_f = {'xG':0, 'goals': 0, 'xA': 0, 'assists': 0, 'shots': 0, 'xGChain': 0, 'xGBuildup': 0 }
        for ff in fixtures_f:
            # def
            def_f['xG'] += float(ff["featuresEuclidien"]["def"]["xG"])
            def_f['goals'] += float(ff["featuresEuclidien"]["def"]["goals"])
            def_f['xA'] += float(ff["featuresEuclidien"]["def"]["xA"])
            def_f['assists'] += float(ff["featuresEuclidien"]["def"]["assists"])
            def_f['shots'] += float(ff["featuresEuclidien"]["def"]["shots"])
            def_f['xGChain'] += float(ff["featuresEuclidien"]["def"]["xGChain"])
            def_f['xGBuildup'] += float(ff["featuresEuclidien"]["def"]["xGBuildup"])
            # mil
            mil_f['xG'] += float(ff["featuresEuclidien"]["mil"]["xG"])
            mil_f['goals'] += float(ff["featuresEuclidien"]["mil"]["goals"])
            mil_f['xA'] += float(ff["featuresEuclidien"]["mil"]["xA"])
            mil_f['assists'] += float(ff["featuresEuclidien"]["mil"]["assists"])
            mil_f['shots'] += float(ff["featuresEuclidien"]["mil"]["shots"])
            mil_f['xGChain'] += float(ff["featuresEuclidien"]["mil"]["xGChain"])
            mil_f['xGBuildup'] += float(ff["featuresEuclidien"]["mil"]["xGBuildup"])
            # att_mil
            att_mil_f['xG'] += float(ff["featuresEuclidien"]["att_mil"]["xG"])
            att_mil_f['goals'] += float(ff["featuresEuclidien"]["att_mil"]["goals"])
            att_mil_f['xA'] += float(ff["featuresEuclidien"]["att_mil"]["xA"])
            att_mil_f['assists'] += float(ff["featuresEuclidien"]["att_mil"]["assists"])
            att_mil_f['shots'] += float(ff["featuresEuclidien"]["att_mil"]["shots"])
            att_mil_f['xGChain'] += float(ff["featuresEuclidien"]["att_mil"]["xGChain"])
            att_mil_f['xGBuildup'] += float(ff["featuresEuclidien"]["att_mil"]["xGBuildup"])
            # att
            att_f['xG'] += float(ff["featuresEuclidien"]["att"]["xG"])
            att_f['goals'] += float(ff["featuresEuclidien"]["att"]["goals"])
            att_f['xA'] += float(ff["featuresEuclidien"]["att"]["xA"])
            att_f['assists'] += float(ff["featuresEuclidien"]["att"]["assists"])
            att_f['shots'] += float(ff["featuresEuclidien"]["att"]["shots"])
            att_f['xGChain'] += float(ff["featuresEuclidien"]["att"]["xGChain"])
            att_f['xGBuildup'] += float(ff["featuresEuclidien"]["att"]["xGBuildup"])
            # sub
            sub_f['xG'] += float(ff["featuresEuclidien"]["sub"]["xG"])
            sub_f['goals'] += float(ff["featuresEuclidien"]["sub"]["goals"])
            sub_f['xA'] += float(ff["featuresEuclidien"]["sub"]["xA"])
            sub_f['assists'] += float(ff["featuresEuclidien"]["sub"]["assists"])
            sub_f['shots'] += float(ff["featuresEuclidien"]["sub"]["shots"])
            sub_f['xGChain'] += float(ff["featuresEuclidien"]["sub"]["xGChain"])
            sub_f['xGBuildup'] += float(ff["featuresEuclidien"]["sub"]["xGBuildup"])
            # total
            total_f['xG'] += float(ff["featuresEuclidien"]["total"]["xG"])
            total_f['goals'] += float(ff["featuresEuclidien"]["total"]["goals"])
            total_f['xA'] += float(ff["featuresEuclidien"]["total"]["xA"])
            total_f['assists'] += float(ff["featuresEuclidien"]["total"]["assists"])
            total_f['shots'] += float(ff["featuresEuclidien"]["total"]["shots"])
            total_f['xGChain'] += float(ff["featuresEuclidien"]["total"]["xGChain"])
            total_f['xGBuildup'] += float(ff["featuresEuclidien"]["total"]["xGBuildup"])
        
        # defense
        def_n_array = np.array((def_n['xG'],def_n['goals'],def_n['xA'],def_n['assists'],def_n['shots'],def_n['xGChain'],def_n['xGBuildup']))
        def_f_array = np.array((def_f['xG'],def_f['goals'],def_f['xA'],def_f['assists'],def_f['shots'],def_f['xGChain'],def_f['xGBuildup']))
        
        # milieu
        mil_n_array = np.array((mil_n['xG'],mil_n['goals'],mil_n['xA'],mil_n['assists'],mil_n['shots'],mil_n['xGChain'],mil_n['xGBuildup']))
        mil_f_array = np.array((mil_f['xG'],mil_f['goals'],mil_f['xA'],mil_f['assists'],mil_f['shots'],mil_f['xGChain'],mil_f['xGBuildup']))
        
        # milieu attaquant
        att_mil_n_array = np.array((att_mil_n['xG'],att_mil_n['goals'],att_mil_n['xA'],att_mil_n['assists'],att_mil_n['shots'],att_mil_n['xGChain'],att_mil_n['xGBuildup']))
        att_mil_f_array = np.array((att_mil_f['xG'],att_mil_f['goals'],att_mil_f['xA'],att_mil_f['assists'],att_mil_f['shots'],att_mil_f['xGChain'],att_mil_f['xGBuildup']))
        
        # attaquant
        att_n_array = np.array((att_n['xG'],att_n['goals'],att_n['xA'],att_n['assists'],att_n['shots'],att_n['xGChain'],att_n['xGBuildup']))
        att_f_array = np.array((att_f['xG'],att_f['goals'],att_f['xA'],att_f['assists'],att_f['shots'],att_f['xGChain'],att_f['xGBuildup']))
        
        # remplaçant
        sub_n_array = np.array((sub_n['xG'],sub_n['goals'],sub_n['xA'],sub_n['assists'],sub_n['shots'],sub_n['xGChain'],sub_n['xGBuildup']))
        sub_f_array = np.array((sub_f['xG'],sub_f['goals'],sub_f['xA'],sub_f['assists'],sub_f['shots'],sub_f['xGChain'],sub_f['xGBuildup']))
        
        # total
        total_n_array = np.array((total_n['xG'],total_n['goals'],total_n['xA'],total_n['assists'],total_n['shots'],total_n['xGChain'],total_n['xGBuildup']))
        total_f_array = np.array((total_f['xG'],total_f['goals'],total_f['xA'],total_f['assists'],total_f['shots'],total_f['xGChain'],total_f['xGBuildup']))

        # distance euclidienne
        dist_def = np.linalg.norm(def_n_array-def_f_array)
        dist_mil = np.linalg.norm(mil_n_array-mil_f_array)
        dist_att_mil = np.linalg.norm(att_mil_n_array-att_mil_f_array)
        dist_att = np.linalg.norm(att_n_array-att_f_array)
        dist_sub = np.linalg.norm(sub_n_array-sub_f_array)
        dist_total = np.linalg.norm(total_n_array-total_f_array)
        distance = {"nameTeam": tf["nameTeam"], "idTeam": tf["idTeam"],
          "dist_def": dist_def, "dist_mil": dist_mil,
          "dist_att_mil": dist_att_mil, "dist_att": dist_att,
          "dist_sub": dist_sub, "dist_total": dist_total}
        rank_dist_tn.append(distance)
    rank_dist_tn.sort(key=lambda x: float(x['dist_def'])) 
    for i in range(0, 3):
        tn["global"]["rank_def_europe"].append(rank_dist_tn[i])
   
    rank_dist_tn.sort(key=lambda x: float(x['dist_mil'])) 
    for i in range(0, 3):
        tn["global"]["rank_mil_europe"].append(rank_dist_tn[i])
   
    rank_dist_tn.sort(key=lambda x: float(x['dist_att_mil'])) 
    for i in range(0, 3):
        tn["global"]["rank_att_mil_europe"].append(rank_dist_tn[i])
   
    rank_dist_tn.sort(key=lambda x: float(x['dist_att'])) 
    for i in range(0, 3):
        tn["global"]["rank_att_europe"].append(rank_dist_tn[i])
   
    rank_dist_tn.sort(key=lambda x: float(x['dist_sub'])) 
    for i in range(0, 3):
        tn["global"]["rank_sub_europe"].append(rank_dist_tn[i])
    
    rank_dist_tn.sort(key=lambda x: float(x['dist_total'])) 
    for i in range(0, 3):
        tn["global"]["rank_total_europe"].append(rank_dist_tn[i])
    
cloudantService.persistModelDocument(opta_constants.model_team_whoscored+'_'+season_current,"rank_sorare",model_teams_whoscored)       
