from lib.nba import nba_factory
from constants import opta_constants;
from lib.cloudant.service import cloudant_db_service
cloudantService = cloudant_db_service.Cloudant()
nbaService = nba_factory.NbaService()
year = opta_constants.nba_season
url = opta_constants.base_url_nba_per_game.format(year)
player_stats = nbaService.getStats(url)
teamsName = nbaService.getTeamsName(player_stats)
# Advanced
cloudantService.createDatabase(opta_constants.dbname_nba_per_poss+'_'+str(year))
player_stats_per_100_poss = nbaService.getStats(opta_constants.base_url_nba_per_poss.format(year))
allTeamsPer100Poss = nbaService.getAllStatsPerPoss(teamsName,player_stats_per_100_poss)
for t in allTeamsPer100Poss:
    for p in t['players']:
        cloudantService.persistPlayerNBA(
            opta_constants.dbname_nba_per_poss+'_'+str(year),
            p['name'],
            t['team'],
            p['games'],
            p
        )  