from lib.nba import nba_factory
from constants import opta_constants;
from lib.cloudant.service import cloudant_db_service
from ibm_cloud_sdk_core.api_exception import ApiException
cloudantService = cloudant_db_service.Cloudant()
nbaService = nba_factory.NbaService()
year = opta_constants.nba_season
url = opta_constants.base_url_nba_per_game.format(year)
player_stats = nbaService.getStats(url)
teamsName = nbaService.getTeamsName(player_stats)
# Per Game 
cloudantService.createDatabase(opta_constants.dbname_nba_per_game+'_'+str(year))
allTeamsPerGame = nbaService.getAllStats(teamsName,player_stats)
for t in allTeamsPerGame:
    for p in t['players']:
        cloudantService.persistPlayerNBA(
            opta_constants.dbname_nba_per_game+'_'+str(year),
            p['name'],
            t['team'],
            p['games'],
            p
        )
