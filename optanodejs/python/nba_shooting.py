from lib.nba import nba_factory
from constants import opta_constants;
from lib.cloudant.service import cloudant_db_service
from ibm_cloud_sdk_core.api_exception import ApiException
cloudantService = cloudant_db_service.Cloudant()
nbaService = nba_factory.NbaService()
year = opta_constants.nba_season
url = opta_constants.base_url_nba_per_game.format(year)
player_stats = nbaService.getStats(url)
teamsName = nbaService.getTeamsName(player_stats)
# Shooting
cloudantService.createDatabase(opta_constants.dbname_nba_shooting+'_'+str(year))
player_stats_shooting = nbaService.getStatsShooting(opta_constants.base_url_nba_shooting.format(year))
allTeamsShooting = nbaService.getAllStatsShooting(teamsName,player_stats_shooting)
for ts in allTeamsShooting:
    for ps in ts['players']:
        cloudantService.persistPlayerNBA(
            opta_constants.dbname_nba_shooting+'_'+str(year),
            ps['name'],
            ts['team'],
            ps['games'],
            ps
        )  