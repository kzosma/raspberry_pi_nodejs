from lib.nba import nba_factory
from constants import opta_constants;
from lib.cloudant.service import cloudant_db_service
from ibm_cloud_sdk_core.api_exception import ApiException
cloudantService = cloudant_db_service.Cloudant()
nbaService = nba_factory.NbaService()
year = opta_constants.nba_season
url = opta_constants.base_url_nba_per_game.format(year)
player_stats = nbaService.getStats(url)
teamsName = nbaService.getTeamsName(player_stats)
# Total 
cloudantService.createDatabase(opta_constants.dbname_nba_totals+'_'+str(year))
player_stats_total = nbaService.getStats(opta_constants.base_url_nba_totals.format(year))
allTeamsTotal = nbaService.getAllStats(teamsName,player_stats_total)
for tt in allTeamsTotal:
    for pt in tt['players']:
        cloudantService.persistPlayerNBA(
            opta_constants.dbname_nba_totals+'_'+str(year),
            pt['name'],
            tt['team'],
            pt['games'],
            pt
        ) 
