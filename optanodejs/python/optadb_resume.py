from datetime import datetime
from constants import opta_constants
from lib.understat import understat_factory
from lib.cloudant.service import cloudant_db_service
import numpy as np
import json


# Function search fixture by _id and dayMatch
def searchFixture_By_ID_DayMatch(allteams, id, daymatch):
       desired_team = None
       desired_fixture = None
       for team in allteams:
              if team['doc']["_id"] == id:
                     desired_team = team
                     break
       if desired_team:
              for fixture in desired_team['doc']["fixtures"]:
                     if fixture["dayMatch"] == daymatch:
                            desired_fixture = fixture
                            break
       return desired_fixture
# GET Formation
def getFormation(fixture):
       formation = []
       if fixture is not None and isinstance(fixture, dict):
              if ('formation' in fixture):
                     for formation_name, formation_data in fixture['formation'].items():
                            formation.append({
                            'type': formation_name,
                            'time': formation_data["time"],
                            'shots': formation_data["shots"],
                            'goals': formation_data["goals"],
                            'xg': formation_data["xG"] 
              })
       return formation
# GET Timing
def getTiming(fixture):
       timing = []
       if fixture is not None and isinstance(fixture, dict):
              if('timing' in fixture):
                     for timing_name, timing_data in fixture['timing'].items():
                            timing.append({
                     'type': timing_name,
                     'shots': timing_data["shots"],
                     'goals': timing_data["goals"],
                     'xg': timing_data["xG"] 
              })
       return timing

# GET Situation
def getSituation(fixture):
       situation = []
       if fixture is not None and isinstance(fixture, dict):
              if('situation' in fixture):
                     for situation_name, situation_data in fixture['situation'].items():
                            situation.append({
                            'type': situation_name,
                            'shots': situation_data["shots"],
                            'goals': situation_data["goals"],
                            'xg': situation_data["xG"] 
              })
       return situation

# GET GameState
def getGameState(fixture):
       gs = []
       if fixture is not None and isinstance(fixture, dict):
              if('gameState' in fixture):
                     for gs_name, gs_data in fixture['gameState'].items():
                            gs.append({
                            'type': gs_name,
                            'time': gs_data["time"],
                            'shots': gs_data["shots"],
                            'goals': gs_data["goals"],
                            'xg': gs_data["xG"] 
              })
       return gs

# GET Roster
def getRoster(fixture, h_a):
       roster = []
       if(fixture is not None and 'rosterMatch' in fixture):
              rosterData = fixture['rosterMatch']['h'] if h_a == 'h' else fixture['rosterMatch']['a'] 
              for roster_name, roster_data in rosterData.items():
                     roster.append({
                     'player_name': roster_data["player"],
                     'position': roster_data["position"],
                     'shots': roster_data["shots"],
                     'goals': roster_data["goals"],
                     'xG': roster_data["xG"],
                     'xGChain': roster_data["xGChain"],
                     'xGBuildup': roster_data["xGBuildup"],
                     'key_passes': roster_data["key_passes"],
                     'assists': roster_data["assists"],
                     'time': roster_data["time"]
              })
       return roster

understatService = understat_factory.UnderStatService()
cloudantService = cloudant_db_service.Cloudant()
season = '2023'
allTeamsFromDB = cloudantService.getAllDataFromDB(opta_constants.dbname_teams+'_'+season)
# Pour chaque ligue
for league in opta_constants.leagues:
    leagueNameDB = 'optadb_resume_'+league.lower()+'_'+season
    # Suppression du document
    cloudantService.deleteDatabase(leagueNameDB)
    cloudantService.createDatabase(leagueNameDB)
    map_data_league = []
    for team in allTeamsFromDB:
                if team['doc']['nameLeague'] == league:
                    map_data_league.append(team['doc'])
    # Pour chaque journée
    for dayMatch in range(1, len(map_data_league)*2 -2):
            map_data_league_filter_by_day = [o for o in map_data_league if any(fixture["h_a"] == "h" and fixture["dayMatch"] == dayMatch for fixture in o["fixtures"])]
            if len(map_data_league_filter_by_day) > 0:
                   # Create doc by league
                   leagueDocument = {}
                   leagueDocument['_id'] = str(dayMatch)
                   leagueDocument['date'] =  datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                   leagueDocument['data'] = []
                   for match in map_data_league_filter_by_day:
                          # Formation H
                          formation_h = getFormation(match['fixtures'][dayMatch-1])
                          timing_h = getTiming(match['fixtures'][dayMatch-1])
                          situation_h = getSituation(match['fixtures'][dayMatch-1])
                          gamestate_h = getGameState(match['fixtures'][dayMatch-1])
                          roster_h = getRoster(match['fixtures'][dayMatch-1], 'h')
                          matchjson = {
                                 'h': match['fixtures'][dayMatch-1]['name_for'] if match['fixtures'][dayMatch-1]['h_a'] == 'h' else match['fixtures'][dayMatch-1]['name_against'],
                                 'a': match['fixtures'][dayMatch-1]['name_for'] if match['fixtures'][dayMatch-1]['h_a'] == 'a' else match['fixtures'][dayMatch-1]['name_against'],
                                 'id_h': match['fixtures'][dayMatch-1]['id_for'] if match['fixtures'][dayMatch-1]['h_a'] == 'h' else match['fixtures'][dayMatch-1]['id_against'],
                                 'id_a': match['fixtures'][dayMatch-1]['id_for'] if match['fixtures'][dayMatch-1]['h_a'] == 'a' else match['fixtures'][dayMatch-1]['id_against'],
                                 'xg_h':match['fixtures'][dayMatch-1]['xg_for'] if match['fixtures'][dayMatch-1]['h_a'] == 'h' else match['fixtures'][dayMatch-1]['xg_against'],
                                 'xg_a':match['fixtures'][dayMatch-1]['xg_for'] if match['fixtures'][dayMatch-1]['h_a'] == 'a' else match['fixtures'][dayMatch-1]['xg_against'],
                                 'g_h':match['fixtures'][dayMatch-1]['g_for'] if match['fixtures'][dayMatch-1]['h_a'] == 'h' else match['fixtures'][dayMatch-1]['g_against'],
                                 'g_a':match['fixtures'][dayMatch-1]['g_for'] if match['fixtures'][dayMatch-1]['h_a'] == 'a' else match['fixtures'][dayMatch-1]['g_against'],
                                 'formation_h': formation_h,
                                 'timing_h': timing_h,
                                 'situation_h': situation_h,
                                 'gamestate_h': gamestate_h,
                                 'roster_h': roster_h
                                 }
                          # Formation A
                          fixture_a = searchFixture_By_ID_DayMatch(allTeamsFromDB,matchjson['id_a'],dayMatch)
                          formation_a = getFormation(fixture_a)
                          timing_a = getTiming(fixture_a)
                          situation_a = getSituation(fixture_a)
                          gamestate_a = getGameState(fixture_a)
                          roster_a = getRoster(fixture_a, 'a')
                          matchjson['formation_a'] = formation_a
                          matchjson['timing_a'] = timing_a
                          matchjson['situation_a'] = situation_a
                          matchjson['gamestate_a'] = gamestate_a
                          matchjson['roster_a'] = roster_a
                          leagueDocument['data'].append(matchjson)  
                   cloudantService.persistDoc(leagueNameDB, leagueDocument) 
              