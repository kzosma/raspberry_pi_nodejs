from constants import opta_constants
from lib.understat import understat_factory
understatService = understat_factory.UnderStatService()
for league in opta_constants.leagues:
    print('League : ' + league)
    for season in opta_constants.seasons:
        print('Season : ' + season)
        understatService.getOptaPlayersData(league,season)
