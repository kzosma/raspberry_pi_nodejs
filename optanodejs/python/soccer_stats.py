import traceback
from optadatalib.soccer_stat_service import SoccerStat
from constants import opta_constants
from lib.cloudant.service import cloudant_db_service
from datetime import datetime
cloudantService = cloudant_db_service.Cloudant()
soccerStatService = SoccerStat()
championship_infos = soccerStatService.getAllLeagues()

# Wide Table
db_wide_table = opta_constants.dbname_soccer_stats+'_wide_table_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_wide_table)
cloudantService.createDatabase(db_wide_table)

# Half Time Databases
db_half_time_goals_stats = opta_constants.dbname_soccer_stats+'_half_time_goals_stats_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_half_time_goals_stats)
cloudantService.createDatabase(db_half_time_goals_stats)

# Scored / conceded   
db_half_time_scored_conceded = opta_constants.dbname_soccer_stats+'_half_time_scored_conceded_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_half_time_scored_conceded)
cloudantService.createDatabase(db_half_time_scored_conceded)

# Goals By Half   
db_half_time_goals_by = opta_constants.dbname_soccer_stats+'_half_time_goals_by_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_half_time_goals_by)
cloudantService.createDatabase(db_half_time_goals_by)

# HT/FT Home
db_half_time_ht_ft_home = opta_constants.dbname_soccer_stats+'_half_time_ht_ft_home_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_half_time_ht_ft_home)
cloudantService.createDatabase(db_half_time_ht_ft_home)

# HT/FT Away
db_half_time_ht_ft_away = opta_constants.dbname_soccer_stats+'_half_time_ht_ft_away_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_half_time_ht_ft_away)
cloudantService.createDatabase(db_half_time_ht_ft_away)

# Record when leading at half-time
db_half_time_record_leading = opta_constants.dbname_soccer_stats+'_record_leading_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_half_time_record_leading)
cloudantService.createDatabase(db_half_time_record_leading)

# Record when trailing at half-time
db_half_time_record_trailing = opta_constants.dbname_soccer_stats+'_record_trailing_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_half_time_record_trailing)
cloudantService.createDatabase(db_half_time_record_trailing)

# Relative Form
db_relative_form = opta_constants.dbname_soccer_stats+'_relative_form_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_relative_form)
cloudantService.createDatabase(db_relative_form)

# Relative Performance
db_relative_performance = opta_constants.dbname_soccer_stats+'_relative_performance_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_relative_performance)
cloudantService.createDatabase(db_relative_performance)

# Points Win/Lost after 75th minutes
db_points_after_75 = opta_constants.dbname_soccer_stats+'_points_win_after_75_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_points_after_75)
cloudantService.createDatabase(db_points_after_75)

# Current Streaks
db_current_streaks = opta_constants.dbname_soccer_stats+'_current_streaks_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_current_streaks)
cloudantService.createDatabase(db_current_streaks)

# Over / Under Full Time (total goals)
db_over_under_fulltime = opta_constants.dbname_soccer_stats+'_over_under_fulltime_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_over_under_fulltime)
cloudantService.createDatabase(db_over_under_fulltime)

# Lead durations
db_lead_durations = opta_constants.dbname_soccer_stats+'_lead_durations_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_lead_durations)
cloudantService.createDatabase(db_lead_durations)

# Goals types
db_goals_types = opta_constants.dbname_soccer_stats+'_goals_types_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_goals_types)
cloudantService.createDatabase(db_goals_types)

# Goals ranges
db_goals_ranges = opta_constants.dbname_soccer_stats+'_goals_ranges_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_goals_ranges)
cloudantService.createDatabase(db_goals_ranges)

# Goals per 10 mn
db_goals_per_10mn = opta_constants.dbname_soccer_stats+'_goals_per_10mn_'+opta_constants.season_soccer_stat
cloudantService.deleteDatabase(db_goals_per_10mn)
cloudantService.createDatabase(db_goals_per_10mn)

for pays in opta_constants.soccer_stats_leagues:
    
    league_i = [item for item in championship_infos if item.get('abbr')==pays]
    url_halftime = league_i[0]['halftime']
    url_wide_table = league_i[0]['widetable']
    url_relative_form = league_i[0]['relative_form']
    url_relative_perf = league_i[0]['relative_perf']
    url_points_win_lost = league_i[0]['points_win_lost']
    url_current_streaks = league_i[0]['current_streaks']
    url_over_under = league_i[0]['over_under']
    url_lead_durations = league_i[0]['lead_durations']
    url_goals_types = league_i[0]['goals_types']
    url_goals_ranges = league_i[0]['goals_ranges']
    url_goals_per_10 = league_i[0]['goals_per_10']
    abbr = league_i[0]['abbr']
    print(abbr)
    
    if abbr != '':
        
        # Wide Table
        try: 
            wide_table_data = soccerStatService.buildWideTable(url=url_wide_table)
            modelDocument_wide_table = soccerStatService.buildDoc(league_i[0]['title'],wide_table_data)
            cloudantService.persistDoc(db_wide_table, modelDocument_wide_table)
        except:
            pass

        # Match goals stats at half-time
        try:
            half_time_goals = soccerStatService.build_half_time_goals_stats(url=url_halftime)
            modelDocument_half_time_goals_stats = soccerStatService.buildDoc(league_i[0]['title'],half_time_goals)
            cloudantService.persistDoc(db_half_time_goals_stats, modelDocument_half_time_goals_stats)
        except:
            pass

        # Scored / conceded   
        try:
            half_time_scored_conceded = soccerStatService.build_half_time_score_conceded(url=url_halftime,abbr=abbr)
            modelDocument_half_time_scored_conceded = soccerStatService.buildDoc(league_i[0]['title'],half_time_scored_conceded)
            cloudantService.persistDoc(db_half_time_scored_conceded, modelDocument_half_time_scored_conceded)
        except:
            pass    

        # Goals by half
        try:
            half_time_goals_by = soccerStatService.build_half_time_goals_by(url=url_halftime)
            modelDocument_half_time_goals_by = soccerStatService.buildDoc(league_i[0]['title'],half_time_goals_by)
            cloudantService.persistDoc(db_half_time_goals_by, modelDocument_half_time_goals_by)
        except:
            pass

        # HT/FT Home
        try:
            half_time_ht_ft_home = soccerStatService.build_home_away_ht_ft(url=url_halftime,id='HT/FT at home')
            modelDocument_half_time_ht_ft_home = soccerStatService.buildDoc(league_i[0]['title'],half_time_ht_ft_home)
            cloudantService.persistDoc(db_half_time_ht_ft_home, modelDocument_half_time_ht_ft_home)
        except:
            pass    

        # HT/FT Away
        try:
            half_time_ht_ft_away = soccerStatService.build_home_away_ht_ft(url=url_halftime,id='HT/FT away')
            modelDocument_half_time_ht_ft_away = soccerStatService.buildDoc(league_i[0]['title'],half_time_ht_ft_away)
            cloudantService.persistDoc(db_half_time_ht_ft_away, modelDocument_half_time_ht_ft_away)
        except:
            pass    
        
        # Record when leading at half-time
        try:
            id_record_leading = r"WHT_1"
            half_time_record_leading = soccerStatService.build_half_time_record_leading_trailing(url=url_halftime,id=id_record_leading)
            modelDocument_half_time_record_leading = soccerStatService.buildDoc(league_i[0]['title'],half_time_record_leading)
            cloudantService.persistDoc(db_half_time_record_leading, modelDocument_half_time_record_leading)
        except:
            pass         
        # Record when trailing at half-time
        try:
            id_record_trailing = r"LHT_1"
            half_time_record_trailing = soccerStatService.build_half_time_record_leading_trailing(url=url_halftime,id=id_record_trailing)
            modelDocument_half_time_record_trailing = soccerStatService.buildDoc(league_i[0]['title'],half_time_record_trailing)
            cloudantService.persistDoc(db_half_time_record_trailing, modelDocument_half_time_record_trailing)
        except:
            pass
        
        # Relative Form
        try:
            relative_form_data = soccerStatService.buildRelativeForm(url_relative_form)
            modelDocument_relative_form = soccerStatService.buildDoc(league_i[0]['title'],relative_form_data)
            cloudantService.persistDoc(db_relative_form, modelDocument_relative_form)
        except:
            pass    
        
        # Relative Performance
        try:
            relative_performance_data = soccerStatService.buildRelativePerformance(url_relative_perf)
            modelDocument_relative_performance = soccerStatService.buildDoc(league_i[0]['title'],relative_performance_data)
            cloudantService.persistDoc(db_relative_performance, modelDocument_relative_performance)
        except:
            pass    
        
        # Points Win/Lost after 75th minutes
        try:
            points_win_after_75 = soccerStatService.buildPointsWinAfter75(url_points_win_lost)
            modelDocument_points_win_after_75 = soccerStatService.buildDoc(league_i[0]['title'],points_win_after_75)
            cloudantService.persistDoc(db_points_after_75, modelDocument_points_win_after_75)
        except:
            pass    
        
        # Current Streaks
        try:
            current_streaks_data = soccerStatService.buildCurrentStreaks(url_current_streaks)
            modelDocument_current_streaks = soccerStatService.buildDoc(league_i[0]['title'],current_streaks_data)
            cloudantService.persistDoc(db_current_streaks, modelDocument_current_streaks)
        except:
            pass    
        
        # Over / Under (total goals)
        try:
            over_under_fulltime_data = soccerStatService.buildOverUnderFullTime(url=url_over_under)
            modelDocument_over_under_fulltime = soccerStatService.buildDoc(league_i[0]['title'],data=over_under_fulltime_data)
            cloudantService.persistDoc(db_over_under_fulltime, modelDocument_over_under_fulltime)
        except:
            pass
        
        # Lead durations
        try:
            lead_durations_data = soccerStatService.buildLeadDurations(url=url_lead_durations)
            modelDocument_lead_durations = soccerStatService.buildDoc(league_i[0]['title'],data=lead_durations_data)
            cloudantService.persistDoc(db_lead_durations, modelDocument_lead_durations)
        except:
            pass    
        
        # Goals types
        try:
            goals_types_data = soccerStatService.buildGoalsTypes(url=url_goals_types)
            modelDocument_goals_types = soccerStatService.buildDoc(league_i[0]['title'],data=goals_types_data)
            cloudantService.persistDoc(db_goals_types, modelDocument_goals_types)
        except:
            pass

        # Goals ranges
        try:

            goals_ranges_data = soccerStatService.buildGoalsRanges(url=url_goals_ranges)
            modelDocument_goals_ranges = soccerStatService.buildDoc(league_i[0]['title'],data=goals_ranges_data)
            cloudantService.persistDoc(db_goals_ranges, modelDocument_goals_ranges)
        except:
            pass

        # Goals per 10 mn
        try:
            goals_per_10_data = soccerStatService.buildGoalsPer10(url=url_goals_per_10)
            modelDocument_goals_per_10 = soccerStatService.buildDoc(league_i[0]['title'],data=goals_per_10_data)
            cloudantService.persistDoc(db_goals_per_10mn, modelDocument_goals_per_10)
        except:
            pass    
        
        # Debug Table
        # soccerStatService.debugTable(url_current_streaks)